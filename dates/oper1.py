#!/usr/bin/python

import time

current_time = time.localtime()

print "time.localtime() : %s" % current_time

print time.strftime('%a, %d %b %Y %H:%M:%S ', current_time)

unixTimeStr=time.time()
print "Time in seconds since the epoch: %s" %unixTimeStr

