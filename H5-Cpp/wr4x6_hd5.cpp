/* 
This example illustrates how to:
a) create a dataset that is a 4 x 6
b) write data 

 on Cori:
 module load cray-hdf5
 CC wr4x6_hd5.cpp -o wr4x6_hd5
 ./wr4x6_hd5

*/

#include <iostream>
#include <string>
#include "H5Cpp.h"
using namespace H5;
using namespace std;

H5std_string	FILE_NAME("data4x6.h5");
const H5std_string	DATASET_NAME("dset");
const int	 NX = 4;                     // dataset dimensions
const int	 NY = 6;
const int	 RANK = 2;

string h5Fname("abc");
const H5std_string	FILE_NAME2(h5Fname);

int main (void) {
  int a[2]={1,2};
  const int K=2;
  hsize_t  b[K]={1,2};
  // Data initialization.     
    int i, j;
    int data[NX][NY];	    // buffer for data to write

    for (j = 0; j < NX; j++)
	for (i = 0; i < NY; i++)
	 data[j][i] = i * 6 + j + 1;

    // Try block to detect exceptions raised by any of the calls inside it
  try  {
    // Turn off the auto-printing when failure occurs so that we can
    // handle the errors appropriately
    Exception::dontPrint();
    
    // Create a new file using the default property lists. 
    H5File file(FILE_NAME, H5F_ACC_TRUNC);

    // Create the data space for the dataset.
    hsize_t dims[2];               // dataset dimensions
    dims[0] = NX;
    dims[1] = NY;
    DataSpace dataspace(RANK, dims);

    // Create the dataset.      
    DataSet dataset = file.createDataSet(DATASET_NAME, PredType::STD_I32BE, dataspace);

    // Write the data to the dataset using default memory space, file
    // space, and transfer properties.
    dataset.write(data, PredType::NATIVE_INT);
    
  }  // end of try block

  // catch failure caused by the H5File operations
  catch(FileIException error)  {
    error.printErrorStack();
    return -1;
  }

  // catch failure caused by the DataSet operations
  catch(DataSetIException error)  {
    error.printErrorStack();
    return -1;
  }

  // catch failure caused by the DataSpace operations
  catch(DataSpaceIException error)  {
    error.printErrorStack();
    return -1;
  }
  
  return 0;  // successfully terminated
}

