/* 
This example illustrates how to use a class:

a) create a dataset that is a pair of arrays
  pars= [a,b] x N
  fpoly= [F1(x), F2(x), F3(x) ] x N 
  where F1(x) = a*x+b, F2(x)=a*x^2+b, F3(x)=a+b*x
  the value of N is dynamic

In ML-world: X=[F1,F2,F3],  Y=[a,b]

b) write the data 

 on Cori:
 module load cray-hdf5
 CC wrFab_hd5.cpp -o wrFab_hd5
 ./wrFab_hd5.cpp

*/

#include <string>
#include <assert.h>
#include "H5Cpp.h"
using namespace H5;
using namespace std;

// - - - - - Header - - - - - 
class EpiData_to_hd5 {
public:
  EpiData_to_hd5( int nx, string xSetName, int ny, string ySetName, string h5Fname );
  ~EpiData_to_hd5() { }
  int nx,ny;  // dataset fixed dimensions
  int createH5(int neve);
  int writeH5(float *dF, int* dI);
private:
  H5std_string	h5Name;
  H5std_string	xSetName,ySetName;
  const int   dataRank = 2; // each set will have also # of event dim
  int neve;  // varied 2nd dim
  DataSet xdataF, ydataI;
};

// - - - - - class body - - - - - 
EpiData_to_hd5::EpiData_to_hd5(int nx1, string xName, int ny1, string yName, string h5Fname){
  h5Name=H5std_string(h5Fname);
  xSetName=H5std_string(xName);
  ySetName=H5std_string(yName);
  nx=nx1; ny=ny1;
  printf(" aaset names: %s %s\n",xSetName.c_str(),ySetName.c_str());
}


int EpiData_to_hd5::createH5(int neve1){
  neve=neve1;
  printf("creating %s for nEve=%d\n",h5Name.c_str(),neve);
  printf(" set names: %s %s\n",xSetName.c_str(),ySetName.c_str());

  try  {
    // Turn off the auto-printing when failure occurs so that we can handle the errors appropriately
    //Exception::dontPrint();
    
    // Create a new file using the default property lists. 
    H5File file(h5Name, H5F_ACC_TRUNC);

    // Create the data space for the dataset and the dataset
    hsize_t  dims[dataRank];    // dataset dimensions
    dims[0] = neve;

    dims[1] = nx;
    DataSpace xdataspace(dataRank, dims);
    xdataF = file.createDataSet(xSetName, PredType::NATIVE_FLOAT, xdataspace);
    
    dims[1] = ny;
    DataSpace ydataspace(dataRank, dims);
    ydataI = file.createDataSet(ySetName, PredType::NATIVE_INT, ydataspace);
    
  }  // end of try block

  // catch failure caused by the H5File operations
  catch(FileIException error)  {
    error.printErrorStack();
    return -1;
  }

  // catch failure caused by the DataSet operations
  catch(DataSetIException error)  {
    error.printErrorStack();
    return -1;
  }

  // catch failure caused by the DataSpace operations
  catch(DataSpaceIException error)  {
    error.printErrorStack();
    return -1;
  }
  
  return 0;  // successfully terminated

}

int EpiData_to_hd5::writeH5(float* data1,int* data2){
  ydataI.write(data2, PredType::NATIVE_INT);
  xdataF.write(data1, PredType::NATIVE_FLOAT);
}


//=======================
//=======================
//=======================

int main (void) {  

  int nEve=10, nx=3, ny=2;
  EpiData_to_hd5 epi5(nx,"func",ny,"pars","epiFab.h5");
  
  // create buffers for data to write
  float *funcData=(float*) malloc(nx*nEve*sizeof(float));
  int *parData=(int*) malloc(ny*nEve*sizeof(int));

  // populate data-buff w/ values
  for (int i=0; i<nEve; i++){
    int a=i, b=3-i, x=i;
    float f1=a*x+b+0.2;
    float f2=a*x*x+b+0.4;
    float f3=a+x*b+0.6;
    
    float *funcP=funcData+nx*i;
    funcP[0]=f1; funcP[1]=f2; funcP[2]=f3;

    int *parP=parData+ny*i;
    parP[0]=a; parP[1]=b;

  }
  
  assert ( epi5.createH5(nEve)==0) ;
  //epi5.createH5(nEve);return 0;

  epi5.writeH5(funcData,parData);    

}

