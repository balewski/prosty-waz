#include <string>
#include <assert.h>
#include "H5Cpp.h"

using namespace H5;
using namespace std;

class EpiData_sliced_hd5 {
public:
  EpiData_sliced_hd5( int nx, int ny, int nDay, string setN1, string h5Fname );
  ~EpiData_sliced_hd5() { }
  int nx,ny;  // dataset fixed dimensions
  void addRecord(ushort *agentData);
  //int createH5(int neve);
  int writeH5(float *dF, int* dI);
private:
  enum  {  rankAg = 3}; // agent
  hsize_t dimsAg[rankAg], dimsAgSlice[rankAg];
  int dayCnt=0;
  DataSet datasetAg;
  //H5std_string  h5Name;
  //H5std_string  xSetName,ySetName;
  //int neve;  // varied 2nd dim
  //DataSet xdataF, ydataI;
};
