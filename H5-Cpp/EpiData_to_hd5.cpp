#include "EpiData_to_hd5.h"
#include <cstring>
// needs: module load cray-hdf5

EpiData_to_hd5::EpiData_to_hd5(int nx1, int ny1, int nz1){
  //Create the data space with fixed dimensions.
  nx=nx1; ny=ny1; nz=nz1;
  int size=nx*ny*nz*sizeof(int);
  dataTr=(int*)malloc(size);
  memset(dataTr, 0, size);
  nzy=nz*ny;
 }



void EpiData_to_hd5::addTr(int i, int j, int k, int val){
  printf("i=%d, j=%d, k=%d   val=%d\n",i,j,k,val);
  dataTr[i*nzy + j*nz + k]+=val;
}  



int EpiData_to_hd5::writeH5(char * setN, char* fileN){
  H5std_string  setName=H5std_string(setN);
  H5std_string  h5Name=H5std_string(fileN);

  // Create a new file using the default property lists. 
  H5File file(h5Name, H5F_ACC_TRUNC);

  // Create the data space for the dataset and the dataset
  hsize_t dimsTr[rankTr]={nx,ny,nz};
  DataSpace dataspace(rankTr, dimsTr);
  DataSet dataI = file.createDataSet(setName, PredType::NATIVE_INT, dataspace);

  // write
  dataI.write(dataTr, PredType::NATIVE_INT);
  
  return 0;
}

