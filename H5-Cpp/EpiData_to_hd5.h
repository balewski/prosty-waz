#include <string>
#include <assert.h>
#include "H5Cpp.h"

using namespace H5;
using namespace std;

class EpiData_to_hd5 {
public:
  EpiData_to_hd5( int nx, int ny, int nz);
  ~EpiData_to_hd5() { }
  int nx,ny,nz,nzy;  // dataset fixed dimensions
  void addTr(int i, int j, int k, int val);
  int writeH5(char *dF, char* dI);
private:
  enum  {  rankTr = 3}; // traces 
  int *dataTr;
};
