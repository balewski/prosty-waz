/* unit test  of EpiData_to_hd5

create extendable (3,3,3)=(y,x,t) expandable data set fill it with '0'
- write a plane (x,y) at  iT-position fill it with '1+x+2*y^2'
- loop over t from 0...20 

Compile:
 module load cray-hdf5
 CC test_EpiData_sliced_hd5.cpp EpiData_sliced_hd5.cpp

Execute:  ./a.out
Print HD5 output: epiFab.h5

 module load tensorflow/gpu-2.0.0-py37
*/

#include "EpiData_sliced_hd5.h"

//=======================
//=======================
//=======================

int main (void) {

  int nDay=5, nx=3, ny=4;
  EpiData_sliced_hd5 epi5(ny,nx,nDay,"func","epiFab.h5");

  // create buffers for data to write
  ushort *agentData=(ushort*) malloc(nx*ny*sizeof(ushort));

  // populate data-buff w/ values
  for (int k=0; k<nDay; k++){
    printf("new day=%d\n",k);
    for (int j=0 ; j<ny;j++)
      for (int i=0 ; i<nx;i++)
	{
	int idx=i+ j*nx;
	int val=1+i+2*j*j+k;
	agentData[idx]=val;
	printf("k=%d, i=%d, j=%d  idx=%d  val=%d\n",k,i,j,idx,val);	  
      }
    epi5.addRecord(agentData);
  }
  exit(0);

    
  //assert ( epi5.createH5(nEve)==0) ;
  //epi5.createH5(nEve);return 0;

  //epi5.writeH5(funcData,parData);
  
}
