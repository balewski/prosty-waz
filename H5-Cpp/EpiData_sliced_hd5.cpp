#include "EpiData_sliced_hd5.h"
// needs: module load cray-hdf5

EpiData_sliced_hd5::EpiData_sliced_hd5(int nAg, int nFt, int nDay, string nameAg, string h5Fname){
  //Create the data space with unlimited dimensions.
  dimsAg[0]=nAg; dimsAg[1]=nFt; dimsAg[2]=nDay; // init
  dimsAgSlice[0]=nAg; dimsAgSlice[1]=nFt; dimsAgSlice[2]=1;
  hsize_t maxdimsAg[rankAg] = {H5S_UNLIMITED, H5S_UNLIMITED, H5S_UNLIMITED};
  DataSpace mspace1( rankAg, dimsAg, maxdimsAg);

  // Create a new file. If file exists it will be overwritten.
  H5File file( H5std_string(h5Fname), H5F_ACC_TRUNC );
  
  // Modify dataset creation properties, i.e. enable chunking.
  DSetCreatPropList chparms;
  hsize_t      chunk_dims[rankAg] ={1,1, 1};
  chparms.setChunk( rankAg, chunk_dims );

  // Set fill value for the dataset
  int fill_val = -1;
  chparms.setFillValue( PredType::NATIVE_USHORT, &fill_val);
  
  // Create a first dataset within the file using fill-val from cparms creation properties.
  datasetAg = file.createDataSet( H5std_string(nameAg), PredType::NATIVE_USHORT, mspace1, chparms);

  printf("A: size %.2f kB\n",datasetAg.getInMemDataSize()/1024.);

  // 
}



void EpiData_sliced_hd5::addRecord(ushort *agentData){
  printf("addRec for day=%d",dayCnt);
  // Select a hyperslab position on the dataset 2D array
  DataSpace fspace2 = datasetAg.getSpace();
  hsize_t  offset[rankAg]={0,0,dayCnt};   dayCnt++;
  fspace2.selectHyperslab( H5S_SELECT_SET, dimsAgSlice, offset );

  // Define memory space
  DataSpace mspace2( rankAg, dimsAgSlice);

  // write new slice
  datasetAg.write( agentData, PredType::NATIVE_USHORT, mspace2, fspace2 );
 
}  

int EpiData_sliced_hd5::writeH5(float* data1,int* data2){
   return 0;
}

