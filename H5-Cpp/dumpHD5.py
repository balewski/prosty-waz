#!/usr/bin/env python3
""" 
test validity of output files 
on Cori:
 module load tensorflow/gpu-2.0.0-py37

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from pprint import pprint
import h5py

#...!...!..................
def read_data_hdf5(inpF):
        print('read data from hdf5:',inpF)
        h5f = h5py.File(inpF, 'r')
        objD={}
        for x in h5f.keys():
            obj=h5f[x][:]
            print('read ',x,obj.shape, obj.dtype)
            objD[x]=obj

        h5f.close()
        print('see %d datasets'%len(objD))
        return objD


import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("--dataPath",
                        default='./',
                        help="path to input")
    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument("-k", "--depth", type=int, default=2,
                        help="depth of printed  tree ")

    parser.add_argument("-N", "--name",
                        default='h5tutr_dset',
                        help="core name")
    
    args = parser.parse_args()

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

print('\n ---- check meta file ----')
inpF=args.dataPath+args.name+'.h5'
blob=read_data_hdf5(inpF)
for k in blob:
    ds1=blob[k]
    print('\ndataset=',k,ds1.shape,ds1.dtype)
    print(ds1)
