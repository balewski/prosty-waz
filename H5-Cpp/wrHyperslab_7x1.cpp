/* create extendable (5,3)=(y,x) expandable data set
- fill it with '-1'
- write vertical slab (y,1) at position (2,1) , fill it with '2'

 on Cori:
 module load cray-hdf5
 CC wrHyperslab_7x1.cpp -o wrHyperslab_7x1
 ./wrHyperslab_7x1
  
 ./dumpHD5.py -N hyperslab

Based on:
https://support.hdfgroup.org/HDF5/doc/cpplus_RM/extend_ds_8cpp-example.html

*/

#include <string>
#include <iostream>
#include <assert.h>
#include "H5Cpp.h"
using namespace H5;
using namespace std;

const H5std_string FILE_NAME( "hyperslab.h5" );
const H5std_string DATASET_NAME( "extenArr" );
const int      RANK = 2;

int main (void) {

  // - - - - - - - - preparation
  //Create the data space with unlimited dimensions.
  hsize_t      dims[RANK]  = { 5, 3}; // dataset dimensions at creation
  hsize_t      maxdims[RANK] = {H5S_UNLIMITED, H5S_UNLIMITED};
  DataSpace mspace1( RANK, dims, maxdims);

  // Create a new file. If file exists it will be overwritten.
  H5File file( FILE_NAME, H5F_ACC_TRUNC );
  
  // Modify dataset creation properties, i.e. enable chunking.
  DSetCreatPropList chparms;
  hsize_t      chunk_dims[RANK] ={1, 1};
  chparms.setChunk( RANK, chunk_dims );

  // Set fill value for the dataset
  int fill_val = -1;
  chparms.setFillValue( PredType::NATIVE_INT, &fill_val);
  
  // Create a first dataset within the file using fill-val from cparms creation properties.
  DataSet dataset = file.createDataSet( DATASET_NAME, PredType::NATIVE_INT, mspace1, chparms);

  // https://support.hdfgroup.org/HDF5/doc/cpplus_RM/class_h5_1_1_data_set.html
  
  cout << "A:"<<dataset.getInMemDataSize ()<<endl;
  
  // Extend the dataset. Dataset becomes 10 x 3.
  hsize_t   dims2[RANK] = { 7, 1};    /* data2 dimensions */
  dims[0]   = dims[0] + dims2[0]; // only this dim need extension
  dataset.extend( dims );
  
  // Select a hyperslab position on the dataset 2D array
  DataSpace fspace2 = dataset.getSpace ();
  hsize_t     offset[2]={2,1};
  fspace2.selectHyperslab( H5S_SELECT_SET, dims2, offset );

  // Define memory space
  DataSpace mspace2( RANK, dims2 );
  
  // Write the data to the hyperslab.    
  int  data2[7]    = { 2, 2, 2, 2, 2, 2, 2};
  dataset.write( data2, PredType::NATIVE_INT, mspace2, fspace2 );
  cout << "B:"<<dataset.getInMemDataSize ()<<endl;
    
  
}
