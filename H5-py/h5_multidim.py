#!/usr/bin/env python

import sys,os
import h5py
import numpy as np
from h5io_util import write_data_hdf5, read_data_hdf5

#...!...!..................
def write_2DMulti_hdf5(dataD,outF):  # handles only arrays and scalars
    dt = h5py.special_dtype(vlen=np.dtype('float32'))
    # or: dt = h5py.special_dtype(vlen=str)
    h5f = h5py.File(outF, 'w')
    print('save data as hdf5:',outF)
    for item in dataD:
        rec=dataD[item]
        dim1=len(rec)
        print(item,'=item, dim1=',dim1)
        dset = h5f.create_dataset('vlen_int', (len(rec),), dtype=dt)
        for i in range(dim1):
            vec=rec[i]
            if type(vec)!=np.ndarray: # scalar --> array[1]
                vec=np.array([vec])
            dset[i]=vec
        print('h5-write :',item, rec.shape)
    h5f.close()
    xx=os.path.getsize(outF)/1048576
    print('closed  hdf5:',outF,' size=%.2f MB'%(xx))

#=================================
#=================================
#  M A I N 
#=================================
#=================================

if __name__=="__main__":

    print('save few 1D arrays')
    dimL=[3,6,12]
    outD={}
    for d in dimL:
        name='block%d'%d
        a=np.zeros(shape=d)
        a[0]=d; a[-1]=-d
        outD[name]=a
    # add string array
    outD['text1']='qwertyuiop123456789ok'
    # add single floating value
    outD['oneF']=3.14
    
    outF='out/one.h5'
    write_data_hdf5(outD,outF, verb=1)
    blob=read_data_hdf5(outF,verb=1)
    text2=blob['text1']; print('M:text2:',type(text2),len(text2),text2)
    print(' 1D is OK\n')    
    outD.pop('text1')
    
    # lets repack the 3 1D arrays in to 1 object with variable length 2nd dim
    outL=[ outD[x] for x in outD]
    out2D = {'aMulD': np.array(outL, dtype=object) }    
    print(out2D)

    outF='out/two.h5'
    write_2DMulti_hdf5(out2D,outF)
    blob=read_data_hdf5(outF)
    print(' 2D is OK, blob:',blob)


