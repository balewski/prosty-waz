#!/usr/bin/env python3

import h5py,os
import numpy as np
from collections import Counter

#...!...!..................
def write_Counter_hdf5(counter,keySize,outF):
    # keySize: Variable controlling the size of the keys in bits
    
    # Function to convert integer to fixed-length binary string
    def int_to_binary_string(i, bits=keySize):  return format(i, '0{}b'.format(bits))

    # Convert keys to binary string representation
    bit_keys = np.array([int_to_binary_string(key) for key in counter.keys()], dtype=f'S{keySize}')  # 'S{keySize}' for string of length keySize
    values = np.array(list(counter.values()), dtype='int')

    # Create an HDF5 file and store the data
    with h5py.File(outF, 'w') as f:
        f.create_dataset('keys', data=bit_keys)
        f.create_dataset('values', data=values)
    xx=os.path.getsize(outF)/1048576
    print('closed  hdf5:',outF,' size=%.2f MB'%(xx))


#...!...!..................
def read_Counter_hdf5(inpF):

    #Function to convert binary string back to integer
    def binary_string_to_int(b):  return int(b.decode(), 2)

    # Read the HDF5 file and restore the Counter
    with h5py.File(inpF, 'r') as f:
        # Read datasets
        bit_keys = f['keys'][:]
        values = f['values'][:]

    #print('bit-keys:',bit_keys)
    # Convert keys back to integers
    keys = np.array([binary_string_to_int(key) for key in bit_keys])

    # Reconstruct the Counter object
    restored_counter = Counter(dict(zip(keys, values)))
    return restored_counter,len(bit_keys[0])

    
#=================================
#=================================
#  M A I N 
#=================================
#=================================

if __name__=="__main__":

    # Example Counter object with very large integer keys (up to 150-bit)
    counter = Counter({55: 78, 2**149: 657, 2**148: 324, 2**147: 945})
    print('INPUT:\n',counter)
    nBit=150
    outF='counter_data_large_keys.h5'
    
    write_Counter_hdf5(counter,nBit,outF)

    counter2,nBit2=read_Counter_hdf5(outF)
    
    print("Restored, keySize=%d Counter:\n"%nBit2, counter2)

    print('M:done')


