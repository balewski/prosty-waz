import sys
import h5py
import numpy as np
def _copy_all(src,dst):
    try:
      for g in src.keys():
          src[g].copy(src[g],dst,g)
    except Exception as e:
      print ('error in copy:',e)
      pass

if __name__=="__main__":
  if (len(sys.argv)) < 3:
     print ('args: template_file new_file')
     exit()
  try:
     src = h5py.File(sys.argv[1],'r')
     dst = h5py.File(sys.argv[2],'w')
  except Exception as e:
     print ('error in file opening:',e)
  _copy_all(src,dst)
  print ('modifying the existing dset')
  new_data = np.zeros((256,256,512))
  del dst['native_fields/matter_density']
  dset = dst.create_dataset('native_fields/matter_density', data=new_data)
  try:
     src.close()
     dst.close()
  except Exception as e: 
     print ('error in file closing')
