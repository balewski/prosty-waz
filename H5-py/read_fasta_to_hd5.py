#!/usr/bin/env python3
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

'''
converts fasta file to HDF5, includes meta-data

This is one-off example and meta-data are hardcoded in the main()

'''

from pprint import pprint
import numpy as np
import json
from Util_H5io4 import  read4_data_hdf5, write4_data_hdf5
import fastaparser
    
#...!...!.................... 
def read_covid_spike_protein(fastaN):

    print('RCSP input FASTA',fastaN)
    seqD={}
    headD={}
    metaD={}
    
    maxLen=0; maxId=None
    with open(fastaN) as fasta_file:
        parser = fastaparser.Reader(fasta_file, parse_method='quick')
        # seq is a FastaSequence object
        # seq is a namedtuple('Fasta', ['header', 'sequence'])    
        for seq in parser:
            seqid=seq.header.split()[0][1:]
            seqlen=len(seq.sequence)
            seqD[seqid]=seq.sequence
            headD[seqid]=seq.header
            if len(seqD)<10: print('name: %s, len: %d'%(seqid,seqlen))
            
            if maxLen<seqlen:   maxLen=seqlen; maxId=seqid
            continue
            print('Header:', seq.header, type(seq.header))
            print('Sequence:', seq.sequence)
    metaD['num_seq']=len(seqD)
    metaD['max_seq_len']=maxLen
    metaD['max_seq_name']=maxId
    metaD['fasta_name']=fastaN
    print('PPSP  got sequences', metaD['num_seq']%3); pprint(metaD)
    
    if 1:  # print more info about FAST content
        print('longest head=',headD[maxId])
        print('seq=',seqD[maxId][:50],'....')
    return seqD,headD,metaD
    
    
#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__=="__main__":
    #fastaN='SARS-CoV-2_seq_update.fa'
    fastaN='NC_045512.2.fasta'    
    seqD,headD,metaD=read_covid_spike_protein(fastaN)

    # add headers to bigD because they may be large
    seqD['seq_headers.JSON']=json.dumps(headD)
    # add other info to MD
    metaD['comment']='A complete genome sequence  (isolate BetaCoV/Nepal/61/2020)  was obtained for a severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2) strain isolated from an oropharyngeal swab specimen of a Nepalese patient with coronavirus disease 2019 (COVID-19), who had returned to Nepal after traveling to Wuhan, China.'
    metaD['fasta_download']='https://www.ncbi.nlm.nih.gov/nuccore/NC_045512.2?report=fasta'
    metaD['cite_paper']='Sah, Ranjit et al. "Complete Genome Sequence of a 2019 Novel Coronavirus (SARS-CoV-2) Strain Isolated in Nepal." Microbiology resource announcements vol. 9,11 e00169-20. 12 Mar. 2020, doi:10.1128/MRA.00169-20'

    
    
    outF=fastaN.replace('.fasta','.h5')
    outF=outF.replace('.fa','.h5')  # cover the alternative, order matters
    write4_data_hdf5(seqD,outF,metaD)

    print('\nM: test reading HD5 back')
    bigD,md=read4_data_hdf5(outF, verb=0)
    pprint(md)
    seqN=md['max_seq_name']
    seq0=bigD[ seqN ][0].decode('UTF-8')  # each sequence is np.array of size (1,)
    
    # vectorized decode:
    if 0:
        decoder = np.vectorize(lambda x: x.decode('UTF-8'))
        seq1 = decoder(seq0)
    print('see seq len:',len(seq0), type(seq0))
    print('dump seq:',seq0[:50])

    print('\nM recover headers ...')
    headJS=bigD.pop('seq_headers.JSON')[0]
    headD2=json.loads(headJS)
    assert len(headD2)==len(bigD)
    print(headD2[seqN])
    print('M:done')


''' Alternative Covid-19 sequence with isolated spikes
   fastaN='SARS-CoV-2_seq_update.fa'

Prepare a snippet of COVID protein sequence
Use https://pypi.org/project/fasta-reader/
Data from 
https://academic.oup.com/nar/article/49/D1/D282/5901966#supplementary-data
Spike FASTA sequence downloaded from
https://cov3d.ibbr.umd.edu/
'''
