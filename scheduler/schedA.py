#!/usr/bin/env python
import sched, time
s = sched.scheduler(time.time, time.sleep)
global k

def print_time(): 
    global k
    print "From print_time", k,time.time()
    k+=1

k=7
def print_some_times():
     print 'A', time.time()
     s.enter(5, 1, print_time, ())
     s.enter(10, 1, print_time, ())
     s.run()
     print 'B',time.time()

if __name__ == '__main__':
    print_some_times()
