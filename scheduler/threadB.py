#!/usr/bin/env python
from threading import Timer

class RepeatedTimer(object):
    def __init__(self, interval, function, *args, **kwargs):
        self._timer     = None
        self.interval   = interval
        self.function   = function
        self.args       = args
        self.kwargs     = kwargs
        self.is_running = False
        self.start()

    def _run(self):
        self.is_running = False
        self.start()
        self.function(*self.args, **self.kwargs)

    def start(self):
        if not self.is_running:
            self._timer = Timer(self.interval, self._run)
            self._timer.start()
            self.is_running = True

    def stop(self):
        self._timer.cancel()
        self.is_running = False

#=========================
if __name__ == '__main__':
    from time import sleep

    global k
    k=5
    def helloB(name):
        global k
        k+=1
        print "Hello %s" % name, 'k,m=',k

    periodSec=3
    durationSec=15
    print "starting...k=",k, ' period/sec=',periodSec
    rt = RepeatedTimer(periodSec, helloB, "World"+str(k)) # it auto-starts, no need of rt.start()
    try:
        sleep(durationSec) # total time of whole sesion
    finally:
        rt.stop() # better in a try/finally block to make sure the program ends!



