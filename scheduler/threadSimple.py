#!/usr/bin/env python

"""

Python application is pending, the statement reads files from the mouse. In the interim, the command will not work any other way. 
"""

import threading
import time
 
class mouseTrack(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.xpos = 0
        self.needToStop = False
 
    def run(self):
        print "mouse-thread started"
        while (not self.needToStop): 
            # unrealiable read from mouse 
            #state = ord(self.f.read(1))
            
            # update the position & buttons
            self.xpos += 1
            time.sleep(3)        
        print "Exiting thread"
 
if __name__ == '__main__':
 
    mt = mouseTrack()
    mt.start()   # start the thread
    k=0
    while True:
        try:
            # print the position every 1 second
            print k,"xpos=%6d  " % (mt.xpos)
            k+=1
            if  mt.xpos>4:
                mt.needToStop = True
                print 'STOP by button in 1 sec'
                break
            time.sleep(1)
            
 
# detect Ctrl-C
        except (KeyboardInterrupt, SystemExit):
            # stop the thread by setting this variable
            mt.needToStop = True
            break
        
