#!/usr/bin/env python
from threading import Event, Thread
import time

def call_repeatedly(interval, func, *args):
    stopped = Event()
    def loop():
        while not stopped.wait(interval): # the first call is in `interval` secs
            func(*args)
    Thread(target=loop).start()    
    return stopped.set



global k

def print_time(): 
    global k
    print "From print_time", k,time.time()
    k+=1

k=7

if __name__ == '__main__':
    cancel_future_calls = call_repeatedly(5, print, "Hello, World")
# do something else here...
    time.sleep(12)  # sleep while time-delay events execute
    cancel_future_calls() # stop future calls


