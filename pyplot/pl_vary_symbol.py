#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt


fig = plt.figure()
fig.subplots_adjust(top=0.8)
ax = fig.add_subplot(111)
ax.set_ylabel('volts')

t = np.arange(0.0, 1.0, 0.05)
s = np.sin(2*np.pi*t)
r=s*0.2
print('ss',s.shape)
xI=[i for i in  range(s.shape[0])]
xL=['a%d'%i for i in  range(s.shape[0])]

mL7=['*','^','x','h','o','x','D']

cL7=['b', 'g', 'r', 'c', 'm', 'y', 'k']  # 7 distinct colors
cL10 = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']  # 10 distinct colors

cL=cL10
mK=4
mL=['$U$','^','x','$a$']


# position in list indexes different types
dD=[{'x':[],'y':[],'ey':[], 'ec':[],'mc':cL[i],'m':mL[i],'lab':'my%d'%i}  for  i in  range(mK)]
print('dD',dD)
for i in range(s.shape[0]):
    j=i%mK  # selector of types
    D=dD[j]
    D['x'].append(xI[i])
    D['y'].append(s[i])
    D['ey'].append(r[i])
    D['ec'].append(cL[i%3])

for i  in  range(mK):  
    ax.errorbar(dD[i]['x'], dD[i]['y'], yerr= dD[i]['ey'], marker= dD[i]['m'], fmt='o', ecolor=dD[i]['ec'],color=dD[i]['mc'], label=dD[i]['lab'])

ax.legend(loc='upper right', title='ecolor=proc3')
ax.set_xticks(xI)
ax.set_xticklabels(xL,rotation=45)
ax.grid()

txt2="Marked and errors have different colors"
plt.text(0.05, 0.95, txt2, fontsize=14, transform=plt.gcf().transFigure)
print ('continue computation')

# at the end call show to ensure window won't close.
plt.show()
