#!/usr/bin/env python
import numpy as np
import random
from matplotlib import pyplot as plt

data = np.array([1.1, 2.1, 0.9, 1.2, 1.4, 3., 8.])

# general layout

fig = plt.figure(figsize=(9,2.5))
#  grid is (yN,xN) - so dumm
ax1 = plt.subplot2grid((3,2), (0,0) )
ax2 = plt.subplot2grid((3,2), (1,0), rowspan=2 )
ax3 = plt.subplot2grid((3,2), (1,1) )
ax4 = plt.subplot2grid((3,2), (2,1) )



# fixed bin size
bins = np.arange(-4, 30, 2) # fixed bin for underlying histo
print ('accumulator bins',bins)
#
ax2.hist(data, bins=bins, alpha=0.5)
ax2.set_xlim([min(data)-1, max(data)+5]) # reduce display range

ax2.set_title(' jan plot, np=%d'%(data.size))
ax2.set_xlabel('variable X')
ax2.set_ylabel('count')


#tight_layout() can take keyword arguments of pad, w_pad and h_pad. These control the extra padding around the figure border and between subplots. The pads are specified in fraction of fontsize.
#plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
plt.tight_layout()
plt.show()
