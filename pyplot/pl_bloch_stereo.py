#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LightSource


#-----------------------------------
#-----------------------------------
#-----------------------------------
class c_bloch2():
    def __init__(self,titA=['aa','bb']):
        fig = plt.figure(figsize=(10,5))
        ns=2
        myViews=[{'elev':85,'azim':45}, # almost top view
                {'elev':10,'azim':20}] #  almost x-y view
        self.axs =[]
        for j in range(ns):
             ax=plt.subplot(1,ns,j+1,projection='3d') 
             self.drawSphere(ax)
             ax.set_title(titA[j])
             self.axs.append(ax)
             ax.view_init(**myViews[j])

#...!...!..................
    def drawSphere(self,ax):
        ax.set_box_aspect([1,1,1],zoom=1.5)
        u, v = np.mgrid[0:2*np.pi:61j, 0:np.pi:31j]
        x = np.cos(u)*np.sin(v)
        y = np.sin(u)*np.sin(v)
        z = np.cos(v)

        #... 3 version of shading of the sphere
        ls=LightSource(azdeg=135,altdeg=0.0)
        self.rgb=ls.shade(np.zeros(z.shape)*1,plt.cm.Greys)
        #self.rgb=ls.shade(1-z,plt.cm.jet)
        self.rgb=ls.shade_rgb(np.ones((z.shape[0],z.shape[1],3)),z) # my preference
        #self.ax.plot_wireframe(x, y, z, color="gray",rstride=5,cstride=5)#,facecolors=self.rgb)
        
        ax.plot_surface(x, y, z, color="gray",rstride=5,cstride=5,alpha=0.2,facecolors=self.rgb)

        # add lables and axis in 3D
        ax.text(1.05,0,0,'X',None,fontsize=15,color='r')
        ax.plot([0,1],[0,0],[0,0],color='r')
        
        ax.text(0,1.05,0,'Y',None,fontsize=15,color='g')
        ax.plot([0,0],[0,1],[0,0],color='g')
        
        ax.text(0,0,1.05,'Z,|0>',None,fontsize=15,color='b')
        ax.plot([0,0],[0,0],[0,1],color='b')

        # mark 0,1 states
        ax.scatter([0], [0], [1], color="b", s=50)
        
        ax.text(0,0,-1.1,'|1>',None,fontsize=15,color='k')
        ax.scatter([0], [0], [-1], color="gold", s=50)
                      
        ax.set_axis_off()
        
#...!...!..................
    def plot(self,xyz,**kwargs):
        if isinstance(xyz, list):
            xyz=np.array(xyz)
            if xyz.ndim==1: xyz=xyz.reshape(-1,3)
                
        assert xyz.shape[1]==3
        for ax in self.axs:
           ax.plot(xyz[:,0],xyz[:,1],xyz[:,2],**kwargs)

#...!...!..................
    def arrows(self,a,**kwargs):
        b=np.copy(a)
        for i in range(nd-1):
            b[i]-=a[i+1]

        p=a[1:]
        v=b[:-1]
        for ax in self.axs:
           ax.quiver(p[:,0],p[:,1],p[:,2],v[:,0],v[:,1],v[:,2],**kwargs)
#=================================
#=================================
#  M A I N 
#=================================
#=================================

if __name__=="__main__":

    # example data-set to be plotted on Bloch sphere
    nd=20
    u=np.linspace(10,270,nd)*np.pi/180
    v=np.linspace(0,160,nd)*np.pi/180
    a=np.zeros((nd,3)) # position in 3D
    for i in range(nd):
        a[i,0]=np.cos(u[i])*np.sin(v[i])
        a[i,1]=np.sin(u[i])*np.sin(v[i])
        a[i,2]=np.cos(v[i])

       
    #.... plot spheres and data
    bloch=c_bloch2(['myBloch','2nd view'])
 
    bloch.plot(a[::2],color='k',linestyle = 'None',marker='*')  # every other point
    bloch.arrows(a,color='g')
    bloch.plot([0.1,0.2,0.3],color='c',marker='o')
    
    plt.tight_layout()
    plt.show()
