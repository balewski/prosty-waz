#!/usr/bin/env python3
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


""" 
plot graphs using  PlotterBackbone

Options to exercise:
 --outPath  to change output dir
 -X   to switch backend and not display plots
 use plot.display_all(png=0)  to produce PDF output

Dependency: matplotlib
Must have X11 tunnel opened  (ssh -Y ...)
all plots show up after plot.display_all() is called - at the end

Issues: 
* output path must exist  (out/ is the default )
* plot.display_all()  block program so call it at the end. You can kill canvas to stop the program, or close canvas to continue
* you CAN'T  do import matplotlib as mpl in to your code  - let PlotterBackbone handle it.


PlotterBackend expects args. to have those members:
args.prjName - the string pre-pended to any plot
args.outPath - valid dir for any outpt files
args.noXterm (boolen) , if True no graphics show up, this is for the batch mode 

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import numpy as np
import  time,os
from pprint import pprint
from PlotterBackbone import PlotterBackbone
from networkx import nx
from networkx.drawing.nx_agraph import graphviz_layout

import argparse

def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],help="increase output verbosity", default=1, dest='verb')

    parser.add_argument("-o", "--outPath", default='out/',help="output path for plots and tables")

    parser.add_argument( "-X","--noXterm", dest='noXterm',  action='store_true', default=False, help="disable X-term for batch mode")

    args = parser.parse_args()
    args.prjName='graph'

    args.formatVenue='prod'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#............................
#............................
#............................
class PlotterEnergyUse(PlotterBackbone):
    def __init__(self, args):
        PlotterBackbone.__init__(self,args)

#...!...!....................
    def one_graph(self,myG,figId=5):
        nrow,ncol=1,1
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        figId=self.smart_append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(7,6))
        ax=self.plt.subplot(nrow,ncol,1)

        color_map = []
        for node in myG:
            if node < 3:
                color_map.append('blue')
            else: 
                color_map.append('yellow')      
        pos=graphviz_layout(myG)
        nx.draw(myG, with_labels=True,pos=pos,  node_color=color_map, edge_color='red')
        #  OR .... , node_color='red'
        edge_labels = nx.get_edge_attributes(G,'e')
        print('append EL',edge_labels)
        nx.draw_networkx_edge_labels(myG, pos,labels = edge_labels) # edge attribute+ID
        #1nx.draw_networkx_edge_labels(G, pos, edge_labels)  # only edgeID

#...!...!....................
def edges_summary(G):
    print('Collect node:neighbour')
    grEdges=[]
    grAF={}  # aft-forw edges
    grAMF={}  # aft-mid-forw =pairs of consecutive edges
    grSL=[]  # self-loops
    grAML={}  # aft-mid loops
    for n in G.nodes():
        grAF[n]=[]
        grAMF[n]=[]
        grAML[n]=[]
        for e in G.neighbors(n):
            #print('n:%d e:%d'%(n,e),e)
            grEdges.append([n,e])
            grAF[n].append(e)
            # collect self-loops
            if e==n: grSL.append(n)
            # collect all 2nd level edges
            for e2 in G.neighbors(e):
                grAMF[n].append([e,e2])
                if e2==n: grAML[n].append(e)
    # compact
    #grAF={ n:[e  for e in G.neighbors(n)]  for n in G.nodes()}
    print('gr edges:',grEdges)   
    print('grAF:',grAF)   
    print('grAMF:',grAMF)
    print('grSL:',grSL)
    print('grAML:',grAML)
    isDAG=nx.is_directed_acyclic_graph(G)
    print('gr is acyclic:',isDAG)
    if isDAG:
        print('gr longest path:',nx.dag_longest_path(G))
    
#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__ == "__main__":

    args=get_parser()

    if 0:
        G = nx.random_tree(6)

    if 0:  # from list of edges
        edges=[[1,0], [0,2],[2,3],[0,3],[0,4],[4,0],[4,5],[5,6]]  # 7n8e
        #edges=[[0,1], [1,2],[2,1],[1,1] ] # 3n4e
        G = nx.DiGraph()
        G.add_edges_from(edges)

    if 1:  # random DAG w/ weighted edges
        G0=nx.gnp_random_graph(10,0.3,directed=True)

        G = nx.DiGraph([(u,v,{'w':np.random.randint(-3,3)}) for (u,v) in G0.edges() if u<v])
        #G= nx.DiGraph([(u,v) for (u,v) in G0.edges() if u<v])  # no edge weighs
        
        
    print("density: %s" % nx.density(G))
    print("is_tree: %s" % nx.is_tree(G))
    edges_summary(G)
    
    # add attributes to edges
    edgeL=G.edges()
    for i,ed in enumerate(edgeL):
        print('work on %d edge='%i, ed)
        G.add_edge(ed[0],ed[1],e=i)

    # ----  just plotting 
    plot=PlotterEnergyUse(args)

    plot.one_graph(G)

    plot.display_all()  # now all plots are created
