#!/usr/bin/env python
# Using scipy, you could use stats.gaussian_kde to estimate the probability density function:

import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats

noise = np.random.normal(0, 1, (1000, ))
density = stats.gaussian_kde(noise)
n, x, _ = plt.hist(noise, bins=np.linspace(-3, 3, 50), 
                   histtype=u'step', density=True)  
plt.plot(x, density(x))

plt.xlabel(r'xxx, $\Omega=22$')
plt.ylabel('Probability')
plt.title(r'$\mathrm{Histogram\ of\ IQ:}\ \mu=100,\ \sigma=15$')

plt.grid(True)

# at the end call show to ensure window won't close.
print ('continue computation')
plt.show()
