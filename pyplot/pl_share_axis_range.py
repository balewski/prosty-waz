#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
fig = plt.figure()

nrow,ncol=2,2
#  grid is (yN,xN) - y=0 is at the top,  so dumm

nPar=3

for iPar in range(nPar):            
    
    ax=plt.subplot(nrow,ncol,iPar+1)            
    if iPar>0 : ax.get_shared_x_axes().join(ax, ax0)
    ax0=ax

    if iPar==0:
        ax.set_title('a sine wave')
        t = np.arange(0.0, 1.0, 0.01)
        s = np.sin(2*np.pi*t)
        ax.plot(t, s, color='blue', lw=2)

    if iPar==1:
        ax.set_title('some histo')
        n, bins, patches = ax.hist(np.random.randn(1000), 50,
                                   facecolor='yellow', edgecolor='yellow')
        
    if iPar==2:
        ax.set_title('two sine waves')

        t = np.arange(0.0, 2.0, 0.01)
        s = np.sin(2*np.pi*t)
        ax.plot(t, s, color='red', lw=1)

    if iPar%ncol==0: # 1st column
        ax.set_ylabel('volts')
    else:
        ax.set_yticklabels([])

    if iPar-nPar>=-ncol:
        ax.set_xlabel('time (s)')
    else:
        ax.set_xticklabels([])

plt.tight_layout() 
print ('continue computation')

# at the end call show to ensure window won't close.
plt.show()
