#!/usr/bin/env python3
import matplotlib as mpl
mpl.use('TkAgg')


import sys
import numpy as np
import random
from matplotlib import pyplot as plt

from matplotlib import cm as cmap

# Default delta is large because that makes it fast, and it illustrates
# the correct registration between image and contours.
delta = 0.5

extent = (-3, 4, -4, 3)

x = np.arange(-3.0, 4.001, delta)
y = np.arange(-4.0, 3.001, delta)
X, Y = np.meshgrid(x, y)
Z1 = np.exp(-X**2 - Y**2)
Z2 = np.exp(-(X - 1)**2 - (Y - 1)**2)
Z = (Z1 - Z2) * 2

# Boost the upper limit to avoid truncation errors.
levels = np.arange(-2.0, 1.601, 0.4)

norm = cmap.colors.Normalize(vmax=abs(Z).max(), vmin=-abs(Z).max())
myCm=cmap.get_cmap('bwr', 11)    # 11 discrete colors
#myCm=cmap.bwr  # continuous colors

fig = plt.figure()
ax=plt.subplot(1, 1, 1)

img=ax.imshow(Z, origin='lower', extent=extent, cmap=myCm, norm=norm)
ax.contour(Z, levels, colors='k', origin='lower', extent=extent)
ax.set_title("Image, origin 'lower'")
fig.colorbar(img, ax=ax,label='shots')


plt.show()
