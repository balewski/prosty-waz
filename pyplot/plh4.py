#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
plt.style.use('ggplot')  # ziu style 1
#plt.style.use('bmh')  # ziu style 2


A = np.arange(1, 11)
B = np.random.randn(10) # 10 rand. values from a std. norm. distr.
C = B.cumsum()

fig, (ax0, ax1) = plt.subplots(ncols=2, sharex=True, sharey=True, figsize=(10,5))

## A) via plt.step()

ax0.step(A, C, label='cumulative sum') # cumulative sum via numpy.cumsum()
ax0.scatter(A, B, label='actual values')
ax0.set_ylabel('Y value')
ax0.legend(loc='upper right')
# text position is using plot coordinates
ax0.text(6.5, -2.0, 'hello world: $\int_0^\infty e^x dx$', size=14, ha='center', va='center')

## B) via plt.plot()

ax1.plot(A, C, label='cumulative sum') # cumulative sum via numpy.cumsum()
ax1.scatter(A, B, label='actual values')
ax1.legend(loc='upper right')
# text position is using plot coordinates???
ax1.annotate('0.25 on axes', (0.25, 0.25), xycoords='axes fraction', size=20)

fig.text(0.5, 0.04, 'sample number', ha='center', va='center')
fig.text(0.5, 0.95, 'Cumulative sum of 10 samples from a random normal distribution', ha='center', va='center')

print ('plot displayed ...')
plt.show()
