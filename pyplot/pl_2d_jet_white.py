import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap

# Create the custom colormap 'jet_white_map'
def create_jet_white_colormap():
    jet_white_colors = [
        #(0, 0, 0.5),  # Dark blue
        (0, 0, 1),    # Blue
        (0, 1, 0),    # Green
        (0, 1, 1),    # Cyan
        (1, 1, 1),    # White
        (1, 1, 0),    # Yellow
        (1, 0, 0),    # Red
        (0.5, 0, 0)   # Dark red
    ]
    return LinearSegmentedColormap.from_list("jet_white_map", jet_white_colors)

# Define the function to plot
def plot_function(a, tau):
    # Create a meshgrid for the x and y values
    x = np.linspace(-5, 5, 400)
    y = np.linspace(-5, 5, 400)
    X, Y = np.meshgrid(x, y)
    R = np.sqrt(X**2 + Y**2)
    
    # Define the function
    Z = np.cos(a * R) * np.exp(-R / tau)
    
    # Plot
    fig, ax = plt.subplots(figsize=(7.2, 6))
    jet_white_map = create_jet_white_colormap()
    zMx=0.7
    c = ax.pcolormesh(X, Y, Z, cmap=jet_white_map, vmin=-zMx, vmax=zMx, shading='auto')
    fig.colorbar(c, ax=ax)
    ax.set_title('2D Function Plot with Custom Colormap')
    ax.set_aspect(1.)
    plt.xlabel('x')
    plt.ylabel('y')
    plt.axis('equal')  # Ensure the aspect ratio is equal to make the circle round
    plt.savefig('out/drop.png')
    plt.show()

# Constants for the function
a = 0.5  * np.pi  # Adjust 'a' to control the number of oscillations
tau = 5  # Adjust 'tau' to control the decay rate

# Plot the function
plot_function(a, tau)
