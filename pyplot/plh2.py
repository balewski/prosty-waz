#!/usr/bin/env python
import sys
(va,vb,vc,vd,ve)=sys.version_info ; assert(va==3)  # needes Python3

import numpy as np
import random
from matplotlib import pyplot as plt

data = np.array([1.1, 2.1, 0.9, 1.2, 1.4, 3., 8.])


# fixed bin size
bins = np.arange(-4, 30, 2) # fixed bin for underlying histo
print ('accumulator bins',bins)

nrow,ncol=1,2
plt.figure(13,facecolor='white', figsize=(6,4))

ax = plt.subplot(nrow,ncol, 1)
ax.hist(data, bins=bins, alpha=0.5)
ax.set_xlim([min(data)-1, max(data)+5]) # reduce display range

ax.set_title(' jan plot, np=%d'%(data.size))
ax.set_xlabel('variable X')
ax.set_ylabel('count')

ax = plt.subplot(nrow,ncol, 2)
plt.axis('off')
txt2='aaa\nbb\n text is show w/o axis'
ax.text(0.1,0.3,txt2,transform=ax.transAxes)
plt.show()
