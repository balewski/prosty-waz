#!/usr/bin/env python3
#from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt


fig = plt.figure()
ax = fig.add_subplot(111,projection='3d')

# Data for a three-dimensional line
zline = np.linspace(0, 15, 1000)
xline = np.sin(zline)
yline = np.cos(zline)
ax.plot3D(xline, yline, zline, 'gray')

# Data for three-dimensional scattered points
zdata = 15 * np.random.random(100)
xdata = np.sin(zdata) + 0.1 * np.random.randn(100)
ydata = np.cos(zdata) + 0.1 * np.random.randn(100)
ax.scatter3D(xdata, ydata, zdata, c=zdata) #, cmap='Greens')


ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')

print ('continue computation')

# rotate the axes and update
elev=30
for angle in range(0, 360,2):
    ax.view_init(elev, azim=angle)
    print('elev,azim=',elev,angle)
    plt.draw()
    plt.pause(.001)
    
