#!/usr/bin/env python3

import numpy as np
from scipy.interpolate import RegularGridInterpolator

#...!...!..................
def interpolate_2Dimage(A,nZoom=2):
    # it will work with mutli-channel array, format : W,H,C
    sizeX=A.shape[0]
    sizeY=A.shape[1]
    binX=np.linspace(0,sizeX-1,sizeX, endpoint=True)
    binY=np.linspace(0,sizeY-1,sizeY, endpoint=True)
    print('A:',A,'nZoom=',nZoom,'\nbinX:',binX,'\nbinY:',binY)
    
    interpA=RegularGridInterpolator((binX,binY),A)

    # new bins for the target B
    binU=np.linspace(0,sizeX-1,sizeX*nZoom, endpoint=True)
    binV=np.linspace(0,sizeY-1,sizeY*nZoom, endpoint=True)
    print('\nbinU',binU.shape, binU)
    print('\nbinV',binV.shape, binV)
    bin1,bin2 = np.meshgrid(binV,binU)
    
    bin2D=np.stack((bin2, bin1), axis=-1)
    #print('\nbin2D',bin2D.shape, bin2D)
    B=interpA(bin2D)
    return B,bin2D
    

#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__ == '__main__':
    if 1:
        A=np.array([[10,20,30],[40,50,60]])
        B,bin2D=interpolate_2Dimage(A,nZoom=2)
        print('\nB:',B,'\nbins:',bin2D[:2,:2])


    if 0:
    # try if it works on multi-channel data, format : W,H,C
        A=np.array([ [[10,11],[20,21]],[[40,41],[50,51]] ])
        B,bin2D=interpolate_2Dimage(A,nZoom=2)
        print('\nB:',B,'\nbins:',bin2D[:2,:2])
