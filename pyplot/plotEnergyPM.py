#!/usr/bin/env python3
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


""" 
plot energy usage by PM jobs

Options to exercise:
 --outPath  to change output dir
 -X   to switch backend and not display plots
 use plot.display_all(png=0)  to produce PDF output

Dependency: matplotlib
Must have X11 tunnel opened  (ssh -Y ...)
all plots show up after plot.display_all() is called - at the end

Issues: 
* output path must exist  (out/ is the default )
* plot.display_all()  block program so call it at the end. You can kill canvas to stop the program, or close canvas to continue
* you CAN'T  do import matplotlib as mpl in to your code  - let PlotterBackbone handle it.


PlotterBackend expects args. to have those members:
args.prjName - the string pre-pended to any plot
args.outPath - valid dir for any outpt files
args.noXterm (boolen) , if True no graphics show up, this is for the batch mode 

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import numpy as np
import  time,os
from pprint import pprint
from PlotterBackbone import PlotterBackbone
import argparse

def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],help="increase output verbosity", default=1, dest='verb')

    parser.add_argument("-o", "--outPath", default='out/',help="output path for plots and tables")

    parser.add_argument( "-X","--noXterm", dest='noXterm',  action='store_true', default=False, help="disable X-term for batch mode")

    args = parser.parse_args()
    args.prjName='eneUse'

    args.formatVenue='prod'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#............................
#............................
#............................
class PlotterEnergyUse(PlotterBackbone):
    def __init__(self, args):
        PlotterBackbone.__init__(self,args)

    #...!...!....................
    def one_job(self,metaD,bigD,figId=5):
        nrow,ncol=1,1
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        figId=self.smart_append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(6,4))
        ax=self.plt.subplot(nrow,ncol,1)
        tit='job=%s'%(metaD['name1'])
        xV=bigD['xV']
        fV=bigD['f1']
        ax.plot(xV,fV)
        ax.set(xlabel='wall time (sec)',ylabel='power (W)', title=tit)
                
         
    #...!...!....................
    def other_job(self,metaD,bigD,figId=5):
        nrow,ncol=1,2
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        figId=self.smart_append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(5,3))
        ax=self.plt.subplot(nrow,ncol,1)
        tit='job=%s'%(metaD['name1'])
        xV=bigD['xV']
        fV=bigD['f2']
        ax.plot(xV,fV)
 
                
         

#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__ == "__main__":
    args=get_parser()

    metaD={'name1':'my func'}
    xV=np.arange(0., 40)* np.pi/100.
    f1=np.sin(xV)
    f2=np.tan(xV)

    bigD={'xV':xV,'f1':f1,'f2':f2}
    # ----  just plotting 
    plot=PlotterEnergyUse(args)

    plot.one_job(metaD,bigD)
    plot.other_job(metaD,bigD)

    plot.display_all()  # now all plots are created
