#!/usr/bin/env python
import sys
(va,vb,vc,vd,ve)=sys.version_info ; assert(va==3)  # needes Python3

import numpy as np
import matplotlib.pyplot as plt
plt.style.use('ggplot')  # ziu style 1
#plt.style.use('bmh')  # ziu style 2

line1, = plt.plot([1,2,3], label="Line 1", linestyle='--')
line2, = plt.plot([3,2,1], label="Line 2", linewidth=4)

# Create a legend for the first line.
first_legend = plt.legend(handles=[line1], loc=1)

# Add the legend manually to the current Axes.
ax = plt.gca().add_artist(first_legend)

# Create another legend for the second line.
plt.legend(handles=[line2], loc=4)


plt.show()
