#!/usr/bin/env python3
import numpy as np
import argparse,os
'''  verify driver works
ssh pm
module load pytorch
python3

import matplotlib as mpl
mpl.use('TkAgg') 
import matplotlib.pyplot as plt 
ax=plt.subplot(1,1,1)
plt.show()
'''
#...!...!..................
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2, 3], help="increase output verbosity", default=1, dest='verb')
   
    parser.add_argument( "-X","--noXterm", action='store_true', default=False, help="disable X-term for batch mode")
    parser.add_argument("--outPath", default='out/',help="output path for plots and tables")

    args = parser.parse_args()
    
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    if not os.path.exists(args.outPath):
        os.makedirs(args.outPath);   print('M: created',args.outPath)
    return args

#...!...!..................
def mini_plotter(args):
    import matplotlib as mpl
    if args.noXterm:
        mpl.use('Agg')  # to plot w/o X-server
        print('Graphics disabled')
    else:
        mpl.use('TkAgg') 
        print('Graphics started, canvas will pop-out')
    import matplotlib.pyplot as plt
    return plt

#...!...!..................
def save_fig( fid, ext='my', png=1):
     plt.figure(fid)
     plt.tight_layout()
     
     figName=os.path.join(args.outPath,'%s_f%d'%(ext,fid))
     if png: figName+='.png'
     else: figName+='.pdf'
     print('Graphics saving to ',figName)
     plt.savefig(figName)
     
#=================================
#=================================
#  M A I N 
#=================================
#=================================

if __name__ == "__main__":
    args=get_parser()
    plt=mini_plotter(args)

    # - - - - - - define data - - - - - - 
    X=[1,2,3]
    Y=[1,4,9]
    
    # - - - - just plotting - - - - 
    ncol,nrow=2,1; figId=4
    plt.figure(figId,facecolor='white', figsize=(8,4))
    ax=plt.subplot(nrow,ncol,1)

    ax.plot(X,Y,'o-')
    ax.set(xlabel='time',ylabel='speed',title='parabola')
    
    save_fig(figId)
    plt.show()
