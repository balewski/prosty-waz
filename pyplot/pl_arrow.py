#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
fig = plt.figure()
fig.subplots_adjust(top=0.8)
ax = fig.add_subplot(111)

#define two arrays for plotting
A = np.array([3, 5, 5, 6, 7, 8])
B = np.array([12, 14, 17, 20, 22, 27])

#create scatterplot, specifying marker size to be 40
ax.scatter(A, B, s=40)

#add arrow to plot
ax.arrow(x=4, y=18, dx=2, dy=5, width=.08,fc='B',shape='right') 

# connect dots  with arrows
DA= A[1:] - A[:-1]
DB= B[1:] - B[:-1] 
PA=A[:-1]
PB=B[:-1]

"""
    display : {'length', 'width', 'alpha'}
        The arrow property to change.
    shape : {'full', 'left', 'right'}
        For full or half arrows.
    max_arrow_width : float
        Maximum width of an arrow, in data coordinates.
    alpha : float
        Maximum opacity of arrows.
    **kwargs
        `.FancyArrow` properties, e.g. *linewidth* or *edgecolor*.
    """
display='length'

width=0.05
max_arrow_width=0.03
alpha=0.5
sf = 0.1  # max arrow size represents this in data coords
fc='red'

for x,y,dx,dy in zip(PA,PB,DA,DB):
    ax.arrow(x,y,dx,dy, fc=fc, ec=fc, alpha=alpha, width=width,
             length_includes_head=True,head_width=0.5)
#  head_width, head_length=head_length, shape=shape,
print ('continue computation')

# at the end call show to ensure window won't close.
plt.show()
