#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt

fig = plt.figure()
fig.subplots_adjust(top=0.8)
ax1 = fig.add_subplot(211)
ax1.set_ylabel('volts')
ax1.set_title('a sine wave 2')

t = np.arange(0.0, 1.0, 0.01)
s = np.sin(2*np.pi*t)
line, = ax1.plot(t, s, color='red', lw=2)

avrT=t.mean()
print('avrT=%.1f '%avrT)
ax1.axvline(avrT, color='b', linestyle='dashed', linewidth=2)

dataL=[1.1,2.1,0.9,1.2,1.4,3.]

# 2nd plot has displaced 
ax2 = fig.add_axes([0.25, 0.1, 0.7, 0.3])

hcount, hbins=np.histogram(dataL, bins=[0, 0.5, 1, 1.5, 2.])
#hcount, hbins=np.histogram(dataL, bins=6)
#np.histogram(np.arange(4), bins=np.arange(5), density=True)
print('hcount=',hcount)
print('hbins=',hbins)


n, bins, patches = ax2.hist(hcount)
ax2.set_xlabel('counts per bin')
ax2.set_title('show numpy histogram')
print ('continue computation')

# at the end call show to ensure window won't close.
plt.show()
