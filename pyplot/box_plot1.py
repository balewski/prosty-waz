#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np

# Fixing random state for reproducibility
np.random.seed(19680801)

# fake up some data
spread = np.random.rand(50) * 100
center = np.ones(25) * 50
flier_high = np.random.rand(10) * 100 + 100
flier_low = np.random.rand(10) * -100
d1 = np.concatenate((spread, center, flier_high, flier_low))

# fake up some more data
spread = np.random.rand(50) * 100
center = np.ones(25) * 40
flier_high = np.random.rand(10) * 100 + 100
flier_low = np.random.rand(10) * -100
d2 = np.concatenate((spread, center, flier_high, flier_low))
d1.shape = (-1, 1)
print('d1 shape',d1.shape)
d2.shape = (-1, 1)

# Making a 2-D array only works if all the columns are the
# same length.  If they are not, then use a list instead.
# This is actually more efficient because boxplot converts
# a 2-D array into a list of vectors internally anyway.

# = = = = = =  = = == = = = = = 
# JAN:  the relevant code is below
data = [d1, d2, d2[::2, 0]]
xlab=['aa','bb','cc']
fig, ax = plt.subplots()

ax.boxplot(data,labels=xlab,vert=False, showfliers=False) # last is no-outliers




# at the end call show to ensure window won't close.
plt.show()
