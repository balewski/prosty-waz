#!/usr/bin/env python
import datetime
import matplotlib.pylab as plt
from matplotlib.dates import DateFormatter

tmL =[]
yL=[]
np=0
for x in range(1,7):
    print (x)
    date1 = datetime.datetime(2003, 8, x, 12, 30, 45)
    print(x,date1)
    tmL.append(date1)
    yL.append(x*x)

fig, ax = plt.subplots()
ax.plot_date(tmL,yL,'r--', linewidth=1, label='my curve')
plt.legend(loc="upper left", bbox_to_anchor=[0, 1],
           ncol=2, shadow=True, title="Legend", fancybox=True)

ax.fmt_xdata = DateFormatter('%Y-%m-%d %H')
fig.autofmt_xdate()

plt.grid(True)

plt.xlabel('date')
plt.ylabel('bird count')
plt.title(r'$\mathrm{Histogram\ of\ IQ:}\ \mu=100,\ \sigma=15$')

print ('continue computation')
plt.show() #to ensure window won't close.


