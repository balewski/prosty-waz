#!/usr/bin/env python3

import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import numpy as np
#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__ == "__main__":

        nrow,ncol=1,1
        figId=1
        fig=plt.figure(figId,facecolor='white', figsize=(6,5))
        ax = plt.subplot(nrow,ncol,1)

        # Generate random x, y pairs
        import random
        from matplotlib.colors import ListedColormap

        random.seed(42)  # Set the random seed for reproducibility
        num_circles = 15
        zClip=3.999
        zticks= [i for i in range(-4, 4+1)]
        x = [random.uniform(0, 10) for _ in range(num_circles)]
        y = [random.uniform(0, 10) for _ in range(num_circles)]
        zV = [random.uniform(-5,5) for _ in range(num_circles)]
        zVc=np.clip(zV,-zClip,zClip)
        
        # Define color map, qualitative divergent, gray is in the middle
        my_colors = [ '#006837', '#1a9641',  '#a6d96a','#c0c0c0','#c0c0c0','yellow','#fdae61','#d7191c']
        numCol=len(my_colors)
        my_cmap = ListedColormap(my_colors)  # Create a custom colormap 
                
        # Plot circles
        for i, (x_val, y_val) in enumerate(zip(x, y)):
            radius = 0.4
            icol=int((zVc[i]+zClip))
            #print('aa','%.2f=%d'%(zVc[i],icol))
            circle_color =  my_colors[icol]
            ax.add_patch(plt.Circle((x_val, y_val), radius, color=circle_color, alpha=0.78))
            ax.text(x_val, y_val,'%.2f=%d'%(zV[i],icol))

        ax.set_xlim(0, 10)
        ax.set_ylim(0, 10)
        ax.set_aspect('equal')
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_title('Circles with random weights')

        
        # Add colorbar representing the circle index based on the sine of the index
        cbar = plt.colorbar(plt.cm.ScalarMappable(cmap=my_cmap), ax=ax)
        cbar.set_label('Sine of Circle Index')
        
        #zticks = [-4,-3,-2,-1,0,1,2,3,4]
        
        cbar.set_ticks(np.linspace(0, 1, num=len(zticks)))
        cbar.set_ticklabels(zticks)

        # Add colorbar as the z-axis representing the gradient colors
        cbar.set_label('deviation from mean')


        plt.show()
