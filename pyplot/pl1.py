#!/usr/bin/env python3
from matplotlib.pyplot import plot, draw, show
import matplotlib
matplotlib.use('TkAgg')

print('my backend: ',matplotlib.get_backend())

plot([1,2,3])
draw()
print ('continue computation')

# at the end call show to ensure window won't close.
show()
