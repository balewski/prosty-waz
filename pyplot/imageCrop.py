#!/usr/bin/env python3
'''
Verify, how images are  alltered by the Image class

'''

from PIL import Image
import torchvision.transforms as transforms

import numpy as np
import argparse,os

#...!...!..................
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2, 3], help="increase output verbosity", default=1, dest='verb')

    parser.add_argument( "-i","--index", default=4,type=int,help="image index")
    parser.add_argument( "-X","--noXterm", action='store_true', default=False, help="disable X-term for batch mode")
    parser.add_argument("--outPath", default='out/',help="output path for plots and tables")

    args = parser.parse_args()
    
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    if not os.path.exists(args.outPath):
        os.makedirs(args.outPath);   print('M: created',args.outPath)
    return args

#...!...!..................
def mini_plotter(args):
    import matplotlib as mpl
    if args.noXterm:
        mpl.use('Agg')  # to plot w/o X-server
        print('Graphics disabled')
    else:
        mpl.use('TkAgg') 
        print('Graphics started, canvas will pop-out')
    import matplotlib.pyplot as plt
    return plt

#...!...!..................
def save_fig( fid, ext='my', png=1):
     plt.figure(fid)
     plt.tight_layout()
     
     figName=args.outPath+'%s_f%d'%(ext,fid)
     if png: figName+='.png'
     else: figName+='.pdf'
     print('Graphics saving to ',figName)
     plt.savefig(figName)
     
#=================================
#=================================
#  M A I N 
#=================================
#=================================

if __name__ == "__main__":
    args=get_parser()
    plt=mini_plotter(args)
    
    #.......... define data
    data_dir             = "data/DIV2K/train"       # The address of the training dataset.
    image_size            =  500      # High-resolution image size in the training dataset.
    nameL = [os.path.join(data_dir, x) for x in os.listdir(data_dir)]
    nameL=sorted(nameL)
    print('num files:',len(nameL),nameL[:2])

    hr_transforms = transforms.Compose([
        transforms.RandomCrop(int(image_size*1.5)),
        transforms.RandomRotation(90),
        transforms.RandomHorizontalFlip(0.5),
        transforms.CenterCrop(image_size)

        #,transforms.ToTensor()  # set channels as 0-axis
    ])
    idx=args.index
    hr0 = Image.open(nameL[idx])
    print('hr0:',type(hr0),np.asarray(hr0).shape)

    hr1=hr_transforms(hr0)
    
    plDD={}
    plDD[0]=['2k',hr0]
    plDD[1]=['rnd-%d'%image_size, hr1]
    
    # - - - - just plotting - - - - 
    ncol,nrow=2,1; figId=4
    plt.figure(figId,facecolor='white', figsize=(10,5))
    for i in range(ncol*nrow):    
        ax=plt.subplot(nrow,ncol,1+i)
        ax.set_aspect(1.)
        txt,hr=plDD[i]
        print(txt,'hr:',type(hr0),np.asarray(hr).shape)
        ax.imshow(np.asarray(hr))
        tit='image %d %s'%(idx,txt)
        ax.set(title=tit)
        if i==0: ax.grid()
    
    save_fig(figId)
    plt.show()
