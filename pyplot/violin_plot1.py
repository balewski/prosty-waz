#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np

''' definition: 
 https://matplotlib.org/api/_as_gen/matplotlib.axes.Axes.violinplot.html

more examples: 
 https://matplotlib.org/gallery/statistics/customized_violin.html#sphx-glr-gallery-statistics-customized-violin-py

See this for WEIGHTED violin plot
https://gist.github.com/jquacinella/1a6341f0f1446973714c

'''


# create test data
np.random.seed(19680801)
data = [sorted(np.random.normal(0, std, 100)) for std in range(1, 5)]
# list of 4, each size of 100

#data=np.array(data);
print('data:',len(data), len(data[0]))
fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, figsize=(9, 4), sharey=True)

ax1.set_title('Default violin plot')
ax1.set_ylabel('Observed values')
xBins=[5,13,22,33]
ax1.violinplot(data,positions=xBins,widths=3)

ax2.set_title('Customized violin plot')
ax2.set_ylabel('Observed letters')
viop=ax2.violinplot(data,showmeans=True, showmedians=False,  showextrema=False)
for pc in viop['bodies']:
    pc.set_facecolor('red')

''' all options: 
Axes.violinplot(self, dataset, positions=None, vert=True, widths=0.5, 
showmeans=False, showextrema=True, showmedians=False, quantiles=None, 
points=100, bw_method=None, *, data=None)[source]
'''

# set style for the axes

def set_axis_style(ax, labels):
    ax.get_xaxis().set_tick_params(direction='out')
    ax.xaxis.set_ticks_position('bottom')
    ax.set_xticks(np.arange(1, len(labels) + 1))
    ax.set_xticklabels(labels)
    ax.set_xlim(0.25, len(labels) + 0.75)
    ax.set_xlabel('Sample name')



labels = ['A', 'B', 'C', 'D']
for ax in [ax2]:
    set_axis_style(ax, labels)


plt.tight_layout()
plt.subplots_adjust(bottom=0.15, wspace=0.05)
plt.show()
