#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import scipy
from scipy.stats import norm,rayleigh

''' 
Based on
https://stackoverflow.com/questions/6620471/fitting-empirical-distribution-to-theoretical-ones-with-scipy-python?lq=1
  
fits empirical data with various PDFs
Assumes x is int

'''
events = 10

# it is chi2 with mean and std
#https://en.wikipedia.org/wiki/Rayleigh_distribution
y = rayleigh.rvs(loc=5,scale=2,size=events) # samples generation
x = np.linspace(3,15,20)


h = plt.hist(y, bins=x)

dist_names = ['rayleigh', 'gamma', 'beta', 'norm', 'pareto']

for dist_name in dist_names:
    dist = getattr(scipy.stats, dist_name)
    param = dist.fit(y)
    print('fit:',dist_name,'param-->',param)

    # Get line for each distribution (and scale to match observed data)
    pdf_fitted = dist.pdf(x, *param[:-2], loc=param[-2], scale=param[-1])
    scale_pdf = np.trapz (h[0], h[1][:-1]) / np.trapz (pdf_fitted, x)
    pdf_fitted *= scale_pdf
    plt.plot(x,pdf_fitted, label=dist_name)
    #plt.xlim(0,47)
    #break
plt.legend(loc='upper right')
plt.tight_layout()
plt.show()
