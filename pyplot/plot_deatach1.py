#!/usr/bin/env python

import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import numpy as np
import os

def detach_display():
    mu, sigma = 0, 0.5
    x = np.linspace(-3, 3, 100)
    plt.plot(x, mlab.normpdf(x, mu, sigma))
    plt.show()    

if os.fork():
    # Parent
    pass
else:
    # Child
    detach_display()
