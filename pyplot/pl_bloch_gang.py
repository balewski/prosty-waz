#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LightSource


#-----------------------------------
#-----------------------------------
#-----------------------------------
class c_bloch():
    def __init__(self,fig):
        self.ax = fig.add_subplot(111,projection='3d')
        self.drawSphere()

#...!...!..................
    def drawSphere(self):
        self.ax.set_box_aspect([1,1,1],zoom=1.5)
        u, v = np.mgrid[0:2*np.pi:61j, 0:np.pi:31j]
        x = np.cos(u)*np.sin(v)
        y = np.sin(u)*np.sin(v)
        z = np.cos(v)

        #... 3 version of shading of the sphere
        ls=LightSource(azdeg=135,altdeg=0.0)
        self.rgb=ls.shade(np.zeros(z.shape)*1,plt.cm.Greys)
        #self.rgb=ls.shade(1-z,plt.cm.jet)
        self.rgb=ls.shade_rgb(np.ones((z.shape[0],z.shape[1],3)),z) # my preference
        #self.ax.plot_wireframe(x, y, z, color="gray",rstride=5,cstride=5)#,facecolors=self.rgb)
        
        self.ax.plot_surface(x, y, z, color="gray",rstride=5,cstride=5,alpha=0.2,facecolors=self.rgb)

        # add lables and axis in 3D
        self.ax.text(1.1,0,0,'X',None,fontsize=15,color='r')
        self.ax.plot([0,1],[0,0],[0,0],color='r')
        
        self.ax.text(0,1.1,0,'Y',None,fontsize=15,color='g')
        self.ax.plot([0,0],[0,1],[0,0],color='g')
        
        self.ax.text(0,0,1.1,'Z',None,fontsize=15,color='b')
        self.ax.plot([0,0],[0,0],[0,1],color='b')
        self.ax.text(0,0,0.9,'|0>',None,fontsize=15,color='k')
        self.ax.scatter([0], [0], [1], color="b", s=100)
        
        self.ax.text(0,0,-0.9,'|1>',None,fontsize=15,color='k')
        self.ax.scatter([0], [0], [-1], color="gold", s=100)
                      
        self.ax.set_axis_off()
        
#...!...!..................
    def plot(self,xyz,**kwargs):
        if isinstance(xyz, list):
            xyz=np.array(xyz)
            if xyz.ndim==1: xyz=xyz.reshape(-1,3)
                
        assert xyz.shape[1]==3
        self.ax.plot(xyz[:,0],xyz[:,1],xyz[:,2],**kwargs)
     
#=================================
#=================================
#  M A I N 
#=================================
#=================================

if __name__=="__main__":

    nd=20
    u=np.linspace(10,270,nd)*np.pi/180
    v=np.linspace(0,160,nd)*np.pi/180
    a=np.zeros((nd,3))
    for i in range(nd):
        a[i,0]=np.cos(u[i])*np.sin(v[i])
        a[i,1]=np.sin(u[i])*np.sin(v[i])
        a[i,2]=np.cos(v[i])

    #.... plot sphere and data
    fig = plt.figure(figsize=(5,5))
    bloch=c_bloch(fig)
    bloch.ax.set_title('bloch')
    bloch.plot(a,color='k',marker='*')
    bloch.plot([0.1,0.2,0.3],color='c',marker='o')
    
    plt.tight_layout()
    plt.show()
