#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.patches as mpatches # for legend

ax= plt.subplot(1,1,1)

#
# first create a single histogram
#
data = np.random.randn(82)

# the histogram of the data with histtype='step'
binX=10
binX=np.linspace(-2,2,num=10)
n, bins, patches = ax.hist(data, bins=binX, normed=1, histtype='stepfilled')
print('bins',bins,bins.shape,binX.shape)

# Set the ticks to be at the edges of the bins.
ax.set_xticks(bins)

# Set ticks labels for x-axis
x_ticks_labels = ['my%.1f'%x for x in binX ]
ax.set_xticklabels(x_ticks_labels, rotation=90, fontsize=18)

plt.tight_layout()

plt.show()
