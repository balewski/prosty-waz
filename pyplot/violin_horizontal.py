#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.patches as mpatches # for legend

''' definition: 
 https://matplotlib.org/api/_as_gen/matplotlib.axes.Axes.violinplot.html

more examples: 
 https://matplotlib.org/gallery/statistics/customized_violin.html#sphx-glr-gallery-statistics-customized-violin-py
'''

# create test data
np.random.seed(19680801)
data = [sorted(np.random.normal(0, std, 100)) for std in range(1, 5)]
# list of 4, each size of 100

#data=np.array(data);
print('data:',len(data), len(data[0]))
ax= plt.subplot(1,1,1)

ax.set_title('Customized horizontal violin plot')
ax.set_xlabel('Observed letter prob.')
viop=ax.violinplot(data,showmeans=True, showmedians=False,  showextrema=False, vert=False, widths=0.9)
for pc in viop['bodies']:
    pc.set_facecolor('red')

''' all options: 
Axes.violinplot(self, dataset, positions=None, vert=True, widths=0.5, 
showmeans=False, showextrema=True, showmedians=False, quantiles=None, 
points=100, bw_method=None, *, data=None)[source]
'''

# set style for the axes

def set_axis_style(ax, labels):
    ax.get_xaxis().set_tick_params(direction='out')
    ax.xaxis.set_ticks_position('bottom')
    ax.set_yticks(np.arange(1, len(labels) + 1))
    ax.set_yticklabels(labels)
    ax.set_ylim(0.25, len(labels) + 0.75)
    ax.set_ylabel('Sample name')


labels = ['Axxxx', 'Byyyyy', 'Cdddddd', 'D2133-e12-0213']
set_axis_style(ax, labels)

# add label to violine shade
labels = []
def add_label(viop, label):
    color = viop["bodies"][0].get_facecolor().flatten()
    labels.append((mpatches.Patch(color=color), label))

add_label(viop, "4 letters")
plt.legend(*zip(*labels), loc=2)

plt.tight_layout()
plt.subplots_adjust(bottom=0.15, wspace=0.05)
plt.show()
