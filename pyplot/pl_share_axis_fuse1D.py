#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt
import numpy as np

# Some example data to display
x = np.linspace(0, 2 * np.pi, 400)
y = np.sin(x ** 2)

fig, axs = plt.subplots(3, sharex=True, sharey=True, gridspec_kw={'hspace': 0})
#fig.suptitle('Sharing both axes')
axs[0].plot(x, y ** 2)
axs[1].plot(x, 0.3 * y, 'o')
axs[2].plot(x, y, '+')


# Hide x labels and tick labels for all but bottom plot.
for ax in axs:
    ax.label_outer()


ax.set(xlabel='xlab', ylabel='yLab')
axs[0].set(title='top-tit')
    
# DO NOT USE:
plt.tight_layout() 
print ('continue computation')

# at the end call show to ensure window won't close.
plt.show()
