#!/usr/bin/env python3
import sys

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import matplotlib as mpl
mpl.use('TkAgg')
#plt.style.use('ggplot')  # ziu style 1
#plt.style.use('bmh')  # ziu style 2

n=11000
x = np.random.normal(5,10,11000)
y = np.random.normal(-5,8,11000)

fig=plt.figure(1,facecolor='white', figsize=(10,5))
ax=plt.subplot(121)

binsL=np.linspace(-30,30,20)
zsum,xbins,ybins,img = ax.hist2d(x,y,bins=binsL,norm=LogNorm(), 
                                   cmap = plt.cm.rainbow)

plt.colorbar(img, ax=ax)
#fig.colorbar()
z0=np.amin(zsum)
z1=np.amax(zsum)
dz=z1-z0
print('h2 out',zsum.shape,z0,z1)

zLevel=[1,5,10,25,50,70,80,100]
zLevel=np.linspace(z0+0.1*dz,z1-0.1*dz,8)

print('zLevel',zLevel)
ax=plt.subplot(122)
ax.contour(zsum.T,extent=[xbins[0],xbins[-1],ybins[0],ybins[-1]],
           linewidths=3, cmap = plt.cm.rainbow, levels = zLevel)


plt.tight_layout()  
plt.show()
