#! /usr/bin/python

print 'Test single ssh  using Popen'

from subprocess import Popen, PIPE
myKey=' -i ~/.ssh/id_rsa-futureGrid '
userAtIP=' balewski@sierra.futuregrid.org'
remoteCmd='ls -l; xload; pwd '
cmd = " ssh "+myKey+userAtIP+" '"+remoteCmd+"'"
print 'cmd='+cmd+'='
#exit(1)
p = Popen(cmd , shell=True, stdout=PIPE, stderr=PIPE)
out, err = p.communicate()
    print "***Return code: ", p.returncode
    print "***Return stdout:", out.rstrip()
    print "***Return stderr:", err.rstrip()
