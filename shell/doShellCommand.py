#!/usr/bin/env python
import subprocess



################################
#     execute shell command 'cmd'
################################

def execShell( cmd):
    print 'do command:',cmd
    p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                         close_fds=True)
    print 'cmd: stdOut='
    for line in p.stdout.readlines():
        print line[:-1]

    print 'cmd: stdErr='
    for line in p.stderr.readlines():
        print line[:-1]


################################
#     MAIN
################################

cmd ='ls -l; pwd'

execShell(cmd)
