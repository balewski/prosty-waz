#! /usr/bin/python

print 'Test single scp  using Popen'

from subprocess import Popen, PIPE
myKey=' -i ~/.ssh/id_rsa-futureGrid '
userAtIP=' balewski@sierra.futuregrid.org'
inpFile='test.txt'
outFile=' '
cmd = " scp "+myKey+inpFile+userAtIP+":"+outFile
print 'cmd='+cmd+'='
#exit(1)
p = Popen(cmd , shell=True, stdout=PIPE, stderr=PIPE)
out, err = p.communicate()
print "Return code: ", p.returncode
print out.rstrip(), err.rstrip()
