#! /usr/bin/python

print 'Test single ssh  using Popen'

from subprocess import Popen, PIPE
myKey=' -i ~/.ssh/id_rsa-futureGrid '
userAtIP=' balewski@sierra.futuregrid.org'

myKey2=' -i ~/.ssh/FG-opnstk-balewski3-sierra.pem '
userAtIP2='  root@10.35.23.48 '

doubleRemoteCmd='ls -l RemoteVM '
remoteCmd=" ssh "+myKey2+userAtIP2+'" echo ssh1 done; '+doubleRemoteCmd+'"'
cmd = " ssh "+myKey+userAtIP+" '"+remoteCmd+"'"
print 'cmd='+cmd+'='
p = Popen(cmd , shell=True, stdout=PIPE, stderr=PIPE)
out, err = p.communicate()
print "***Return code: ", p.returncode
print "***Return stdout:", out.rstrip()
print "***Return stderr:", err.rstrip()
