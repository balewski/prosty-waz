#!/usr/bin/env python3

# https://zetcode.com/python/socket/

# One service that used UDP is the Quote of the Day (QOTD).

import socket

#hostN='campbell109.dyn.berkeley.edu'
addr = ("djxmmx.net", 17)  # host,port

with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:

    message = b''
    
    print('for host:',addr)

    s.sendto(message, addr)

    data, address = s.recvfrom(1024)
    print(data.decode())


