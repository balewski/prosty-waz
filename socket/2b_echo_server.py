#!/usr/bin/env python3
'''
code from
https://stackoverflow.com/questions/51331426/python-socket-how-do-i-choose-between-s-send-and-conn-send

minimal example programs using the TCP/IP protocol: a server that echoes all data that it receives back (servicing only one client), and a client using it. Note that a server must perform the sequence socket(), bind(), listen(), accept() (possibly repeating the accept() to service more than one client), while a client only needs the sequence socket(), connect(). Also note that the server does not sendall()/recv() on the socket it is listening on but on the new socket returned by accept().

The server one is where there there are both s and conn
'''


import socket

HOST = ''                 # Symbolic name meaning all available interfaces
PORT = 50007              # Arbitrary non-privileged port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)
conn, addr = s.accept()
print ('Got client ', addr)
while 1:
    data = conn.recv(1024)
    if not data:    break
    
    print('M: received len:',len(data),data)
    data2=data+bytearray('-Re:I do not care', 'utf-8')
    conn.sendall(data2)
    print ('M: responded')
conn.close()
