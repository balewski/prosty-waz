#!/usr/bin/env python3
'''
2 code taken from http://zetcode.com/python/socket/

Shell test: $  nc -vz time.nist.gov 13
'''

import socket

host = "webcode.me" ; port = 80
#host = "time.nist.gov" ; port = 13

ip = socket.gethostbyname(host)
print('host:',host,' has the IP address:',ip)

print('M: connect to socket %s:%d\n'%(host,port))
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:

    s.settimeout(5)  # timeout in seconds
    s.connect((host, port))

    s.sendall(b"HEAD / HTTP/1.1\r\nHost: webcode.me\r\nAccept: text/html\r\n\r\n")
    print(str(s.recv(1024), 'utf-8'))
    print('M: head received\n')
