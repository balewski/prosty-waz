#!/usr/bin/env python3

# https://zetcode.com/python/socket/

import socket

hostN='example.com'
hostN='campbell109.dyn.berkeley.edu'

ip = socket.gethostbyname(hostN)

print('for host:',hostN,'we get the IP address:',ip)
