#!/usr/bin/env python3
'''
code from
https://stackoverflow.com/questions/51331426/python-socket-how-do-i-choose-between-s-send-and-conn-send


Port forwarding:
I did the following
 a) ssh baci-desktop.dhcp.lbl.gov,  started the server: ./2b_echo_server.py
 b) ssh cori07 , xterm A,
   ssh  -L 50007:localhost:50007 baci-desktop.dhcp.lbl.gov

   ssh was accepted and I see prompt of baci-desktop  - this should keep the local port 5007 on cori mapped to local port 5007 on baci-desktop.
 c) ssh cori07 , xterm B,  start client:  ./2a_echo_client.py  


b')  ssh -p 8844 -L 50007:localhost:50007 campbell109.dyn.berkeley.edu


'''


import socket

#HOST = 'baci-desktop.dhcp.lbl.gov'    # The remote host
HOST='localhost'
HOST='127.0.0.1'
PORT = 50007              # The same port as used by the server

ip = socket.gethostbyname(HOST)
print('target host:',HOST,'has IP address:',ip)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
addr=(HOST, PORT)
s.connect(addr)
print('M: connected to',addr)

string = "Python is interesting."; print('inp len:',len(string))
p= bytearray(string, 'utf-8')
s.send(p)
print('M: send done')
data = s.recv(1024)
print ('Received', len(data), repr(data) )
s.close()

