#!/usr/bin/env python
'''
The are servers that provide current time. A client simply connects to the server with no commands, and the server responds with a current time.

Note: Time servers come and go, so we might need to find a working server on https://www.ntppool.org/en/.

Shell test: $  nc -vz time.nist.gov 13
'''

import socket
import time
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:

    host = "time.nist.gov"
    port = 13

    s.connect((host, port))
    s.sendall(b'')
    print(str(s.recv(1024), 'utf-8'))
    print('M: timestap received from %s:%d'%(host,port))
