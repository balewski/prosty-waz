#!/usr/bin/env python

from pprint import pprint


""" 
function-based, Insert single record w/ timestamp to mongoDb
Needed packages:
module load python/2.7.3  

"""
# - - - -  assemble record to be inserted
import datetime


def get_db():
    from pymongo import MongoClient
    URI ='mongodb://mongodb01.nersc.gov:27017'
    DB_NAME='STARProdState' # aka collection
    USERNAME='STARProdState_admin'
    PASSWORD='w2e23sddf21'

    client = MongoClient(URI)
    db = client[DB_NAME]
    db.authenticate(USERNAME, PASSWORD)
    return db

def add_one(db,collName):
    postOne = {"author": "Jan","text": "heart A","date": datetime.datetime.utcnow()}

    myColl=db[collName]
    myColl.insert(postOne)
    pprint(postOne)
    print 'added ',myColl.find({}).count(),' rec to coll=',collName
    

def get_all_doc(db,collName):
    cursor = db[collName].find({})
    print 'count=',cursor.count()
 
    for item in cursor:
        pprint(item) 


if __name__ == "__main__":

    db = get_db()
    print 'my db HEAD=',db

    # add one item to this speciffic collection
    coll='myHeart'
    add_one(db,coll)
    #get_all_doc(db,coll)
  
