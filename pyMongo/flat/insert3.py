#!/usr/bin/env python

""" 
Insert single record to mongoDb
Needed packages:
module load python/2.7.3  

"""
import pymongo
from pymongo import MongoClient

URI ='mongodb://mongodb01.nersc.gov:27017'
DB_NAME='STARProdState' # aka collection
USERNAME='STARProdState_admin'
PASSWORD='w2e23sddf21'
cli = MongoClient(URI)
dbh = cli[DB_NAME]
dbh.authenticate(USERNAME, PASSWORD)

print 'my db host=',dbh,' , client=',cli

# - - - -  assemble record to be inserted
import datetime
postOne = {"author": "Mike3_utc","text": "My first X","date": datetime.datetime.utcnow()}

postOne={"name" : "Canada"}

print 'my new record to be added=',postOne

# ---- insert record into collection
postMan = dbh.posts
print 'my postMan so far=',postMan

id=postMan.insert([postOne])
print 'id=',id
