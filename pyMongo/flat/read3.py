#!/usr/bin/env python

""" 
Insert single record to mongoDb
Needed packages:
module load python/2.7.3  

"""
import pymongo
from pymongo import MongoClient

URI ='mongodb://mongodb01.nersc.gov:27017'
DB_NAME='STARProdState'
USERNAME='STARProdState_admin'
PASSWORD='w2e23sddf21'
MY_COLL='restaurants'
cli = MongoClient(URI)
db = cli[DB_NAME]
db.authenticate(USERNAME, PASSWORD)

print 'my coll=',db,'  client=',cli
cursor = db[MY_COLL].find({})

# for nice printing
from pprint import pprint

print 'count=',cursor.count()

for item in cursor:
    pprint(item) 

