#!/usr/bin/env python

# for nice printing
from pprint import pprint


""" 
function-based, Insert single record to mongoDb
Needed packages:
module load python/2.7.3  

"""


def get_db():
    from pymongo import MongoClient
    URI ='mongodb://mongodb01.nersc.gov:27017'
    DB_NAME='STARProdState' # aka collection
    USERNAME='STARProdState_admin'
    PASSWORD='w2e23sddf21'

    client = MongoClient(URI)
    db = client[DB_NAME]
    db.authenticate(USERNAME, PASSWORD)
    return db

def add_one(db):
    db[MY_COLL].insert({"name2" : "Canada"})
    

def get_all_docs(db,coll):
    cursor = db[coll].find({})
    print 'count=',cursor.count()
    
def del_coll(db,coll):
    result = db[coll].find({})
    myLen=result.count()
    print 'del=',coll,' count=',myLen
    if myLen<=0:        
        return
    print ' drop coll=',coll
    db[coll].drop()
    print 'drop done'



if __name__ == "__main__":

    db = get_db()
    print 'my db HEAD=',db
    MY_COLL='myName2'
    #MY_COLL='mycollection'
    
    del_coll(db,MY_COLL)
        
    print 'All collections:'
    collList=db.collection_names()
    #print collList
    for X in collList:
        if X=='system.indexes':
            print '---Totla numColl=',db[X].find({}).count()
            continue;
        
        print "***COLL:",X
        get_all_docs(db,X)

