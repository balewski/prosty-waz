#!/usr/bin/env python

# for nice printing
from pprint import pprint


""" 
function-based, Insert single record to mongoDb
Needed packages:
module load python/2.7.3  

"""


def get_db():
    from pymongo import MongoClient
    URI ='mongodb://mongodb01.nersc.gov:27017'
    DB_NAME='STARProdState' # aka collection
    USERNAME='STARProdState_admin'
    PASSWORD='w2e23sddf21'

    client = MongoClient(URI)
    db = client[DB_NAME]
    db.authenticate(USERNAME, PASSWORD)
    return db

def add_one(db):
    db[MY_COLL].insert({"name2" : "Canada"})
    
def get_one(db):    
    return db[MY_COLL].find_one()

def get_all(db):
    cursor = db[MY_COLL].find({})
    print 'count=',cursor.count()
    return
    for item in cursor:
        pprint(item) 

    return db[MY_COLL].find_one()

if __name__ == "__main__":

    db = get_db()
    print 'my db HEAD=',db

    # add one item to this speciffic collection
    MY_COLL='myName2'
    add_one(db)

    
    print 'All collections:'
    collList=db.collection_names()
    #print collList
    for item in collList:
        if item=='system.indexes':
            continue;
        
        print "***COLL:",item
        MY_COLL=item

        get_all(db)
        pprint(get_one(db))        
