#!/usr/bin/env python
""" Example of write to  mongoDB w/ swiss knife """

import datetime

    
from MongoDbUtilLean import MongoDbUtil

def oneWrite(collName,userD):

    print 'write to coll=',collName
    dbColl = MongoDbUtil('admin').database()[collName]
    print dbColl
    mxN=dbColl.count()

    postOne={}
    postOne['date']=datetime.datetime.utcnow()
    postOne['users']=userD
    
    # ---- insert record into collection
    print 'writte rec=',mxN+1,' body=',postOne 
    id=dbColl.insert([postOne])
    print 'id=',id


#---------------------
def recentRead(collName,nShow=5):

    print 'write to coll=',collName
    dbColl = MongoDbUtil('admin').database()[collName]
    print dbColl
    mxN=dbColl.count()
    
    mxN=dbColl.count()
    if mxN<=0:
        print 'your collection is empty, quit'
        return
    k=0
    
    if nShow > mxN-1:
        recL=dbColl.find()
    else:
        recL=dbColl.find().skip(mxN-nShow)

    print 'show  last ',nShow, ' records of ',mxN
    print
    for it in recL:
        if k==99:
            print it
        k+=1    
        print k,  it['date']



################################
#     MAIN
################################


if __name__ == '__main__':

    collName='janTestB'
    userD={"jan":5,"tom":4}
    oneWrite(collName,userD)

    recentRead(collName)
