#! /usr/bin/python

print 'STAR of fiboMain'
# based on tutorial:  http://docs.python.org/2/tutorial/modules.html
from fiboModule import fib1

fib1(5)
#fib3(5)  # error , undefined

# alternatively:
print 'load all  all names within the  module'

from fiboModule import *

print fib2(5)

fib3(5)  # works here
