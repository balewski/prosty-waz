#!/usr/bin/env python

class Staff601:
    course = '6.01'
    building = 34
    salary=-1

    # constructor 
    def __init__(self, name, role, age): 
        self.name = name
        self.role = role
        if self.role == 'Professor':
            self.salary = 100
        elif self.role == 'TA':
            self.salary = 50
        else:
            self.salary = 20
        self.giveRaise((age - 18) * 0.03)
        
    
         
    def salutation(self):
        return self.role + ' ' + self.name
        
    def giveRaise(self, percentage):
        print 'giveRaise: to ',self.name,' old salary=',self.salary,' rise procentage=', percentage
        self.salary = self.salary + self.salary * percentage
        
    def dump(self):
        print 'dump all: name=',self.name, ' role=',self.role, ' salary=',self.salary
        print'            course,building=', self.course,self.building

################################
#     MAIN
################################
if __name__ == '__main__':
	pat = Staff601('Pat', 'Professor', 60)
	pat.dump()

	jim = Staff601('Jim', 'TA', 39)  
	jim.dump()
	print 'salute=', jim.salutation()

	exit()


 
