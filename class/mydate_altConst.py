class DateJan(object):
    def __init__(self, Year, Month, Day,**kwargs):
        print('M:cstr')
        self.year  = Year
        self.month = Month
        self.day   = Day
        for k, v in kwargs.items():
            print('arg :val',k,v)

    def __str__(self):
        return 'Date({}, {}, {})'.format(self.year, self.month, self.day)

    def set_date(self, y, m, d):
        self.year = y
        self.month = m
        self.day = d

    @classmethod
    def from_str(class_object, date_str):
        '''Call as
           d = Date.from_str('2013-12-30')
        '''
        print(class_object)
        year, month, day = map(int, date_str.split('-'))
        return class_object(year, month, day)

    @classmethod
    def test77(class_object, day):
        print('clsm=77')
        return class_object('1999','03', day)

    @classmethod
    def test88(class_object, args):
        print('clsm=88')
        return class_object('1999','03', '11',**vars(args))
