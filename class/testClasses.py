#! /usr/bin/python
class MyClass:
    """documentation for this class"""
    def f(self,a):
        return 'hello world',a

    def len(self):
        return 'len=r+i', self.r+self.i

    def __init__(self, realpart, imagpart): # constructor
        self.r = realpart
        self.i = imagpart


print 'Start testing classes'
mc = MyClass(4,8)

print 'mc.i=',mc.i
print 'mc.len()=',mc.len()
print 'mc.f("aa")=',mc.f("aa")
print 'mc.__doc__=',mc.__doc__

x=mc.f
print 'x=mc.f, x(4) -->',x(4)

print 'end of test1'

print 'use class as data structure'
class Employee:
    pass

john = Employee() # Create an empty employee record

# Fill the fields of the record
john.name = 'John Doe'
john.dept = 'computer lab'
john.salary = 1000

print 'john.name',john.name

class Bag:
    def __init__(self):
        self.data = []
    def add(self, x):
        self.data.append(x)
    def addtwice(self, x): # resue of own method
        self.add(x)
        self.add(x)
    def dump(self):
        print "len=",len(self.data)
        for i in self.data:
            print 'i=',i
    

bg=Bag()
bg.add('A')
bg.addtwice('B')
print 'bg=>',bg.dump()


# inheritance: DerivedCLass(BaseClass)
class Truck(Bag):
    maxLoad=66
    def __init__(self):
        Bag.__init__(self) # this calls constructor of base class , otherwise overwritten
        self.driver="jan"

T=Truck()
T.add("bread")
print 'T=>',T.driver, T.maxLoad,T.dump()
