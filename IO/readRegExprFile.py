#!/usr/bin/python

inpF='oakland-blades.inp'
print "reading dict from file=", inpF

arrayL = []
with open(inpF, "r") as inst:
    for line in inst:
        if line[0]=='#':
            continue
        arrayL.append(line.rstrip('\n')) 
        
print "Out of loop, nLines=",len(arrayL), ' 1st line=',arrayL[0]
# plain reading of lines , skipping #-lines is done

import reAsmbRegExpr
# expand names using regular syntax

for core in arrayL:
    #print core
    userstring=core
    fullL=reAsmbRegExpr.do(userstring)
    print fullL
        
