#!/usr/bin/python3
import json

#dict1 = {'Name': 'Zara', 'Age': 7, 'Class': 'First'};
dict1={'DD':{'a':1,'b':2},'EE':[4,5,6]}

print ("dump:", dict1)


outF='my_dict.json'
print ("write dict as json to file=",outF)

with open(outF, 'w') as f:
    json.dump(dict1, f)
