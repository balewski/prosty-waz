#!/usr/bin/python
import re
def do(userstring): 
    blockL=[ part.split(',') if i%2 else [part]  for i,part in enumerate(re.split(r'\[(.*?)\]',userstring)) ]
    #print 'INP: ',blockL
    if len(blockL)==1:  # nothing to expand
        return [userstring]
    while len(blockL)<3:
        blockL.append('')
    addresses = []
    for machine in blockL[1]:
 
        if machine.find('-')>0:
            r = machine.split('-')
            #print 'my r=',r, str.isdigit(r[0])

            if str.isdigit(r[0]):
                for m in xrange(int(r[0]),int(r[1])+1):
                    addresses.append("%s%02d%s"%(blockL[0][0],m,blockL[2][0]))
            else:
                #print 'r is char'
                for c in xrange(ord(r[0]), ord(r[1])+1):
                    #print chr(c) # resp. print unicode(c)
                    addresses.append("%s%c%s"%(blockL[0][0],c,blockL[2][0]))
        else:
            addresses.append("%s%s%s"%(blockL[0][0],machine,blockL[2][0]))
 
    #print 'OUT:',addresses
    return addresses

if __name__== '__main__':
    userstring='pc18[03,05-07,20]'
    userstring='pc18[1-4]'
    userstring='pc18[a-c]'
    fullL=do(userstring)
    print fullL
