#!/usr/bin/env python
from os import kill
from signal import alarm, signal, SIGALRM, SIGKILL
from subprocess import PIPE, Popen

#............................
#............................
#............................
class ShellCmdwTimeout(object):
    def __init__(self,args, cwd = None, shell = True, kill_tree = True, timeout = -1, env = None):
        '''
        Run a command with a timeout after which it will be forcibly
        killed.
        '''
        class Alarm(Exception):
            pass
        def alarm_handler(signum, frame):
            raise Alarm
        p = Popen(args, shell = shell, cwd = cwd, stdout = PIPE, stderr = PIPE, env = env)
        if timeout != -1:
            signal(SIGALRM, alarm_handler)
            alarm(timeout)
        try:
            stdout, stderr = p.communicate()
            if timeout != -1:
                alarm(0)
        except Alarm:
            pids = [p.pid]
            if kill_tree:
                pids.extend(self.get_process_children(p.pid))
            for pid in pids:
                # process might have died before getting to this line
                # so wrap to avoid OSError: no such process
                try: 
                    kill(pid, SIGKILL)
                except OSError:
                    pass
            self.ret=-9
            self. stdout=''
            self. stderr=''
            return

        self.ret=p.returncode
        self.stdout=stdout.decode('utf-8').split('\n')
        self.stderr=stderr.decode('utf-8').split('\n')

        
    def get_process_children(self,pid):
        p = Popen('ps --no-headers -o pid --ppid %d' % pid, shell = True,
              stdout = PIPE, stderr = PIPE)
        stdout, stderr = p.communicate()
        return [int(p) for p in stdout.split()]

if __name__ == '__main__': 
    cmd='ls -l; sleep 4; ls aa'
    print('cmd="',cmd, '", A w/ timeout, B w/o ...')
    out=ShellCmdwTimeout(cmd, timeout = 3)
    print('A= ret', out.ret, out.stdout,out.stderr)
    out=ShellCmdwTimeout(cmd)
    print('B=ret', out.ret, out.stdout,out.stderr)
    
