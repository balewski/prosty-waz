#!/usr/bin/env python3
''' needs those module
on Cori:
  module load python/3.5-anaconda
on PDSF:
  module load python/3.4.3

'''
import sys
(va,vb,vc,vd,ve)=sys.version_info ; assert(va==3)  # needes Python3


inpF='home.templ'

def replace_words(base_text, device_values):
    for key, val in device_values.items():
        base_text = base_text.replace(key, val)
    return base_text
 
# Here I'll create an empty dictionary, and prompt the user to enter in the values
 
device = {}
 
device["$hostname"] = 'aaHost'
device["$ip"] = '123Add'
device["<answer>"] = 'ccYes'
 
# Open your desired file as 't' and read the lines into string 'tempstr'
 
t = open(inpF, 'r')
tempstr = t.read()
t.close()
 
print('tempstr=',tempstr)
 
# Using the "replace_words" function, we'll pass in our tempstr to be used as the base, 
# and our device_values to be used as replacement.  
 
output = replace_words(tempstr, device)
 
# Write out the new config file
outF=inpF.replace('templ','out')
fout = open(outF, 'w')
fout.write(output)
fout.close()

print(' exec:  cat ',outF)
