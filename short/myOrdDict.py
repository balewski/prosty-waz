#!/usr/bin/env python
# A Python program to demonstrate working of key
# value change in OrderedDict
from collections import OrderedDict
 
print("Before:")
od = OrderedDict()
od['a'] = 99
od['x'] = 2
od['c'] = 3
od['y'] = 4
for key, value in od.items():
    print(key, value)
 
print("\nAfter:")
od['c'] = 5
for key, value in od.items():
    print(key, value)

print('size',len(od),list(od))

print("\nAfter deleting 'c':")
od.pop('c')
for key, value in od.items():
    print(key, value)
 
print("\nAfter re-inserting 'c':")
od['c'] = 3
for key, value in od.items():
    print(key, value)

print("\nprint using enum")
for i,k in enumerate(od):
    print(i,k,od[k])
