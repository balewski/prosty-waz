#!/usr/bin/env python
import sys

print(" alternatives for hashing string")
# based on https://www.pythoncentral.io/hashing-strings-with-python/

import hashlib

# this is avaliable:
# print(hashlib.algorithms_available)

print('this is present in the hashlib  module')
print(hashlib.algorithms_guaranteed)

hao = hashlib.md5(b'Hello World')
print('\nmd5:',hao.hexdigest())

hash_object = hashlib.sha1(b'Hello World')
hex_dig = hash_object.hexdigest()
print('\nsha1:',hex_dig)

myInp = input('\nEnter String to hash: ')
# Assumes the default UTF-8
hao = hashlib.md5(myInp.encode())
print('my input=%s --> md5:'%myInp,type(myInp),hao.hexdigest())
