#!/usr/bin/env python3
# use 2^k list growth to speed up list.append() 

import sys
import numpy as np
import time

class BigItem: # big objects in memory
    def __init__(self,seed,myLen=10000):
        self.data=np.full(myLen,seed).astype('float32')


nArg=len(sys.argv)-1
print ('Number of arguments:', nArg)
if nArg !=1:
    print('Argument List:', str(sys.argv[1:]))
    print("usage %s numItem "%(sys.argv[0]))
    exit(1)
    
numItem=int(sys.argv[1])

one=BigItem(33)
oneSize=sys.getsizeof(one)+sys.getsizeof(one.data)
print('one size=%.1f kB'%(oneSize/1024.))

# ............ 
print('Append %d items, conventional list.append()'%(numItem))
L=[]
T0=time.time()
for i in range(numItem):
    L.append(BigItem(i))
    if i%1000==0: 
        T1=time.time()
        print('added %d , elaT=%.1f min'%(i,T1-T0))

# test
i=numItem//2
print('data[%d]:'%i,L[i].data[:10])

