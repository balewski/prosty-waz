#!/usr/bin/env python
import yaml
from pprint import pprint

dataD = dict(
    A = 'a',
    B = dict(
        C = 'c',
        D = 'd',
        E = 'e',
    ),
    L=[1.2, 3.4, 6.8]
)

# Setting up some example data
d2 = {'name': 'A Project',
     'version': {'major': 1, 'minor': 4, 'patch': 2},
     'add-ons': ['foo', 'bar', 'baz']}

ymlF='out1.yml'
with open(ymlF, 'w') as outfile:
    yaml.dump(dataD, outfile)
print ('saved ',ymlF, ' default style')

ymlF='out2.yml'

with open(ymlF, 'w') as outfile:
    yaml.dump(d2, outfile, default_flow_style=False)
print ('saved ',ymlF,' default_flow_style=False')

ymlF='cosmoMeta.yaml'
print ("read YAML from ",ymlF,' and pprint it:')
inpFile=open(ymlF)
docs = yaml.load(inpFile)
pprint(docs)

xx=docs['zRedShift'][0]
print('xx',xx, type(xx))




