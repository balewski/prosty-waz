#!/usr/bin/env python3

''' note, if you want pass func by name do one of those:

def doIt(func, *args):
   func_dict = {'dork1':dork1,'dork2':dork2}
   result = func_dict.get(func)(*args)
   return result
# use:
func = 'dork2'
ned = doIt(func,3, 4, 9)

'''

#...!...!..................
def run_func(func, *args):
   result = func(*args)
   return result

#...!...!..................
def dork1(var1, var2, var3):
   thing = (float(var1 + var2) / var3)
   return thing

#...!...!..................
def dork2(var1, var2, var3):
   thing = float(var1) + (float(var2) / var3)
   return thing

def condF(x):
    print('condF called, x=', x)
    return x=='7'

#=================================
#  M A I N 
#=================================

#This can be run as follows:

func = dork1
ned = run_func(func,3, 4, 9)
print('dork:', ned)

binVal=run_func( condF, '6')
print('binV:', binVal)
