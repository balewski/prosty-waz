#!/usr/bin/env python3

# works with GMT  and has time zones, easier compute time differences

# to avoid time-format hell lets use this convention
from dateutil import tz
from dateutil.parser import parse as parse_date
from datetime import datetime
import pytz

#...!...!..................
def dateNowUtc():  # offset-aware datetimes
    nowT = datetime.utcnow().replace(tzinfo=pytz.utc)
    return nowT
    
#...!...!..................
def dateT2Str(xT):  # --> string
    nowStr=xT.strftime("%Y-%m-%d_%H:%M:%S_%Z")
    return nowStr

#...!...!..................
def dateStr2T(xS):  #  --> datetime
    nowStr=xS.replace('_',' ')
    dt = parse_date(nowStr)
    #print("dt=",dt, 'tmz=',dt.tzinfo)
    return dt

#=================================
#  M A I N - E X E C 
#=================================

if __name__ == '__main__':
    print('testing date util')

    nowT=dateNowUtc()  #  offset-aware datetimes
    strT=dateT2Str(nowT) ; print('now str:',strT)
    nowT2=dateStr2T(strT); print("nowT2=",nowT2, 'tmz=',nowT2.tzinfo)
    print('diff=',nowT - nowT2 , 'below 1 sec?')

