#!/usr/bin/env python
'''  median for 1D array

based on
https://www.statology.org/confidence-interval-for-median/
'''
import scipy.stats as stats
import numpy as np

X = [8, 11, 12, 13, 15, 17, 19, 20, 21, 21, 22, 23, 25, 26, 28]
X = np.array(X)
print('len', len(X), 'X:', X)
m1 = np.median(X)
print('NUMPY: median m1=', m1)

#...!...!..................
def median_conf_V(data,p=0.68):  # vectorized version
    # computes median vs. axis=0, independent sorting of every 'other' bin
    # returns : axis=0: m, m-std, m+std; other axis 'as-is'
    sdata=np.sort(data,axis=0)
    N = data.shape[0]
    delN=int(N*p)
    lowCount=(N-delN)//2
    upCount =(N+delN)//2
    #print('MED:idx:', lowCount, upCount, N)
    out=[sdata[N // 2],sdata[lowCount], sdata[upCount]]
    return  np.array(out)

#...!...!..................
def median_conf(data,p=0.68):
    # returns : m, m-std, m+std
    # flat to one dimension array
    data = data.reshape(-1)
    data = np.sort(data)
    N = data.shape[0]

    delN=int(N*p)
    
    lowCount=(N-delN)//2
    upCount =(N+delN)//2
    print('idx:', lowCount, upCount, N)
    #print('data sorted', data)

    return data[lowCount], data[N // 2], data[upCount]


print('scipy X len :',X.shape,'m,  m-std, m+std:', median_conf(X, 0.68))


# test w/ 1D gassian
Y = np.random.normal(loc=10.5, scale=1.2, size=101)
print('scipy Y len :',Y.shape,'m,  m-std, m+std:', median_conf(Y, 0.68))
m2 = np.median(Y)
print('NUMPY: median m2=', m2)
