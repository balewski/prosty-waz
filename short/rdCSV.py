#!/usr/bin/env python
import sys
import csv

#............................
def read_one_csv(fname):
    print('read_one_csv:',fname)
    tabL=[]
    with open(fname) as csvfile:
        drd = csv.DictReader(csvfile)
        print('see %d columns'%len(drd.fieldnames),drd.fieldnames)
    
        for row in drd:
            tabL.append(row)

    print('got %d rows'%(len(tabL)))
    print('LAST:',row)
    return tabL,drd.fieldnames

#............................
def write_one_csv(fname,rowL,colNameL):
    print('write_one_csv:',fname)
    print('export %d columns'%len(colNameL), colNameL)
    with open(fname,'w') as fou:
        dw = csv.DictWriter(fou, delimiter='\t', fieldnames=colNameL)
        dw.writeheader()
        for row in rowL:
            dw.writerow(row)    

#=================================
#=================================
#  M A I N 
#=================================
#=================================

tabL,keyL=read_one_csv('janTest6.csv')
myDtn='dtn01'

for row in tabL:
    #print('R:',row)
    if row['target'] == myDtn:
        print(row)
        for k in row.keys():
            print(k,row[k])
        break

write_one_csv('out.csv',tabL,keyL)
    
