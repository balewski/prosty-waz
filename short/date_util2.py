#!/usr/bin/env python3

# to avoid time-format hell lets use this convention
# works with localtime and has time zones
from datetime import datetime
import time

#...!...!..................
def dateT2Str(xT):  # --> string
    nowStr=time.strftime("%Y-%m-%d_%H:%M:%S_%Z",xT)
    return nowStr

#...!...!..................
def dateStr2T(xS):  #  --> datetime
    yT = time.strptime(xS,"%Y-%m-%d_%H:%M:%S_%Z")
    #print("yT=",yT)
    return yT

#=================================
#  M A I N - E X E C 
#=================================

if __name__ == '__main__':
    print('testing date util')

    nowT1=time.localtime()
    print("nowT1=",nowT1, nowT1.tm_zone)
    strT2=dateT2Str(nowT1) ; print('now str:',strT2)
    nowT3=dateStr2T(strT2) # convert back
    print("nowT3=",nowT3, nowT3.tm_zone)
    time.sleep(1)
    nowT4=time.localtime()
    delTsec=time.mktime(nowT4) - time.mktime(nowT1)
    print('diff=%.3f'%(delTsec) , 'below 1 sec?', type(delTsec))
    print('T4=',time.mktime(nowT4),time.time() )
