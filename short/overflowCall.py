import numpy as np

def err_handler(type, flag):
    print("Floating point error (%s), with flag %s" % (type, flag))

saved_handler = np.seterrcall(err_handler)
save_err = np.seterr(all='call')
#np.seterrcall(saved_handler)
#np.seterr(**save_err)



# Demonstrate my func can detect overflow
print('\nJ: it will detect overflow for float')
np.array([1, 0, 3]) / 0.0


print('\nJ: Demonstrate overflow is **undetected**')
A = np.zeros((100,100), np.uint8)
A[3,4] = 255 # Max value, setting up next step in example
print('J: before',A[3,4])
A[3,4] += 1 # Silently overflows
print('J: after',A[3,4],A.dtype)


print('\n --- acceptable solution - temporarily increase capacity, use np.clip, next cast back')
A[3,4] = 255
B=A.astype(np.int16)
print('J: before B',B[3,4])
B[3,4] += 1 # Silently overflows
print('J: after B',B[3,4],B.dtype)

Bc=np.clip(B,0,255)
C=Bc.astype(np.uint8)
print('J: after C',C[3,4],C.dtype)
