#!/usr/bin/env python3
''' needs those module
on Cori:
  module load python/3.5-anaconda
on PDSF:
  module load python/3.4.3

'''
import sys
(va,vb,vc,vd,ve)=sys.version_info ; assert(va==3)  # needes Python3

import argparse
parser = argparse.ArgumentParser()

#-db DATABSE -u USERNAME -p PASSWORD -size 20
parser.add_argument("-db", "--hostname", help="Database name")
parser.add_argument("-u", "--username", help="User name")
parser.add_argument("-p", "--password", help="Password")
parser.add_argument("-size", "--size", help="Size", type=int)
parser.add_argument('-trig', action='append', dest="bbb")
args = parser.parse_args()

print( "Hostname {} User {} Password {} size {} t {}".format(
        args.hostname,
        args.username,
        args.password,
        args.size,
        args.bbb
        ))

# for list types see:
# http://stackoverflow.com/questions/15753701/argparse-option-for-passing-a-list-as-option

argD=vars(args)

for key in argD:
   print('key=',key, ' val=',argD[key])
