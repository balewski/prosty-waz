#!/usr/bin/env python3
import configparser
from pprint import pprint
configFile = 'myConf.txt'
print('input:',configFile)

cfg = configparser.RawConfigParser()   
cfg.optionxform = str  # make option names case sensitive
cfg.read(configFile)

print('sections:',cfg.sections())
for sectN in cfg.sections():
    sect=cfg[sectN]
    print('\nsubs:',sect)
    for k,v in sect.items():
        print(k,v)

# add new section
cfg['bitbucket.org'] = {}
cfg['bitbucket.org']['User'] = 'hg'

outF='myConf.out'
print('\nM:saving;',outF)
with open(outF, 'w') as fd:
    cfg.write(fd)
