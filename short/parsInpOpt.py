#!/usr/bin/env python3
''' needs those module
on Cori:
  module load python/3.5-anaconda
on PDSF:
  module load python/3.4.3

'''
import sys
(va,vb,vc,vd,ve)=sys.version_info ; assert(va==3)  # needes Python3

import sys, getopt

def main(argv):
   inputfile = 'fixMe1'
   outputfile = 'fixMe2'
   try:
      opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
   except getopt.GetoptError:
      print ('test.py -i <inputfile> -o <outputfile>')
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print ('test.py -i <inputfile> -o <outputfile>')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         inputfile = arg
      elif opt in ("-o", "--ofile"):
         outputfile = arg
   print ('Input file is', inputfile)
   print ('Output file is', outputfile)

if __name__ == "__main__":
   main(sys.argv[1:])
