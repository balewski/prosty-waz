#!/usr/bin/env python
import sys
(va,vb,vc,vd,ve)=sys.version_info ; assert(va==3)  # needes Python3

mxRec=10
plPage=1

print ('Number of arguments:', len(sys.argv)-1)
print ('Argument List:', str(sys.argv[1:]))
print ("usage %s [numRec=%d] [page=%d]"%(sys.argv[0],mxRec, plPage))

if  len(sys.argv)>1:
    mxRec=int(sys.argv[1])
    
if  len(sys.argv)>2:
    plPage=int(sys.argv[2])

print ("use numRec=%d page=%d"%(mxRec, plPage))
