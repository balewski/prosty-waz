#!/usr/bin/env python3
import sys
(va,vb,vc,vd,ve)=sys.version_info 
print("Python ver=",(va,vb,vc,vd,ve)); assert(va==3)  # needes Python3

print(" convert arbitrary string to datetime variable")

from dateutil.parser import parse as parse_date
dt = parse_date("Aug 28 1999 12:00AM")
print("dt1=",dt, ' type=',type(dt))

dtStr='Wed Dec 24 10:04:51 2016'
dt = parse_date(dtStr)
print("dt=",dt, 'tmz=',dt.tzinfo)

print('\nnow add cpeciffic different timezone')
from dateutil import tz
tzinfos = {"CDT": tz.gettz('US/Central'),'PCT': tz.gettz('US/Pacific')}

dt3 = parse_date(dtStr+' CDT', tzinfos=tzinfos)
print("dt3=",dt3, 'tmz=',dt3.tzinfo)
dt4 = parse_date(dtStr+' PCT', tzinfos=tzinfos)
print('del PCT-CDT=',dt4-dt3)


import datetime
nowT = datetime.datetime.now()

print ('just year for nowT=',nowT.year)
nowStr=nowT.strftime("%B %d %Y %I:%M%p")
print(nowStr,'tmz=',nowT.tzinfo )

print('convert now to CDT')
dt5 = parse_date(nowStr+' CDT', tzinfos=tzinfos)
print("now2=",dt5, 'tmz=',dt5.tzinfo)


print('\n timzeone-aware dt')
import pytz
from datetime import datetime
nowT2=datetime.utcnow().replace(tzinfo=pytz.utc)
print(nowT2,'tmz=',nowT2.tzinfo )
delT=nowT2-dt3
print('time dif=',delT)

nowStr=nowT2.strftime("%Y-%m-%d_%H:%M:%S-%Z")
print(nowStr)

