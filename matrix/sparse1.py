#!/usr/bin/env python
import sys
(va,vb,vc,vd,ve)=sys.version_info ; assert(va==3)  # needes Python3
import numpy as np

'''
A dense matrix stored in a NumPy array can be converted into a sparse matrix using the CSR representation by calling the csr_matrix() function.
https://machinelearningmastery.com/sparse-matrices-for-machine-learning/


https://docs.scipy.org/doc/scipy/reference/sparse.html

https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.csr_matrix.html

'''

from scipy.sparse import csr_matrix
# create dense matrix
A = np.array([[1, 0, 0, 1, 0, 0], [0, 0, 2, 0, 0, 1], [0, 0, 0, 2, 0, 0]])
print('\ndens A:',type(A),A)

# convert to sparse matrix (CSR method)
S = csr_matrix(A)
print('\n sparse S:',type(S),S)

# reconstruct dense matrix
B = S.todense()
print('\ndens B:',type(B),B)


# calculate sparsity
sparsity = 1.0 - np.count_nonzero(A) / A.size
print('A sparsity=%.3f'%sparsity, A.shape)

# play w/ scipy sparse arrays

S1=csr_matrix((3, 4), dtype=np.int8)
print('\n null S1 :',S1,' (prints nothing)',S1.size,' shape=',S1.shape,'\n S1 as dense:\n',S1.toarray())

# define SM in scr-format
row = np.array([0, 0, 1, 2, 2, 2])
col = np.array([0, 2, 2, 0, 1, 2])
data = np.array([1, 2, 3, 4, 5, 6])
S2=csr_matrix((data, (row, col)), shape=(3, 3))
print('\n  S2 :\n',S2,' size=',S2.size,' shape=',S2.shape,'\n S2 as dense:\n',S2.toarray())
print('num. stored zeros S2.nnz:',S2.nnz,',S2.has_sorted_indices=',S2.has_sorted_indices)

''' There is zillion of methods:

__len__()
__mul__(other) interpret other and call one of the following
arcsin() Element-wise arcsin.
argmax([axis, out]) Return indices of maximum elements along an axis.
....

https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.csr_matrix.html
'''






