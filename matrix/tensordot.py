#!/usr/bin/env python
import numpy as np
from pprint import pprint

''' axes: integer_like If an int N, sum over the last N axes of a and the first N axes of b in order. The sizes of the corresponding axes must match.
'''
a = np.arange(60.).reshape(5,3,4)
b = np.arange(8.).reshape(4,2)
c = np.tensordot(a,b, axes=1)
print('c',c.shape)
