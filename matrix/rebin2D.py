#!/usr/bin/env python
import numpy as np
from pprint import pprint

'''
rewbin 2D array
'''
def rebin2d(cube):
        nb=2
        a=cube.shape[0]
        assert a%nb==0
        b=cube.shape[1]
        assert b%nb==0
        print('aaa',a,b,nb)
        sh = a//nb,nb,b//nb,nb
        B=cube.reshape(sh)
        print('Bsh',B.shape)
        pprint(B)
        C=np.sum(B,axis=-1)
        print('Csh',C.shape)
        pprint(C)
        D=np.sum(C,axis=-2)
        print('Dsh',D.shape)
        pprint(D)


# create dense matrix
A = np.array([[1, 2, 3, 4, 5, 6], [7, 8, 9, 10, 11, 12],[21, 22, 23, 24, 25, 26], [37, 38, 39, 30, 31, 32]])
print('\ndens A:',type(A),A.shape)
pprint(A)

rebin2d(A)
