#!/usr/bin/env python
import sys
(va,vb,vc,vd,ve)=sys.version_info ; assert(va==3)  # needes Python3

import numpy as np
from numpy import linalg as LA
from pprint import pprint

if  len(sys.argv)>1:
    myDtn=sys.argv[1]

# 2 features, 3 measurements, opposite correlation
x = np.array([[0, 2], [1, 1], [2, 0.3]]).T
print('x dims:',x.shape,'\n dump x:')
pprint(x)

# compute cov matrix
c=np.cov(x)
print('cov M:')
pprint(c)
c1=np.diagonal(c)
c2=np.sqrt(c1)
print('sigmas:',c2)

print('correl matrix')
r=np.corrcoef(x)
print('cor: 01=%.3f'%(r[0][1]) )
pprint(r)


# find eignevalueas
'''Returns:	
w : (..., M) ndarray

The eigenvalues in ascending order, each repeated according to its multiplicity.

v : {(..., M, M) ndarray, (..., M, M) matrix}

The column v[:, i] is the normalized eigenvector corresponding to the eigenvalue w[i]. Will return a matrix object if a is a matrix object.
'''
w, v = LA.eigh(c)
print('eig vals shape',w.shape)
pprint(w)
print('eig vects shape',v.shape)
pprint(v)

print('\n confirm eig-vect diogonalizes cov for input data')
xr=np.matmul(LA.inv(v),x)
print('xr dims:',x.shape,'\n dump xr:')
pprint(xr)

# compute cov matrix
cr=np.cov(xr)
print('cov Mr:')
pprint(cr)
