/* 
On Cori do:

module load cray-hdf5   

cc test_chunking.c

based on
 https://support.hdfgroup.org/HDF5/doc/Advanced/Chunking/index.html  

*/


#include "hdf5.h" 
int main(void) {
    hid_t   file_id, dset_id, fspace_id, space_id, dcpl_id;
    hsize_t chunk_dims[2] = {10, 1};
    hsize_t dset_dims[2] = {10, 10};
    hsize_t mem_dims[1] = {5};
    hsize_t start[2] = {3, 2};
    hsize_t count[2] = {5, 1};
    int     buffer[5];

    /* Create the file */
    file_id = H5Fcreate("file.h5", H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

    /* Create a dataset creation property list and set it to use chunking
     * with a chunk size of 10x1 */
    dcpl_id = H5Pcreate(H5P_DATASET_CREATE);
    H5Pset_chunk(dcpl_id, 2, chunk_dims);

    /* Create the dataspace and the chunked dataset */
    space_id = H5Screate_simple(2, dset_dims, NULL);
    dset_id = H5Dcreate(file_id, "myDataset55", H5T_NATIVE_INT, space_id, dcpl_id, H5P_DEFAULT);

    /* Select the elements from 3, 2 to 7, 2 */
    H5Sselect_hyperslab(fspace_id, H5S_SELECT_SET, start, NULL, count, NULL);

    /* Create the memory dataspace */
    mspace_id = H5Screate_simple(1, mem_dims, NULL);

    /* Write to the dataset */
    buffer = H5Dwrite(dset_id, H5T_NATIVE_INT, mspace_id, fpsace_id, H5P_DEFAULT, buffer);

    /* Close */
    H5Dclose(dset_id);
    H5Sclose(fspace_id);
    H5Sclose(mspace_id);
    H5Pclose(dcpl_id);
    H5Fclose(file_id);
    return 0;
}

