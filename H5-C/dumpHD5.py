#!/usr/bin/env python3
""" 
test validity of output files 
Can handle an array of variable strings

on Cori:
 module load tensorflow/intel-2.0.0-py37

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from pprint import pprint
import h5py
import time

#...!...!..................
def read_data_hdf5(inpF,verb=1):
    if verb>0:
            print('read data from hdf5:',inpF)
            start = time.time()
    h5f = h5py.File(inpF, 'r')
    objD={}
    for x in h5f.keys():
        if verb>1: print('item=',x,type(h5f[x]),h5f[x].shape,h5f[x].dtype)
        if h5f[x].dtype==object:
            obj=[]
            for i in range(h5f[x].shape[0]):
                obj.append(h5f[x][i].decode("utf-8"))
                #print('bbb',type(obj),obj.dtype)
                if verb>0: print('read str:',i,x,len(obj[i]),type(obj[i]))
        else:
            obj=h5f[x][:]
            if verb>0: print('read obj:',x,obj.shape,obj.dtype)
        objD[x]=obj
    if verb>0:
            print(' done h5, num rec:%d  elaT=%.1f sec'%(len(objD),(time.time() - start)))

    h5f.close()

    return objD


import argparse

#=================================
#=================================
#  M A I N 
#=================================
#=================================

print('\n ---- dump H5 ----')
inpF='epiCastOut.h5'
#inpF='vlstra.h5'
blob=read_data_hdf5(inpF,verb=1)
for k in blob:
    ds1=blob[k]
    print(k,':',ds1)
