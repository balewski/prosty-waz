/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5.  The full HDF5 copyright notice, including     *
 * terms governing use, modification, and redistribution, is contained in    *
 * the COPYING file, which can be found at the root of the source code       *
 * distribution tree, or in https://support.hdfgroup.org/ftp/HDF5/releases.  *
 * If you do not have access to either file, you may request a copy from     *
 * help@hdfgroup.org.                                                        *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* 
 *  This example illustrates how to write and read data in an existing
 *  dataset.  It is used in the HDF5 Tutorial.

On Cori:
 module load cray-hdf5
 

 */

#include <assert.h>
#include "hdf5.h"
#define FILE "dset.h5"

int main() {

   hid_t       file_id, dataset_id;  /* identifiers */
   herr_t      status;
   int         i, j, dset_data[4][6];

   /* Initialize the dataset. */
   for (i = 0; i < 4; i++)
      for (j = 0; j < 6; j++)
         dset_data[i][j] = i * 6 + j + 1;

   /* Open an existing file. */
   printf("inp File=%s\n",FILE);

   //file_id = H5Fcreate (FILE, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);


   file_id = H5Fopen(FILE, H5F_ACC_RDWR, H5P_DEFAULT);
   printf("opened, %p\n",file_id);
   assert(file_id!=-1);

   /* Open an existing dataset. */
   char *dsetName="/dsetJ";
   printf(" Open  EMPTY  dataset named=%s\n",dsetName);

   dataset_id = H5Dopen2(file_id, dsetName , H5P_DEFAULT);
   printf("found data set, %p\n",dataset_id);
   assert(dataset_id!=-1);

   /* Write the dataset. */
   status = H5Dwrite(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, 
                     dset_data);

   printf("wrote data set, %d\n",status);
   assert(status!=-1);
   status = H5Dread(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, 
                    dset_data);
   printf("read data set, %d\n",status);
   assert(status!=-1);

   /* Close the dataset. */
   status = H5Dclose(dataset_id);
   printf("closed set, %d\n",status);
   assert(status!=-1);

   /* Close the file. */
   status = H5Fclose(file_id);
   printf("closed file, %d\n",status);
   assert(status!=-1);
}
