#include "hdf5.h"
extern unsigned short * g_hd5_buff;
//extern int g_ndays;
void setup_hd5_buffer(unsigned int mxId,int nVar);
void push_day_hd5(int currentDay, int *Sick, int *Pop, int *Inc, int *Pro, int *Pox); // not used in demo
void save_epicast_h5(char *h5Name, char *textRec1,char *textRec2);
void put_1D_vector(int iz,int iy,int *vect); 
char *argv_to_text(int argc, char **argv);
