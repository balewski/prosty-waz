/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

On Cori:
 module load cray-hdf5
  cc rdH5_to_LZdist.c 
 ./a.out

 */

#include <assert.h>
#include <string.h>
#include <malloc.h>
#include <hdf5.h>


/* taken from
Lempel-Ziv complexity
Otu & Sayood, Bioinformatics (2003), 19(16): p.2122-2130

Code base source:
http://bioinformatics.org.au/tools/decaf+py/
*/
 
bool LZ_is_reproducible(char *seq, int index, int hist_len, int *last_match){

  int hist_start = index - hist_len;

  int last_match0=*last_match; // just in case
  for (int i=last_match0; i< hist_start; i++){
    //j == hist_len in last iteration
    int j0;
    for(int j=0; j< hist_len+1; j++){
      j0=j;
      //printf("i=%d j=%d  %c %c \n",i,j, seq[i+j], seq[hist_start+j]);
      if ( seq[i+j] != seq[hist_start+j] )  break;
    }
    //printf("Q i=%d, j0=%d hil=%d\n",i,j0, hist_len);
    if (j0 == hist_len ){
      // returns index of new last_match
      *last_match=i;
      return true;
    }
    
  }
  *last_match=0;
  return false;
}


int LZ_complexity(char *seq) {
  int start = 2;
  int length = strlen(seq);
  if (length < start ) {
    // here, complexity == length
    return length;
  }

  int  complexity = 2;
  int  hist_len = 1;
  int  pos = start;
  int  last_match = 0;

  /* seq[index] == next symbol */
  for (int index=pos; index< length; index++){
    bool is_repro = LZ_is_reproducible( seq, index, hist_len, &last_match);
    //printf("LZ idx=%d C=%c hil=%d lstm=%d isR=%d cmplx=%d\n", index, seq[index], hist_len, last_match,is_repro,complexity);
    if (is_repro) {
       hist_len += 1;
    }  else {    
       hist_len = 1;
       complexity += 1;
    }
  }
  //printf("LZ end: %d len=%d\n",complexity, length);
  return complexity;
}




int main() {

   hid_t       file_id, dataset_id;  /* identifiers */
   herr_t      status;


   char *seqS = "AACGTACCATTG";
   char *seqR = "CTAGGGACTTAT";
   char *seqQ = "ACGGTCACCAA";

   char *seqSR = "AACGTACCATTGCTAGGGACTTAT";
   char *seqRS = "CTAGGGACTTATAACGTACCATTG";
   char *seqSQ = "AACGTACCATTGACGGTCACCAA";
   char *seqQS = "ACGGTCACCAAAACGTACCATTG";
   
   char * seq=seqQS;
   int lzc=LZ_complexity(seq);
   printf("c-code LZC =%d str=%s=\n",lzc,seq);



   /* Open an existing file. */
   char * inpF="data/genomePlen_seg0.hd5";
   printf("inp File=%s\n",inpF);

   file_id = H5Fopen(inpF, H5F_ACC_RDWR, H5P_DEFAULT);
   printf("opened, %d\n",file_id);
   assert(file_id!=-1);

   if (1) {
     printf("Explore what data sets exist\n");
     H5G_info_t  ginfo;
     hid_t   group=file_id;
     status = H5Gget_info (group, &ginfo);
     assert(status!=-1);
     printf("Traversing group using alphabetical %d indices:\n",ginfo.nlinks);
     int i;
     for (i=0; i<ginfo.nlinks; i++) {
       // Get size of name, add 1 for null terminator.
       ssize_t  size = 1 + H5Lget_name_by_idx (group, ".", H5_INDEX_NAME, H5_ITER_INC,i, NULL, 0, H5P_DEFAULT);
       // Allocate storage for name.
       char *name = (char *) malloc (size);
       // Retrieve name, print it, and free the previously allocated space.
       size = H5Lget_name_by_idx (group, ".", H5_INDEX_NAME, H5_ITER_INC, i, name, (size_t) size, H5P_DEFAULT);
       printf ("Index %d: %s\n", (int) i, name);
       free (name);
     }
   }

   /* Open an existing dataset. */
   char *dsetName="1026080_Prodigal_proteins/amino_seq";
   printf(" Open   dataset named=%s\n",dsetName);

   dataset_id = H5Dopen2(file_id, dsetName , H5P_DEFAULT);
   printf("found data set,%d\n",dataset_id);
   assert(dataset_id!=-1);

   hid_t memtype1 = H5Dget_type(dataset_id);
   printf("data set type, %d\n",memtype1);
   assert( memtype1!=-1);

   hid_t dspace1 = H5Dget_space(dataset_id);
   printf("data set space, %d\n",dspace1);
   assert( dspace1 !=-1);
   const int ndims1 = H5Sget_simple_extent_ndims(dspace1);
   printf("data set ndims, %d\n",ndims1);
   assert(ndims1>0);
   hsize_t dims1[ndims1];
   H5Sget_simple_extent_dims(dspace1, dims1, NULL);
   printf("data set dims, %d\n",dims1[0]);
   
   assert ( ndims1==1);
   const int nProt=dims1[0];  // it will be 1605
   char *dset_data[nProt];

 
   // HERE I NEED TO PROVIDE CORRECT STRUCT TO HOLD 1605 strings 

   status = H5Dread(dataset_id, memtype1, H5S_ALL, H5S_ALL, H5P_DEFAULT, dset_data);
   printf("read data set, %d\n",status);
   assert(status!=-1);

   printf("one protein =%s=\n",dset_data[0]);

   const int mxStr=20;
   char outS[mxStr+1];
   for (int i=0; i<nProt; i++) {
     char *inpS=dset_data[i] ;    
     int lzc=LZ_complexity(inpS);
     continue;
     strncpy(outS,inpS,mxStr); outS[mxStr]=0; // add end-of-string manually
     printf("i=%d len=%d lzc=%d first=%s=\n",i,strlen(inpS),lzc, outS);
     //if ( i > 10 ) break;
   } 


   /* Close the dataset. */
   status = H5Dclose(dataset_id);
   printf("closed set, %d\n",status);
   assert(status!=-1);

   /* Close the file. */
   status = H5Fclose(file_id);
   printf("closed file, %d\n",status);
   assert(status!=-1);
}
