/* unit test  of EpiData_to_hd5

create fixed  (4,3,6)=(x,y,t) data set fill it with '0'
- write a plane (x,y) at  iT-position fill it with '1+x+2*y^2'
- loop over t from [0...5] 

Compile with plain C-compiler:
 module load cray-hdf5
 cc test_epicastData_hd5.c EpiCastData2hd5.c 

Execute:  ./a.out
Print HD5 output: epiFab.h5

 module load tensorflow/intel-2.0.0-py37
./dumpHD5.py
*/

#include <stdlib.h>
#include "EpiCastData2hd5.h"

//=======================
//=======================
//=======================
unsigned int Nunit, *myID;

int main (int argc, char **argv) {

  int   nVar=3;
  unsigned int mxId=3;

  //g_ndays=nDay+1; // needed for hd5 output initialization

  if (g_hd5_buff==0  ){
    setup_hd5_buffer(mxId,nVar);
    printf("Jan g_hd5_buff=%p  \n",g_hd5_buff);
  }

  printf("fill some records w/ data\n");
  int *vec=malloc(nVar*sizeof(int));
  int iy=1;
  for (int ix=0; ix<= mxId; ix++) {
    vec[ix]=1+ix+2*iy*iy+1e6;
    printf("iy=%d ix=%d val=%d\n",iy,ix,vec[ix]);
  }

  int iDay=3;
  put_1D_vector(iDay,iy,vec);
  
  save_epicast_h5("epiCastOut.h5","very long string ABC123","other long string FFF567");
  char *txt=argv_to_text(argc, argv);
  printf("M:done, txt=%s=\n",txt);
}
