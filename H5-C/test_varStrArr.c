/*

Compile with plain C-compiler:
 module load cray-hdf5

 cc test_varStrArr.c

based on
https://support.hdfgroup.org/ftp/HDF5/examples/misc-examples/attrvstr.c
*/

#include <hdf5.h>
#include <stdlib.h>

#define FILE        "vlstra.h5"

int verbosity = 1;
int num_errs = 0;
#define FAIL -1

//#define EIP 

static void test_write_vl_string_attribute(void)
{
    hid_t file, root, dataspace, att;
    hid_t type;
    herr_t ret;
    char *string_att[2] = {"This is array of varied strings",
                                 "It has two strings"};
    hsize_t   dims[1] = {2}; // how many elements in the string array to write

    file = H5Fcreate(FILE, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

    /* Create a datatype to refer to. */
    type = H5Tcopy (H5T_C_S1);
    //H5Tset_strpad(type, H5T_STR_SPACEPAD);
    ret = H5Tset_size (type, H5T_VARIABLE);
    //root = H5Gopen(file, "/",H5P_DEFAULT);
    dataspace = H5Screate_simple(1, dims, NULL);
    att = H5Dcreate2(file, "test_string_array", type, dataspace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    ret = H5Dwrite(att, type, H5S_ALL, H5S_ALL, H5P_DEFAULT, &string_att);
    ret = H5Dclose(att);
    //ret = H5Gclose(root);
    ret = H5Tclose(type);
    ret = H5Sclose(dataspace);
    ret = H5Fclose(file);
    return;
}



int main (int argc, char *argv[])
{
      test_write_vl_string_attribute();
      //test_read_vl_string_attribute();
      return 0;
}     

