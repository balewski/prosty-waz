/* Description of hd5 format for epiCast

3D array of ushort [day][var][id] 
1 variable length string array: run-args & exec-date

 */


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <limits.h>

#include "EpiCastData2hd5.h"
unsigned short  *g_hd5_buff=0;
int ndays=0;
int max_id=-1;
#define RANK   3
hsize_t  dims[RANK];
int num_values=0,num_overflow=0;

// those below were already defined
extern unsigned int Nunit, *myID; /* no. of census units and ID array */
extern unsigned int *Population;  /* Population of each unit */


/************************************************************/

void setup_hd5_buffer(unsigned int mxId,int nVar){

  if ( mxId==0 ) {
    for (int i = 0; i < Nunit; i++){
      if (mxId <myID[i]) { mxId =myID[i];}
    }
  } else {
    ndays=4+1;
  }
  
  int nId=mxId+1;
  size_t size=nId*nVar*ndays * sizeof(unsigned short);
  printf("Jan setup_hd5_buffer %d*%d*%d --> %.1f/MB \n",mxId, nVar,  ndays,size/1.e6);
  max_id=mxId;
  assert (max_id>0);
  assert (size>0);
  dims[2]=nId;
  dims[1]=nVar;
  dims[0]=ndays;  
  g_hd5_buff=(unsigned short *)malloc(size);
  memset(g_hd5_buff,0,size); // clear content
  
  printf("JanS2 h5 dims %d %d %d \n",dims[0],dims[1],dims[2]);
}


/************************************************************/

void push_day_hd5(int currentDay, int *Sick, int *Pop, int *Inc, int *Pro, int *Pox){
  printf("Jan push_day_hd5 currentDay=%d aSick=%d\n",currentDay,Sick[3030]);
  assert ( currentDay>=0);
  assert ( currentDay<ndays);
}


/************************************************************/
void put_1D_vector(int iz, int iy,int *vect){
  assert (iz < dims[0]);
  assert (iy < dims[1]);
  int ioff=iy*dims[2] + iz*dims[2]*dims[1];
  
  for (int ix=0; ix<=max_id; ix++) {
    int val=vect[ix];
    if (val > USHRT_MAX) { val=USHRT_MAX; num_overflow++; }
    if (val <0) { val=0; num_overflow++; }
    num_values++;
    g_hd5_buff[ioff+ix]=val;
  }
}

/************************************************************/
void save_epicast_h5(char *h5Name, char *textRec1,char *textRec2){
  char *dsetName="3Dcounter";
  char *ssetName="1Dtexts";
  
  hid_t       file_id, dataset_id, dataspace_id;  /* identifiers */
  herr_t      status;

   /* Create a new file using default properties. */
   file_id = H5Fcreate(h5Name, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

   printf("JanW h5 dims %d %d %d \n",dims[0],dims[1],dims[2]);
   /* Create the data space for the dataset. */
   dataspace_id = H5Screate_simple(RANK, dims, NULL);
  
   printf(" Create    dataset named=%s\n",dsetName);
   dataset_id = H5Dcreate2(file_id, dsetName, H5T_NATIVE_USHORT, dataspace_id,H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

   /* Write the dataset. */
   status = H5Dwrite(dataset_id, H5T_NATIVE_USHORT, H5S_ALL, H5S_ALL, H5P_DEFAULT,g_hd5_buff);

   printf("wrote 3D data set, %d\n",status);
   assert(status!=-1);

   /* add variable length string array */
   hsize_t   strdims[1] = {2};  // can hold 2 strings
   hid_t type = H5Tcopy (H5T_C_S1);
   status = H5Tset_size (type, H5T_VARIABLE);
   hid_t strspace_id = H5Screate_simple(1, strdims, NULL);
   hid_t  strset_id =H5Dcreate2(file_id, ssetName, type, strspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
   /* Write the strings */
   char *string_att[2]={textRec1,textRec2}; 
   status = H5Dwrite(strset_id, type,H5S_ALL, H5S_ALL, H5P_DEFAULT, &string_att);
    printf("wrote 1D string-data set, %d\n",status);
    assert(status!=-1);
   
   /* End access to the dataset and release resources used by it. */
   status = H5Dclose(dataset_id);
   status = H5Dclose(strset_id);

   /* Terminate access to the data space. */ 
   status = H5Sclose(dataspace_id);
   status = H5Sclose(strspace_id);

   /* Close the file. */
   status = H5Fclose(file_id);
   printf("Jan saved %s , mum_values=%d, num_overflow=%d\n",h5Name,num_values,num_overflow);

}

/************************************************************/
char * argv_to_text(int argc, char **argv){
  int i;
  int len = 0;
  char *str = "";
  for(i=0; i<argc; i++)  {
    len += strlen(argv[i]) + 1;
  }
  str = (char *)malloc(sizeof(char) * len);
  str[0]=0; // set string termination terminating 
  printf("ll %d =%s=",len,str);
  for(i=0; i<argc; i++)  {
    strcat(str, argv[i]);
    strcat(str, " ");
  }
  printf("Jan args str = [%s]\n", str);
  return str;
}
