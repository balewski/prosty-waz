#!/usr/bin/env python3
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from PIL import Image
import numpy as np

def add_white_noise(img, noise_level=0.1):
    """Add white noise to the image."""
    # Convert image to numpy array
    img_array = np.array(img)

    # Generate white noise
    noise = np.random.randint(0, 256, img_array.shape).astype(np.float32)

    # Add noise to the image
    noisy_img_array = img_array + noise_level * noise

    # Ensure values are within the valid range and convert back to uint8
    noisy_img_array = np.clip(noisy_img_array, 0, 255).astype(np.uint8)

    # Convert numpy array back to PIL Image
    noisy_img = Image.fromarray(noisy_img_array)
    return noisy_img

def compute_metric(clean_img, noisy_img):
    """Compute a metric (e.g., Mean Squared Error) between two images."""
    clean_array = np.array(clean_img)
    noisy_array = np.array(noisy_img)
    mse = ((clean_array - noisy_array) ** 2).mean()
    return mse

# Load the image
file_path = 'Lena_test_image.png'
lena_image = Image.open(file_path)

# Resize the image
new_width = 250
new_height = int((new_width / lena_image.width) * lena_image.height)
lena_resized = lena_image.resize((new_width, new_height))

noise=0.15
# Add white noise to the image
lena_noisy = add_white_noise(lena_resized, noise_level=noise)

# Compute a metric between the clean and noisy image
metric = compute_metric(lena_resized, lena_noisy)

# Display the original, resized, and noisy images
lena_resized.show()
lena_noisy.show()

print('MSE: %.1f    between clean and noise: %.2f image'%(metric,noise))

lena_resized.save('out/lena_resized.png')
lena_noisy.save('out/lena_noisy.png')


'''  OUTPUT
MSE: 92.8    between clean and noise: 0.15 image
MSE: 83.9    between clean and nois: 0.10 image
MSE: 48.5    between clean and noise: 0.05 image

'''
