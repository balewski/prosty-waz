#!/usr/bin/env python3
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from PIL import Image
import matplotlib.pyplot as plt
import numpy as np

#...!...!..................
def process_and_clip_image(orgImg, mySize, myPos, scale, rotate=None):
    """
    Process the image: convert to grayscale, scale down, crop to a specific size, 
    and optionally rotate.

    :param orgImg: PIL.Image - Original image.
    :param mySize: tuple - Desired output size as (nx, ny).
    :param myPos: tuple - Position (x0, y0) for the lower left corner of the cropped area.
    :param scale: float - Scale factor for resizing the image.
    :param rotate: bool - If True, rotate the image by 90 degrees.
    :return: PIL.Image - The processed image.
    """
    # Convert to grayscale
    gray = orgImg.convert('L')

    # Optionally rotate the image
    if rotate!=None:
        gray = gray.rotate(rotate, expand=True)

    # Scale down the image
    new_size = (int(gray.size[0] * scale), int(gray.size[1] * scale))
    #scaled = gray.resize(new_size, Image.ANTIALIAS)
    scaled = gray.resize(new_size, Image.Resampling.LANCZOS)

    # Calculate the coordinates for cropping
    x0, y0 = myPos
    nx, ny = mySize
    left = x0
    top = scaled.size[1] - ny - y0
    right = left + nx
    bottom = top + ny

    # Crop the image
    cropped = scaled.crop((left, top, right, bottom))

    return cropped

#...!...!..................
def plot_gradients(img,outF):
    """
    Plot the original image and its gradients in both x and y directions with grayscale colorbar, 
    and save the plot.

    :param image_array: 2D numpy array representing the image.
    :param filename: Name of the file to save the plot.
    """
    imageA=np.array(img)
    # Compute gradients
    dyA, dxA = np.gradient(imageA)

    # Set up the plot
    fig, axes = plt.subplots(1, 3, figsize=(18, 6))

    imgL=[imageA,dyA,dxA]
    titL=['Input image', 'gradY','gradX']
    cmapL=[ 'gray','bwr','bwr']
    
    for j in range(3):        
        ax = axes[j]
        
        zBar = ax.imshow(imgL[j], cmap=cmapL[j])
        ax.set_title(titL[j])        
        fig.colorbar(zBar, ax=ax, orientation='horizontal')

        
    '''
    # Plot original image
    im0 = axes[0].imshow(image_array, cmap='gray')
    axes[0].set_title('Original Image')
    #axes[0].axis('off')
    fig.colorbar(im0, ax=axes[0], orientation='horizontal')

    # Plot gradient in y-direction
    im1 = axes[1].imshow(dy, cmap='gray')
    axes[1].set_title('Gradient in y-direction')
    #axes[1].axis('off')
    fig.colorbar(im1, ax=axes[1], orientation='horizontal')

    # Plot gradient in x-direction
    im2 = axes[2].imshow(dx, cmap='gray')
    axes[2].set_title('Gradient in x-direction')
    #axes[2].axis('off')
    fig.colorbar(im2, ax=axes[2], orientation='horizontal')
    '''

    # Save the plot
    print('save 3p:',outF)
    plt.savefig('out/'+outF)
    
'''
from skimage import data
    # Load the Cameraman image
    imgA = data.camera()
    img=Image.fromarray(imgA)
    img.save('out/cameraman.png')
'''
    
#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__=="__main__":
    fileN = 'xray-hand-8y.png'
    #
    if 0: # thumb for Esherhands
        mySize=(64,80) ; myPos=(4,135); scale=0.7; rotate=None
    if 0: # full hand  for Esherhands
        mySize=(128,192) ; myPos=(0,10); scale=0.42; rotate=None
    if 0: # full side hand  for Esherhands
        fileN = 'xray-hand-4y.png'
        mySize=(128,192) ; myPos=(0,25); scale=0.45; rotate=None

    #fileN = 'xray-foot-12y.png' ;   mySize=(192,128) ; myPos=(0,25); scale=0.32; rotate=-10

    #fileN = 'cameraman.png' ; mySize=(256,384) ; myPos=(20,0); scale=0.755; rotate=None

    #fileN = 'igb_facade.png' ;  mySize=(192,128) ; myPos=(0,0); scale=0.28; rotate=None

    #fileN = 'zebra.png' ;  mySize=(384,256) ; myPos=(0,0); scale=0.5; rotate=None
    #fileN = 'shue-hills.png' ;  mySize=(32,32) ; myPos=(0,0 ); scale=0.046; rotate=None
    #fileN = 'alphabet.png' ;  mySize=(64,16) ; myPos=(0,0 ); scale=0.5; rotate=None
    #1mySize=(32,1) ; myPos=(0,0 ); scale=0.5; rotate=None

    #fileN = 'e_coli_bacteria.jpg';  mySize=(192,128) ; myPos=(110,38); scale=0.5; rotate=None

    fileN = 'micro_germ.jpg';  mySize=(384,256) ; myPos=(130,40); scale=0.8; rotate=None

    # Load the image
    orgImg = Image.open('org/'+fileN)
    
    img=process_and_clip_image(orgImg, mySize, myPos, scale, rotate=rotate)    
    
    #----- OUTPUTS ------
    #outF=fileN.replace('.png','_x%d_y%d.png'%(mySize[0],mySize[1]))
    outF=fileN.replace('.jpg','_x%d_y%d.png'%(mySize[0],mySize[1]))
    img.save('out/'+outF)
    print('saved:',outF)
    if 1:
        # Plotting the image using Pyplot
        plt.imshow(img, cmap='gray')    
        plt.title(outF)
        outF2=outF.replace('.png','.framed.png')
        #plt.axis('off')  # Turn off axis numbers and labels    
        plt.savefig('out/'+outF2)

    outF3=outF.replace('.png','.grad.png')
    plot_gradients(img,outF3)
    plt.show()

