"""
Functionality for computing distances between sequences based on the
Lempel-Ziv complexity
Otu & Sayood, Bioinformatics (2003), 19(16): p.2122-2130
"""


def complexity(seq):

    start = 2
    length = len(seq)
    if length < start:
        # here, complexity == length
        return length

    complexity = 2
    hist_len = 1
    pos = start
    last_match = 0

    # seq[index] == next symbol
    for index in range(pos, length):
        is_repro, last_match = is_reproducible(
            seq, index, hist_len, last_match)
        if is_repro:
            hist_len += 1
        else:
            hist_len = 1
            complexity += 1

    print('CM:',complexity)
    return complexity


def is_reproducible(seq, index, hist_len, last_match=0):

    hist_start = index - hist_len
    for i in range(last_match, hist_start):
        # j == hist_len in last iteration
        for j in range(0, hist_len+1):
            if seq[i+j] != seq[hist_start+j]:
                break
        if j == hist_len:
            # returns index of new last_match
            return True, i

    return False, 0


class Distance():

    D        = 'd'
    D_STAR   = 'd_star'
    D1       = 'd1'
    D1_STAR  = 'd1_star'
    D1_STAR2 = 'd1_star2'


    def __get_complexity(self, seqnum1, seqnum2):

        seqs = self._seqs

        # cached values
        c1 = self._complexity[seqnum1]
        c2 = self._complexity[seqnum2]
        # could precompute & add method set_disttype()
        # composite seqs
        seq12 = seqs[seqnum1] + seqs[seqnum2]
        c12 = complexity(seq12)
        seq21 = seqs[seqnum2] + seqs[seqnum1]
        c21 = complexity(seq21)

        return c1, c2, c12, c21


    def __d(self, seqnum1, seqnum2):

        c1, c2, c12, c21 = self.__get_complexity(seqnum1, seqnum2)
        return max(c12 - c1, c21 - c2)


    def __d_star(self, seqnum1, seqnum2):

        c1, c2, c12, c21 = self.__get_complexity(seqnum1, seqnum2)
        return float(max(c12 - c1, c21 - c2)) / max(c1, c2)


    def __d1(self, seqnum1, seqnum2):

        c1, c2, c12, c21 = self.__get_complexity(seqnum1, seqnum2)
        return c12 - c1 + c21 - c2


    def __d1_star(self, seqnum1, seqnum2):

        c1, c2, c12, c21 = self.__get_complexity(seqnum1, seqnum2)
        return float(c12 - c1 + c21 - c2) / c12


    def __d1_star2(self, seqnum1, seqnum2):

        c1, c2, c12, c21 = self.__get_complexity(seqnum1, seqnum2)
        return float(c12 - c1 + c21 - c2) * 2 / (c12 + c21)


    def __init__(self, seqL, disttype=D1_STAR2):
        print('JJ1:',seqL)
        if disttype == self.D:
            pwdist_func = self.__d
        elif disttype == self.D_STAR:
            pwdist_func = self.__d_star
        elif disttype == self.D1:
            pwdist_func = self.__d1
        elif disttype == self.D1_STAR:
            pwdist_func = self.__d1_star
        elif disttype == self.D1_STAR2:
            pwdist_func = self.__d1_star2
        else:
            msg = 'unknown disttype "%s"' % disttype
            raise ValueError(msg)

        self.pairwise_distance = pwdist_func

        self._seqs = seqL
        self._complexity = []
        self.__precompute_complexity()

    def __precompute_complexity(self):

        for seq in self._seqs:
            self._complexity.append(complexity(seq))
