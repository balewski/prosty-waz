#!/usr/bin/env python

"""
PURPOSE:
compute Lempel-Ziv complexity-based distances between sequences

"""

import sys
from optparse import OptionParser, OptionGroup

import LempelZiv

def set_options():
    parser = OptionParser()
    parser.add_option("-d", "--distance", metavar="NAME", default='d1_star2',
                     help="choose from d, d_star, d1, d1_star, "
                     "d1_star2 (default)")
    
    return parser


def main():

    parser = set_options()
    options, args = parser.parse_args()
 
    ''' data from Hasan H. Otu paper
    A new sequence distance measure for
    phylogenetic tree construction

    Vol. 19 no. 16 2003, pages 2122–2130
    DOI: 10.1093/bioinformatics/btg295

    '''

    seqS = 'AACGTACCATTG'
    seqR = 'CTAGGGACTTAT'
    seqQ = 'ACGGTCACCAA'

    seqL=[seqS,seqR,seqQ]
    nSeq=len(seqL)
    dist = LempelZiv.Distance(seqL, disttype=options.distance)
    print('\ndistance matrix:')
    for i in range(nSeq):
       for j in range(i+1,nSeq):
           value = dist.pairwise_distance(i, j)
           print(i,j,'dist=%.3f'%value)


# when executed, just run main():
if __name__ == '__main__':
    main()
