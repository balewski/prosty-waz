#!/usr/bin/env python
"""
Loads configuration of DDS via serial port
must be executed as sudo
To find the anme of serial port do:
 dmesg | grep tty

w/ & w/o serial port device connected
"""

import time
import serial

# configure the serial connections
print 'Initializing Rs232....'
ser = serial.Serial(
    port='/dev/ttyACM0',
    baudrate=115200,
    parity=serial.PARITY_ODD,
    stopbits=serial.STOPBITS_TWO,
    bytesize=serial.SEVENBITS
)


print "RS232: isOpen=",ser.isOpen()

print 'press ctrl-C to terminate'


input=50
while 1 :
    input=input+1
    print ' '
    print "sending input/dec=",input," char=",chr(input)
    out = ''
    ser.write(chr(input))
    time.sleep(1)
    while ser.inWaiting() > 0:
        out += ser.read(1)

    if out != '':
        print "Rs232>>" + out 
        
