#!/usr/bin/env python
import xml.etree.ElementTree as ET
from collections import defaultdict

import sys
print ('python ver=',sys.version)
(va,vb,vc,vd,ve)=sys.version_info
if va==2:
  assert(vb>=7)
return
# this is key function converting xml-struct in to dict, including attributes
def etree_to_dict(t):
    d = {t.tag: {} if t.attrib else None}
    children = list(t)
    if children:
        dd = defaultdict(list)
        for dc in map(etree_to_dict, children):
            for k, v in dc.iteritems():
                dd[k].append(v)
        d = {t.tag: {k:v[0] if len(v) == 1 else v for k, v in dd.iteritems()}}
    if t.attrib:
        d[t.tag].update(('@' + k, v) for k, v in t.attrib.iteritems())
    if t.text:
        text = t.text.strip()
        if children or t.attrib:
            if text:
              d[t.tag]['#text'] = text
        else:
            d[t.tag] = text
    return d

tree1 = ET.parse('country_data.xml')
root1 = tree1.getroot()

print("\ndump children level1")
for child in root1:
     print("tag=",child.tag, " atrib=",child.attrib," len=",len(child))


from pprint import pprint
dict1 = etree_to_dict(root1)

print ('dump dict to screen')
pprint(dict1)

# note, anymarkap was installed w/ pip, works w/ python 2.7+
import anymarkup
print ('dump JSON to file')
anymarkup.serialize_file(dict1, 'out.json', format='json')
print ('dump YAML file')
anymarkup.serialize_file(dict1, 'out.yaml', format='yaml')

