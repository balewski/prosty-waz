#!/usr/bin/env python
# in C++ use :  printf("{\"core\":\"b\"}\n");

import json
from pprint import pprint

#  h='"core":"b"'   bad: missing brackets
# h="{'core':'b'}"   bad  parethesis

h='{"core":111.22}' 
print ( type(h),h)
d1 = json.loads(h)
print ( type(d1),d1)

h = '{"foo":"bar", "foo2":123, "ww":{"aa":7,"bb":8}}'
d2 = json.loads(h)
# pretty-print
print(json.dumps(d2,sort_keys=True,indent=2))
pprint(d2)

jsonF='data.json'

print ("write  JSON to disc")
with open(jsonF, 'w') as outfile:
    json.dump(d1, outfile)
    outfile.write('\n')
    json.dump(d2, outfile)
    outfile.write('\n')
    outfile.close

print ("read JSON from disc")
with open(jsonF) as inpFile:
    for line in inpFile:
        #print (line)
        d = json.loads(line)
        pprint(d)
    inpFile.close
