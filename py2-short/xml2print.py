#!/usr/bin/env python
# in C++ use :  printf("{\"core\":\"b\"}\n");
import xml.etree.ElementTree as ET
tree = ET.parse('country_data.xml')
root = tree.getroot()


print("\ndump children")
for child in root:
     print("tag=",child.tag, "text=",child.text," atrib=",child.attrib," len=",len(child))
     for child2 in child:
         #print(child2)
         print("     tag=",child2.tag, " atrib=",child2.attrib," len=",len(child2))

print("\nFinding elements with tag neighbor")
for neighbor in root.iter('neighbor'):
    print(neighbor.attrib)
    
print("\nfinds only elements with a tag which are direct children of the current element")
for country in root.findall('country'):
     rank = country.find('rank').text
     name = country.get('name')
     print(name, rank)
