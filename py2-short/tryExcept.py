#!/usr/bin/env python

import sys

(x,y) = (5,0)

print("=====A")
try:
    z = x/y
except ZeroDivisionError:
    print ("divide by zero")
print(" more work1")

print("=====B")
try:
    z = x/y
except ZeroDivisionError as e:
    z = e # representation: "<exceptions.ZeroDivisionError instance at 0x817426c>"
    print (z ," integer division or modulo by zero")
print(" more work2")


print("===== General Error Catching")

def bad_code():
    z= x/y
    return z
def rollback():
    print("in roll back")

def proceed_ok():
    print("in proceed_ok")


import sys
try:
    bad_code()
except: # catch *all* exceptions
    e = sys.exc_info()[0]
    print( "mmm Error: %s" % e )
    rollback()
    print("uuu now will crash")
    raise 
else:
    proceed_ok()

print("normal end")
