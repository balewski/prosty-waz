#!/usr/bin/env python
"""
To do it recursively and extract full filenames and directory names, use os.walk and os.path.join.

 Use os.path.isfile(path) to test if an item is actually just a regular file or not.

// filenames = [entry for entry in os.listdir("/tmp") if os.path.isfile(os.path.join('/tmp/',entry))] 
"""

import os
k=0
mx=30
headPath='/export/data/xrd/ns/star/'
for (dirN, _, files) in os.walk(headPath):
    if '.git/' in dirN: continue
    print "--- dir ---",dirN
    for f in files:
        path = os.path.join(dirN, f)
        if os.path.exists(path):
            isLink=os.path.islink(path)
            print k, isLink,path
            if isLink:
                print "truth=",os.readlink(path)
        else:
            print 'NOT existing:',path
        k+=1
        if k>mx: break
    if k>mx: break
