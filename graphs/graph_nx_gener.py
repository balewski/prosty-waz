#!/usr/bin/env python3
import sys
(va,vb,vc,vd,ve)=sys.version_info ; assert(va==3)  # needes Python3
import numpy as np

''' 
graph generators
https://networkx.github.io/documentation/stable/reference/generators.html

'''
import matplotlib.pyplot as plt
from networkx import nx
#from networkx.drawing.nx_agraph import graphviz_layout


# - - - - - -  PARSER 
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-c", "--complete_graph", type=int, default=None, help=" size, e.g.: 9")
parser.add_argument("-r", "--random_tree", type=int, default=None, help=" size, e.g.: 9")
parser.add_argument("-b", "--balanced_tree", type=int, nargs='+', default=None, help=" Branching factor + Height of the tree, e.g.: 2 3")
parser.add_argument("-f", "--full_rary_tree", type=int, nargs='+', default=None, help="tree w/ all non-leaf vertices have exactly r children, e.g.: 3 15  ")
parser.add_argument("-p", "--random_powerlaw_tree", type=float, nargs='+', default=None, help="n (int)  number of nodes + gamma (float)  Exponent of the power law, e.g.: 10 2.5 ")

args = parser.parse_args()
for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
# - - - - - -  PARSER  END

G=None

if args.complete_graph:
    G = nx.complete_graph(args.complete_graph)

if args.random_tree:
    G = nx.random_tree(args.random_tree)

if args.balanced_tree:
    inp=args.balanced_tree
    assert len(inp)==2
    G=nx.balanced_tree( inp[0],inp[1])

if args.full_rary_tree:
    inp=args.full_rary_tree
    assert len(inp)==2
    G=nx.full_rary_tree( inp[0],inp[1])

if args.random_powerlaw_tree:
    inp=args.random_powerlaw_tree
    assert len(inp)==2
    G=nx.random_powerlaw_tree( int(inp[0]),inp[1], tries=1000)

if G==None :
    parser.print_help()
    exit(0)
    
pathlengths = []

print("source vertex {target:length, }")
for v in G.nodes():
    spl = dict(nx.single_source_shortest_path_length(G, v))
    #print('{} {} '.format(v, spl))
    print('v=',v, 'spl=',spl)
    for p in spl:
        pathlengths.append(spl[p])

print('')
print("average shortest path length %s" % (sum(pathlengths) / len(pathlengths)))

# histogram of path lengths
dist = {}
for p in pathlengths:
    if p in dist:
        dist[p] += 1
    else:
        dist[p] = 1

print('')
print("length #paths")
verts = dist.keys()
for d in sorted(verts):
    print('%s %d' % (d, dist[d]))

# add attributes to edges
edgeL=G.edges()
for i,ed in enumerate(edgeL):
    print('work on %d edge='%i, ed)
    G.add_edge(ed[0],ed[1],e=i)


print("radius: %d" % nx.radius(G))
print("diameter: %d" % nx.diameter(G))
print("eccentricity: %s" % nx.eccentricity(G))
print("center: %s" % nx.center(G))
print("periphery: %s" % nx.periphery(G))
print("density: %s" % nx.density(G))
print("is_tree: %s" % nx.is_tree(G))

# plotting

pos = nx.spring_layout(G, seed=7)  # positions for all nodes - seed for reproducibility
nx.draw(G, with_labels=True,pos=pos)
edge_labels = nx.get_edge_attributes(G,'e')
print('EL',edge_labels)
#nx.draw_networkx_edge_labels(G, pos,labels = edge_labels) # edge attribute+ID
nx.draw_networkx_edge_labels(G, pos, edge_labels)  # only edgeID
plt.savefig('myGraph.png')
print('plot.show')
plt.show()
