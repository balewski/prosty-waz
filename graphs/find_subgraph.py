#!/usr/bin/env python3
import sys
(va,vb,vc,vd,ve)=sys.version_info ; assert(va==3)  # needes Python3
import numpy as np

''' 
find all mappings of a small subgraph in a larger graph


'''
import matplotlib.pyplot as plt
from networkx import nx
from itertools import permutations
from networkx.drawing.nx_agraph import graphviz_layout
from pprint import pprint

# - - - - - -  PARSER 
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-c", "--complete_graph", type=int, default=None, help=" size, e.g.: 9")
parser.add_argument("-r", "--random_tree", type=int, default=7, help=" size, e.g.: 9")
args = parser.parse_args()
for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
# - - - - - -  PARSER  END

# star Sk is the complete bipartite graph K1,k: a tree with one internal node and k leaves (but, no internal nodes and k + 1 leaves when k ≤ 1).

def find_Kstar_in_graph(starEdges,G):
    # 1)..... verify star-edges are correct  
    S=nx.parse_edgelist(starEdges, nodetype = int)
    SndegL= S.degree # list of nodes (id,degree)
    s_num_nodes= S.number_of_nodes()
    print('S-graph edges:', list(S.edges()),', nodes:',s_num_nodes)
    print('S-graph (id,degree):', SndegL)
    s_cent_deg=SndegL[0]
    print('S properties:  cent deg=%d, '%(s_cent_deg))
    # make sure it is star-graph , all nodes must have 1 degree, except the center
    assert s_cent_deg==s_num_nodes-1
    for i in range(1,s_num_nodes):
        assert SndegL[i]==1
    # 2) . . . . select center node candidates
    GndegL=G.degree
    centL=[]
    for nid,dg in GndegL:
        #print(nid,dg)
        if dg<s_cent_deg : continue
        centL.append(nid)
    print('G center candidates:',centL)
    # 3) ..... find S-graph matches based on degree, build lists doughters per center
    outD={}
    for cid in centL:
        xid=[n for n in G.neighbors(cid)]
        #print('cent=%d neighbours:'%cid,xid)
        perL=[x for x in permutations(xid,s_cent_deg)]
        #print(' %d  permutations:'%len(perL),perL)
        outD[cid]=perL
    return outD

# = = = = = M A I N  = = = = =
G=None
if args.complete_graph:
    G = nx.complete_graph(args.complete_graph)
if args.random_tree:
    G = nx.random_tree(args.random_tree)
if G==None :
    parser.print_help()
    exit(0)
    
starEdges=["0 1","0 2", "0 3"]
outD=find_Kstar_in_graph(starEdges,G)
print('final solution: {center:[list of leaves]}')
pprint(outD)

# plotting
pos=graphviz_layout(G)
nx.draw(G, with_labels=True,pos=pos)
edge_labels = nx.get_edge_attributes(G,'e')
print('EL',edge_labels)
#nx.draw_networkx_edge_labels(G, pos,labels = edge_labels) # edge attribute+ID
nx.draw_networkx_edge_labels(G, pos, edge_labels)  # only edgeID
plt.savefig('myGraph.png')
print('plot.show')
plt.show()
