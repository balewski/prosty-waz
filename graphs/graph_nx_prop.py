#!/usr/bin/env python3
import sys
(va,vb,vc,vd,ve)=sys.version_info ; assert(va==3)  # needes Python3
import numpy as np

''' 
graph operations, based on
https://networkx.github.io/documentation/stable/tutorial.html

Examples:
https://networkx.github.io/documentation/stable/auto_examples/index.html

'''
import matplotlib.pyplot as plt
from networkx import nx

G = nx.lollipop_graph(4, 5)

pathlengths = []

print("source vertex {target:length, }")
for v in G.nodes():
    spl = dict(nx.single_source_shortest_path_length(G, v))
    print('v=',v, 'spl=',spl)
    for p in spl:
        pathlengths.append(spl[p])

print('')
print("average shortest path length %s" % (sum(pathlengths) / len(pathlengths)))

# histogram of path lengths
dist = {}
for p in pathlengths:
    if p in dist:
        dist[p] += 1
    else:
        dist[p] = 1

print('')
print("length #paths")
verts = dist.keys()
for d in sorted(verts):
    print('%s %d' % (d, dist[d]))

print("radius: %d" % nx.radius(G))
print("diameter: %d" % nx.diameter(G))
print("eccentricity: %s" % nx.eccentricity(G))
print("center: %s" % nx.center(G))
print("periphery: %s" % nx.periphery(G))
print("density: %s" % nx.density(G))
print("is_tree: %s" % nx.is_tree(G))

nx.draw(G, with_labels=True)
print('plot.show')
plt.show()
