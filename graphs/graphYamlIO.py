#!/usr/bin/env python3
""" 
 exercise Yaml IO of graph data
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import ruamel.yaml  as yaml
import networkx as nx

grfF='test.grf.yaml'
G0=nx.path_graph(4)
nx.write_yaml(G0,grfF)
print('saved',grfF)
#nx.read_yaml() is crashing, do it manually
with open(grfF, 'r') as ymlFd :
    G=yaml.load( ymlFd, Loader=yaml.CLoader)

print('gobj=',type(G),len(G))

import matplotlib.pyplot as plt
nx.draw(G)
plt.show()
