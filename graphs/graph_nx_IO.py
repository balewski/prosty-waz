#!/usr/bin/env python3
import sys
(va,vb,vc,vd,ve)=sys.version_info ; assert(va==3)  # needes Python3
import numpy as np

''' 
graph operations, based on
https://networkx.github.io/documentation/stable/tutorial.html

Examples:
https://networkx.github.io/documentation/stable/auto_examples/index.html

'''


import sys

import matplotlib.pyplot as plt
import networkx as nx

G = nx.grid_2d_graph(3, 5, periodic=False)  # 5x5 grid

# print the adjacency list
for line in nx.generate_adjlist(G):
    print(line)

outF="grid.edgelist.txt"
print(' write edgelist to:',outF)
nx.write_edgelist(G, path=outF, delimiter=":")

# read edgelist from grid.edgelist
H = nx.read_edgelist(path=outF, delimiter=":")

nx.draw(H)
print('plot.show')
plt.show()
