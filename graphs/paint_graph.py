#!/usr/bin/env python
import sys
import numpy as np

''' 
paint subsets of DiGraph differently
'''
import matplotlib.pyplot as plt
from networkx import nx
from networkx.drawing.nx_agraph import graphviz_layout
from pprint import pprint

# - - - - - -  PARSER 
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-xx", "--complete_graph", type=int, default=None, help=" size, e.g.: 9")
args = parser.parse_args()
for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
# - - - - - -  PARSER  END


#...!...!..................
def build_graph(G):
    edges1=["A0 B1","A0 C1"]
    edges2=["B1 B2","B1 B3","B3 B4"]
    edges3=["C1 C2"]
    edges=edges1+edges2+edges3
    for e in edges:
        a,b=e.split()
        G.add_edge(a,b)
    return G

# = = = = = M A I N  = = = = =
G=nx.DiGraph()
build_graph(G)
numNode= G.number_of_nodes()
print('G numNode=',numNode)
print('G nodes:', list(G.nodes()))
print('G edges:', list(G.edges()))

#..... subgraph
H=nx.ego_graph(G,'B1',  radius=1000)
print('ego(B1) edges:', list(H.edges()))


#================= plotting
prog='dot'
pos=graphviz_layout(G,prog=prog)
print('pp',len(pos),type(pos),list(pos.keys()))
fig=plt.figure(1,facecolor='white', figsize=(8,4))

# - - - - - - - edges coloring
ax = plt.subplot(1,2,1)
node_size=2
nx.draw(G, with_labels=False,pos=pos,node_size=node_size, arrows=False)
ax.text(0.1,0.95,"a) only edges\n some colored",transform=ax.transAxes)
nx.draw_networkx_edges(G, pos,node_size=node_size,
                       edgelist=[("B1", "B2"),("C1","C2")],
                       width=2, alpha=0.5, edge_color='r')
nn='A0'
x,y=pos[nn]
plt.text(x,y+0.1,s='head='+nn, bbox=dict(facecolor='red', alpha=0.5),horizontalalignment='center')


# - - - - - - - -  nodes coloring, labels outside circle
node_size=40
pos_labels={}
for key in pos:
    x, y = pos[key]
    pos_labels[key] = (x+15, y)
ax = plt.subplot(1,2,2)
ax.text(0.1,0.95,"b) nodes coloring\nlabels offset",transform=ax.transAxes)
nx.draw(G, with_labels=False,pos=pos,node_size=node_size)

labTxtIter=nx.draw_networkx_labels(G,pos=pos_labels,fontsize=20)
for nn,t in labTxtIter.items():
    if 'B' in nn :
        t.set(rotation=30,color='green')

nx.draw_networkx_nodes(G, pos,
                       nodelist=["B1","B2","B3"],
                       node_color='r',node_size=node_size, alpha=0.8)



edge_labels = nx.get_edge_attributes(G,'e')
print('EL',edge_labels)
#nx.draw_networkx_edge_labels(G, pos,labels = edge_labels) # edge attribute+ID
#nx.draw_networkx_edge_labels(G, pos, edge_labels)  # only edgeID

plt.savefig('myGraph.png')
print('plot.show')
plt.show()
