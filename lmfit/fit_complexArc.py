#!/usr/bin/env python3

"""
Complex parametric curve fit
=======================
Based on:
https://lmfit.github.io/lmfit-py/examples/example_complex_resonator_model.html#sphx-glr-examples-example-complex-resonator-model-py

a) fit function is an arc in a complex plain
b) noise amplitude is passed to model.fit() so CHI2/DOF is reasonable

c) allow for amplitude miss-match, just real value

"""
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('TkAgg')

import numpy as np

import lmfit

####################################################################
# Since ``scipy.optimize`` and ``lmfit`` require real parameters

#...!...!....................
def my_arc_func(x, A,F0,Ta):  # decouple phi0 and freq
    phase=2*np.pi*( F0 + x*Ta)
    ringFunc=A*np.exp(1j*phase) 
    return ringFunc


####################################################################
# The standard practice of defining an ``lmfit``model is as follows:

class ResonatorModel(lmfit.model.Model):
    __doc__ = "resonator model" + lmfit.models.COMMON_DOC

    def __init__(self, *args, **kwargs):
        # pass in the defining equation so the user doesn't have to later.
        super().__init__(my_arc_func, *args, **kwargs)

        self.set_param_hint('A', min=0)  # Enforce A is positive

    def guess(self, data, x=None, **kwargs):
        verbose = kwargs.pop('verbose', None)
        if x is None:
            return
        # try bad starting point - still works
        params = self.make_params(A=4, F0=2, Ta=0.02)

        #params['Q'].set(min=Q_min, max=Q_max)
        
        # fix  some params to test sensitivity
        #params['T'].vary = False
        #params['P'].vary = False
        return lmfit.models.update_param_vals(params, self.prefix, **kwargs)


########################################################
# Now let's use the model to generate some fake data:
np.random.seed(123)

nf_step=100
f = np.linspace(1, 20, nf_step)

resonator = ResonatorModel()
true_params = resonator.make_params(A=3,F0=5,Ta=0.03)
true_y = resonator.eval(params=true_params, x=f)
noise_scale = 0.03
weights=np.zeros(nf_step)+ 1./noise_scale

measured_y = true_y + noise_scale*(np.random.randn(nf_step) + 1j*np.random.randn(nf_step))

print('freq:',f.shape,f.dtype)
print('measured_y:',measured_y.shape,measured_y.dtype)


################################################################
# Try out the guess method we added:

guess = resonator.guess(measured_y, x=f, verbose=True)
print('\n guess:')
print(guess)
guess_y = resonator.eval(params=guess, x=f)

plt.figure(facecolor='white', figsize=(12,8))
nrow,ncol=2,2

ax = plt.subplot(nrow, ncol, 1)
zAbs=np.abs(measured_y)
zPhi=np.unwrap(np.angle(measured_y))

guess_zAbs=np.abs(guess_y)
guess_zPhi=np.unwrap(np.angle(guess_y))

ax.plot(f, zAbs, '.', label='noisy data')
ax.plot(f, guess_zAbs, 'k--', label='initial guess')
ax.set(ylabel='abs(Z)',xlabel='MHz', title='simulated data')
ax.grid()
ax.set_ylim(0,)
ax.legend(loc='best')

ax = plt.subplot(nrow, ncol, 2)
ax.plot(f,zPhi, '.',color='green')
ax.plot(f, guess_zPhi, 'k--', label='initial guess')
ax.set(ylabel='Phase (rad)',xlabel='frequency (MHz)', title='simulated data')
ax.grid()
ax.legend(loc='best')
#plt.show() # just input data

################################################################
# And now fit the data using the guess as a starting point:

result = resonator.fit(measured_y, params=guess, x=f, verbose=True, weights=weights)

print('\n fit result:')
print(result.fit_report() + '\n')
result.params.pretty_print()

######################################################################
# Now we'll make some plots of the data and fit. Define a convenience function
# for plotting complex quantities:

def plot_ri(ax,data, *args, **kwargs):
    ax.plot(data.real, data.imag, *args, **kwargs)


fit_y = resonator.eval(params=result.params, x=f)


ax = plt.subplot(nrow, ncol, 3)
plot_ri(ax,measured_y, '.')
plot_ri(ax,fit_y, 'r.-', label='best fit')
plot_ri(ax,guess_y, 'k--', label='inital guess')
ax.legend(loc='best')
ax.set(xlabel='Re(Z)', ylabel='Im(Z)')
ax.set_aspect(1.0)

ax = plt.subplot(nrow, ncol, 4)
ax.plot(f, np.abs(measured_y), '.')
ax.plot(f, np.abs(fit_y), 'r.-', label='best fit')
ax.plot(f, np.abs(guess_y), 'k--', label='initial guess')
ax.legend(loc='best')
ax.set( ylabel='abs(Z)', xlabel='MHz')

plt.show()
