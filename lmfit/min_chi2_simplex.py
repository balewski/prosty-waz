#!/usr/bin/env python3

"""
Based on: https://lmfit.github.io/lmfit-py/examples/example_brute.html

Use simplex minimizer to find 4D minimum of a function describing user data

"""
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import matplotlib
matplotlib.use('TkAgg')

import numpy as np
import copy
from lmfit import Minimizer, Parameters, fit_report


#  M A I N    C O D E 

####################################################################
# We start off by generating some synthetic data with noise for a decaying sine wave, define an objective function and create a Parameter set.
x = np.linspace(0, 15, 301)
np.random.seed(7)
noise_sig=0.3
noise = np.random.normal(size=x.size, scale=noise_sig)
data = (5. * np.sin(2*x - 0.1) * np.exp(-x*0.08) + noise)
#plt.plot(x, data, 'b')


def fcn2min(params, x, data):
    """Model decaying sine wave, subtract data. residua normalized: chi2/dof-->1"""
    amp = params['amp']
    shift = params['shift']
    omega = params['omega']
    decay = params['decay']
    model = amp * np.sin(x*omega + shift) * np.exp(-x*decay)
    return (model - data)/noise_sig  # no chi2/dof should be 1


# create a set of Parameters
params = Parameters()
params.add('amp', value=7, min=2.5)
params.add('decay', value=0.05, min=0.01)
params.add('shift', value=0.0, min=-np.pi/2., max=np.pi/2)
params.add('omega', value=3, min=1)

####################################################################
# initialize minimizer
# varying parameters need the brute_step attribute specified,
params['amp'].set(brute_step=0.4)
params['decay'].set(brute_step=0.01)
params['omega'].set(brute_step=0.2)
print('M:initilized params')
params.pretty_print()

print('M:initialize a Minimizer and perform the grid search:')
fitter = Minimizer(fcn2min, params, fcn_args=(x, data))
result_brute = fitter.minimize(method='brute', Ns=6, keep=4)

''' 
The parameter Ns determines the ‘number of grid points along the axes’ 

In a more realistic, complicated example the brute method will be used to get reasonable values for the parameters and perform another minimization (e.g., using leastsq) using those as starting values. That is where the keep` parameter comes into play: it determines the “number of best candidates from the brute force method that are stored in the candidates attribute”. 
'''

# for 'shift' parameter we have NOT sepciffied 'brute_step' so it somehow computes the grid using 'Ns'
par_name = 'shift'
indx_shift = result_brute.var_names.index(par_name)
grid_shift = np.unique(result_brute.brute_grid[indx_shift].ravel())
print("M:parameter = {}\nnumber of steps = {}\ngrid = {}".format(par_name,len(grid_shift), grid_shift))

print('M:Optimal solution')
print(fit_report(result_brute))


print('\nM:==========  show grid search best candidates #1 and #3 ========')
result_brute.show_candidates(1)
result_brute.show_candidates(3)

plt.plot(x, data, 'b',label='input data')
plt.plot(x, data + fcn2min(result_brute.params, x, data), 'r--',label='brute')


####################################################################
# 2nd minimization pass using 'lastsq' method
best_result = copy.deepcopy(result_brute)
opt_method='leastsq'
#opt_method='simplex'
opt_method='cobyla'

print('\n start pass-2, optimizer=', opt_method)
best_result2 =None
k=0
for candidate in result_brute.candidates:
    trial = fitter.minimize(method=opt_method, params=candidate.params)
    print('trial',k,trial.chisqr , best_result.chisqr)
    k+=1
    if trial.chisqr < best_result.chisqr:
        best_result2 = trial
        break
print('M: done  leastsq minimization,   the most optimal result2:')
if best_result2==None:
    print('no improvement over grid search')
    exit(1)

print(fit_report(best_result2))
plt.plot(x, data + fcn2min(best_result2.params, x, data), 'g--',label='brute followed by leastsq')

plt.legend()


plt.show()

