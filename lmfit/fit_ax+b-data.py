#!/usr/bin/env python3


import matplotlib.pyplot as plt
import numpy as np

import lmfit


# Shivam's data
# Actual , truth
x = np.array([9.0,  15.0,  9.0,  15.0,  7.0,  8.0,  11.0,  15.0,  11.0,  7.0,  8.0,  12.0,  8.0,  13.0,  15.0,  13.0,  10.0,  20.0,  11.0,  8.0,  8.0,  17.0,  12.0,  18.0,  10.0,  5.0,  7.0,  8.0,  16.0,  12.0,  7.0,  11.0,  11.0,  11.0,  10.0,  8.0,  9.0,  14.0,  15.0,  11.0,  6.0,  15.0,  13.0,  11.0,  11.0,  19.0,  7.0,  16.0])

# Predictions 
y = np.array([ 8.71282298,  9.55013371, 11.84617239, 13.35871674,  8.89247695,
        9.6327524 , 10.54306912, 12.70714914, 11.07336354, 11.17967771,
        8.31580482, 12.73529616, 12.8694945 , 13.28860494, 15.3662587 ,
       11.37544473, 12.81517979, 13.85069986, 12.66858638,  9.50414267,
       10.87996255, 14.83543777, 14.25438424, 12.89365457,  9.15714388,
        7.75953135, 10.48162684, 10.55862841, 13.68051519, 11.39996983,
        9.66927029, 12.86727036, 19.68095117, 13.0823815 , 13.88721202,
       13.53442159, 11.94354724, 10.90295354,  9.74140504, 11.61484038,
       10.30430516, 13.45018332,  8.30226062,  8.60282026, 13.96394429,
       13.88333419, 12.7638594 , 15.26267581])


# remove outlier
mask=y<18 ; x=x[mask];  y=y[mask]

# correct for bias both axis
x0=0; y0=0

x0=x.mean()
y0=y.mean()

x-=x0
y-=y0




def resid(params, x, ydata):
    offset = params['offset'].value
    slope = params['slope'].value
    y_model = offset + slope *x
    return y_model - ydata


params = lmfit.Parameters()
params.add('offset', 1.0, min=-5, max=5.0)
params.add('slope', 1., min=-2, max=2.0)

o1 = lmfit.minimize(resid, params, args=(x, y), method='leastsq')
print("# Fit using leastsq:")
lmfit.report_fit(o1)
yr=o1.residual

cnt={'p':0,'n':0}
for i in range(x.shape[0]):
    #print('%d  x%.2f  res=%.2f'%(i,x[i], yr[i]))
    if yr[i]>0: cnt['p']+=1
    else:  cnt['n']+=1
print('cnt',cnt)


fig=plt.figure(1,facecolor='white', figsize=(5.5,5))
ax = plt.subplot(1,1,1)
ax.plot(x, y, 'ko', lw=2,label=' data')
ax.plot(x, y+o1.residual, 'r-', lw=2,label='fit')
ax.legend( loc='upper left')

ax.set_xlim(4-x0,22-x0)
ax.set_ylim(4-y0,22-y0)
ax.plot([0, 1], [0, 1], transform=ax.transAxes)
ax.grid()

plt.show()
