#!/usr/bin/env python3

"""
Based on: https://lmfit.github.io/lmfit-py/examples/example_brute.html

Use simplex minimizer to find 2D minimum of elliptic well 
*) code is fully vectorized for arbitrary num of dims for x

"""
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('TkAgg')

import numpy as np
from lmfit import Minimizer, Parameters, fit_report
params = Parameters()

# all parametesr are set to False except the varied on: x,y
params.add_many(
        ('a', 2, False),
        ('b', 3, False),
        ('x', 0.0, True),
        ('y', 0.0, True))

# The objective function is quadratic polynomila in (x,y)
def func1(p):
    par = p.valuesdict()
    return (par['a'] * par['x']**2 + par['b'] * par['x'] * par['y'] )

####################################################################
# Do grid search, between -4 and 4 and use a stepsize of 0.5.
params['x'].set(min=-4, max=4, brute_step=0.5)
params['y'].set(min=-4, max=4, brute_step=0.5)

print(' do search')
fitter = Minimizer(func1, params)
result = fitter.minimize(method='brute')

print('dump search grid')
grid_x, grid_y = [np.unique(par.ravel()) for par in result.brute_grid]
print('x:',grid_x)

print('result X0:',result.brute_x0)
print(' func(X0):',result.brute_fval)

print('verify the minimum was found...')
zm=999999
x0=y0=None
for x in grid_x:
  for y in grid_y:
    params['x'].value=x
    params['y'].value=y
    z=func1(params)
    #print(x,y,z)
    if z>zm: continue
    zm=z; x0=x;y0=y

print('true minimum:',x0,y0,zm)

# this 2 outputs are big
print('dump used grid'); print(result.brute_grid)
print('Function values at each point of the evaluation grid, i.e., Jout = func(*grid))'); print(result.brute_Jout)
