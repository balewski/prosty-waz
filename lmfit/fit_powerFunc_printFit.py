#!/usr/bin/env python3

from scipy.optimize import curve_fit
import numpy as np 
import matplotlib.pyplot as plt

#additional import
from lmfit import minimize, Parameters, Parameter, report_fit

xData = [0.0009824379203203417, 0.0011014182912933933, 0.0012433979929054324, 0.0014147106052612918, 0.0016240300315499524, 0.0018834904507916608, 0.002210485320720769, 0.002630660216394964, 0.0031830988618379067, 0.003929751681281367, 0.0049735919716217296, 0.0064961201261998095, 0.008841941282883075, 0.012732395447351627, 0.019894367886486918, 0.0353677651315323, 0.07957747154594767, 0.3183098861837907]
yData = [99.61973156923796, 91.79478510744039, 92.79302188621314, 84.32927272723863, 77.75060981602016, 75.62801782349504, 70.48026800610839, 72.21240551953743, 68.14019252499526, 55.23015406920851, 57.212682880377464, 50.777016257727176, 44.871140881319626, 40.544138806850846, 32.489105158795525, 25.65367127756607, 19.894206907130403, 13.057996247388862]

#####the new approach starts here
def func2(params, x, data):

    m = params['m'].value
    c = params['c'].value
    d = params['d'].value

    model = np.power(x,m)*c + d
    return model - data #that's what you want to minimize

# create a set of Parameters
params = Parameters()
params.add('m', value= -2.5) #value is the initial condition
params.add('c', value= 8.4)
params.add('d', value= 20.0) #add: min=0 to prevents d becoming negative

# do fit, here with leastsq model
result = minimize(func2, params, args=(xData, yData))

# calculate final result
final = yData + result.residual

# https://lmfit.github.io/lmfit-py/fitting.html#lmfit.minimizer.MinimizerResult
# write error report
report_fit(result)
#print aux info:
fitD={'largeCorrel':[],'fitPar':{},'fitQA':{}}

fitInfoD={'nfev':'Number of function evaluations', 'success':'fit status','message':'Message about fit','nvarys':'Number of fitted variables','ndata':'Number of data points','nfree':'Degrees of freedom of fit','chisqr':'total chi2','redchi':'chi2/DOF'}
print('\nfitInfo (manual):')
for k in fitInfoD.keys():
    val=getattr(result,k)
    print(k,'=',val,'(%s)'%fitInfoD[k],type(val))

    if 'chi' in k:
        val=float(val)
    fitD['fitQA'][k]=[val,fitInfoD[k]]
        
parNameL=result.var_names
bestPar=result.params
covarPar=result.covar
covThr=0.95
fitD['fitQA']['covThr']=covThr
print('\nparamInfo (manual):',sorted(bestPar),result.nvarys,covThr)
for i,k in enumerate(parNameL):
    P=bestPar[k]
    val=float(P.value)
    err=float(np.sqrt(covarPar[i,i]))
    print(i,P.name,val,err,P.vary)
    fitD['fitPar'][k]=[val,err,P.vary]
    for j in range(i+1,result.nvarys):
            cor=covarPar[j][i]/np.sqrt(covarPar[i][i]*covarPar[j][j])
            #print(cor,i,j,np.abs(cor)>covThr)
            if cor< covThr: continue
            rec=[parNameL[i],parNameL[j],float('%.3f'%cor)]
            fitD['largeCorrel'].append(rec)
print('FRDC fitD',fitD)    

import ruamel.yaml  as yaml
outF='fitPar.yaml'
ymlFd = open(outF, 'w')
yaml.dump(fitD, ymlFd, Dumper=yaml.CDumper)
ymlFd.close()
print('saved: ',outF)

print('\n plotting...')
try:
    import pylab
    pylab.plot(xData, yData, 'k+')
    pylab.plot(xData, final, 'r')
    pylab.show()
except:
    pass
