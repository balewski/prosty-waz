#!/usr/bin/env python3

"""
Complex Resonator Model for S11
=======================
Based on:
https://lmfit.github.io/lmfit-py/examples/example_complex_resonator_model.html#sphx-glr-examples-example-complex-resonator-model-py


reflection of a microwave resonator with total quality factor
:math:`Q`,  and resonant frequency
:math:`f_0` using:

.. math::

    S_{11}(f) =   1 - \frac{1}{ 1 + 2j Q (f -f_0)/ f_0}

assuming:   \omega_0=2\pi f_0; s= j 2 \pi f;  (f+f_0)/f\simeq 2

:math:`S_{11}` is thus a complex function of a real frequency.

a) for matched  input and output transmission impedances.
b) noise amplitude is passed to model.fit() so CHI2/DOF is reasonable

c) allow for amplitude miss-match, just real value

"""
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('TkAgg')

import numpy as np

import lmfit

####################################################################
# Since ``scipy.optimize`` and ``lmfit`` require real parameters


def linear_resonator(f, f_0, Q, A):
    return A*(1 - ( 1. / (1 + 2j * Q * (f - f_0) / f_0)))


####################################################################
# The standard practice of defining an ``lmfit``model is as follows:

class ResonatorModel(lmfit.model.Model):
    __doc__ = "resonator model" + lmfit.models.COMMON_DOC

    def __init__(self, *args, **kwargs):
        # pass in the defining equation so the user doesn't have to later.
        super().__init__(linear_resonator, *args, **kwargs)

        self.set_param_hint('Q', min=0)  # Enforce Q is positive
        self.set_param_hint('A', min=0)  # Enforce A is positive

    def guess(self, data, f=None, **kwargs):
        verbose = kwargs.pop('verbose', None)
        if f is None:
            return
        dataAbs=np.abs(data)
        A_guess=dataAbs.mean()
        argmin_s11 = dataAbs.argmin()
        fmin = f.min()
        fmax = f.max()
        f_0_guess = f[argmin_s11]  # guess that the resonance is the lowest point
        Q_min = 0.1 * (f_0_guess/(fmax-fmin))  # assume the user isn't trying to fit just a small part of a resonance curve.
        delta_f = np.diff(f)  # assume f is sorted
        min_delta_f = delta_f[delta_f > 0].min()
        Q_max = f_0_guess/min_delta_f  # assume data actually samples the resonance reasonably
        Q_guess = np.sqrt(Q_min*Q_max)  # geometric mean, why not?
        if verbose:
            print("fmin=", fmin, "fmax=", fmax, "f_0_guess=", f_0_guess)
            print("Qmin=", Q_min, "Q_max=", Q_max, "Q_guess=", Q_guess,' A_guess=',A_guess)
        params = self.make_params(Q=Q_guess, f_0=f_0_guess, A=A_guess)
        params['%sQ' % self.prefix].set(min=Q_min, max=Q_max)
        params['%sf_0' % self.prefix].set(min=fmin, max=fmax)
        return lmfit.models.update_param_vals(params, self.prefix, **kwargs)


########################################################
# Now let's use the model to generate some fake data:
np.random.seed(123)

nf_step=1000
f = np.linspace(99.99, 100.09, nf_step)

resonator = ResonatorModel()
true_params = resonator.make_params(f_0=100.01, Q=9000, A=1.1)
true_s11 = resonator.eval(params=true_params, f=f)
noise_scale = 0.12
weights=np.zeros(nf_step)+ 1./noise_scale

measured_s11 = true_s11 + noise_scale*(np.random.randn(nf_step) + 1j*np.random.randn(nf_step))

measured_s11 *=20
print('freq:',f.shape,f.dtype)
print('measured_s11:',measured_s11.shape,measured_s11.dtype)

plt.figure()
nrow,ncol=2,1

ax = plt.subplot(nrow, ncol, 1)
zAbs=np.abs(measured_s11)
#zPhi=np.unwrap(np.angle(measured_s11))
zPhi=np.angle(measured_s11)

ax.plot(f, 20*np.log10(zAbs))
ax.set(ylabel='|S11| (dB)',xlabel='MHz', title='simulated measurement')
ax.grid()

ax = plt.subplot(nrow, ncol, 2)
ax.plot(f,zPhi)
ax.set(ylabel='Phase (rad)',xlabel='Modulation frequency (MHz)')
ax.grid()


################################################################
# Try out the guess method we added:

guess = resonator.guess(measured_s11, f=f, verbose=True)
print('\n guess:')
print(guess)

################################################################
# And now fit the data using the guess as a starting point:

result = resonator.fit(measured_s11, params=guess, f=f, verbose=True, weights=weights)

print('\n fit result:')
print(result.fit_report() + '\n')
result.params.pretty_print()

######################################################################
# Now we'll make some plots of the data and fit. Define a convenience function
# for plotting complex quantities:


def plot_ri(ax,data, *args, **kwargs):
    ax.plot(data.real, data.imag, *args, **kwargs)


fit_s11 = resonator.eval(params=result.params, f=f)
guess_s11 = resonator.eval(params=guess, f=f)

plt.figure(facecolor='white', figsize=(12,5))
nrow,ncol=1,2
ax = plt.subplot(nrow, ncol, 1)
plot_ri(ax,measured_s11, '.')
plot_ri(ax,fit_s11, 'r.-', label='best fit')
plot_ri(ax,guess_s11, 'k--', label='inital guess')
ax.legend(loc='best')
ax.set(xlabel='Re(S11)', ylabel='Im(S11)')

ax = plt.subplot(nrow, ncol, 2)
ax.plot(f, 20*np.log10(np.abs(measured_s11)), '.')
ax.plot(f, 20*np.log10(np.abs(fit_s11)), 'r.-', label='best fit')
ax.plot(f, 20*np.log10(np.abs(guess_s11)), 'k--', label='initial guess')
ax.legend(loc='best')
ax.set( ylabel='|S11| (dB)', xlabel='MHz')

plt.show()
