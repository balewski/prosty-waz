#!/usr/bin/env python3

"""
Complex parametric curve fit
=======================
Based on:
https://lmfit.github.io/lmfit-py/examples/example_complex_resonator_model.html#sphx-glr-examples-example-complex-resonator-model-py

a) fit function is an arc in a complex plain
b) noise amplitude is passed to model.fit() so CHI2/DOF is reasonable

c) guess close starting conditions

"""
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('TkAgg')

import numpy as np

import lmfit

####################################################################
# Since ``scipy.optimize`` and ``lmfit`` require real parameters

#...!...!....................
def my_coil_func(x,F0, A,Ta, B,Tb, Tau):
    xloc=x-x[0]
    phaseA=2*np.pi*(  xloc*Ta) +F0 
    phaseB=2*np.pi*(  xloc*Tb) +F0
    arcFunc  = A*np.exp(1j*phaseA) 
    smallFunc= B*np.exp(1j*phaseB) * np.exp(-Tau*phaseA)
    return arcFunc+smallFunc 
####################################################################
# The standard practice of defining an ``lmfit``model is as follows:

class ResonatorModel(lmfit.model.Model):
    __doc__ = "resonator model" + lmfit.models.COMMON_DOC

    def __init__(self, *args, **kwargs):
        # pass in the defining equation so the user doesn't have to later.
        super().__init__(my_coil_func, *args, **kwargs)

        self.set_param_hint('A', min=0)  # Enforce A is positive
        self.set_param_hint('B', min=0)  

    def guess(self, data, x=None, **kwargs):
        verbose = kwargs.pop('verbose', None)
        if x is None:
            return

        # Estimate seed for the fit
        zAbs=np.abs(data)
        zPhi=np.unwrap(np.angle(data))

        F0_s=zPhi[0]
        # estimate ampl of big arc (A)
        A_s=np.mean(zAbs)
        print('F0_s:',F0_s,'A_s:',A_s)
        
        #Ta is proportinal to the big arch span
        dx=x[-1]-x[0]
        dphi=(zPhi[-1]-zPhi[0])/2/np.pi
        Ta_s=dphi/dx
        print('Ta_s:',dx,dphi,Ta_s)

        #B is 1/2 of max - min of amplitude
        # Tb distance between 2 minima in the amplitude
        ''' algo:  
            - find min in the center 
            - find zero crossing to the left
            - find zero crossing to the right
            period=2x index difference
        '''
        idxLen=zAbs.shape[0]
        nClip=idxLen//4
        #print('idxLen=',idxLen,nClip)
        assert nClip>10
        zAbsC=zAbs[nClip:-nClip]
        idx0=np.argmin(zAbsC)
        ampMin=zAbsC[idx0]
        #print('idx0',idx0,ampMin)
        idx0+=nClip
        ampMin=zAbs[idx0]
        #print('idx0',idx0,ampMin)
        i=0
        while zAbs[i+idx0] <A_s : i+=1
        iR=i
        i=0
        while zAbs[i+idx0] <A_s : i-=1
        iL=i
        iD=iR-iL
        print('iL R',iL,iR,iD,dx/iD/4., 9./iD)
        assert iD>5  # period too narrow, retake date with smaller freq step
        assert iD<nClip # period too large, use wider freq range
        Tb_s=0.45/iD/(x[1]-x[0])
        B_s=(max(zAbsC) -min(zAbsC))/2.

        print('B_s:',B_s,'Tb_s:',Tb_s)
        
        
        params = self.make_params(F0=F0_s, A=A_s, Ta=Ta_s, B=B_s, Tb=Tb_s, Tau=0.)

        #params['Q'].set(min=Q_min, max=Q_max)
        
        # fix  some params to test sensitivity
        #params['T'].vary = False
        #params['Tau'].vary = False
        return lmfit.models.update_param_vals(params, self.prefix, **kwargs)


########################################################
# Now let's use the model to generate some fake data:
np.random.seed(123)
showGuess=True
showFit=True

nf_step=200
f = np.linspace(1, 40, nf_step)

resonator = ResonatorModel()
true_params = resonator.make_params(F0=2.5 ,A=1.1,Ta=-0.01, B=0.2, Tb=0.1, Tau=0.1)  # keep decay below 0.1
true_y = resonator.eval(params=true_params, x=f)
noise_scale = 0.03
weights=np.zeros(nf_step)+ 1./noise_scale

measured_y = true_y + noise_scale*(np.random.randn(nf_step) + 1j*np.random.randn(nf_step))

print('freq:',f.shape,f.dtype)
print('measured_y:',measured_y.shape,measured_y.dtype)


################################################################
# Try out the guess method we added:

guess = resonator.guess(measured_y, x=f, verbose=True)
print('\n guess:')
print(guess)
guess_y = resonator.eval(params=guess, x=f)

plt.figure(facecolor='white', figsize=(14,8))
nrow,ncol=2,2

ax = plt.subplot(nrow, ncol, 1)
zAbs=np.abs(measured_y)
zPhi=np.unwrap(np.angle(measured_y))

guess_zAbs=np.abs(guess_y)
guess_zPhi=np.unwrap(np.angle(guess_y))

ax.plot(f, zAbs, '.', label='noisy data')
if showGuess:
    ax.plot(f, guess_zAbs, 'k--', label='initial guess')
ax.set(ylabel='abs(Z)',xlabel='MHz', title='simulated data')
ax.plot(f[0:1],zAbs[0:1],'*',mfc='magenta', ms=15.)
ax.grid()
#ax.set_ylim(0,)
ax.legend(loc='best')

ax = plt.subplot(nrow, ncol, 2)
ax.plot(f,zPhi, '.',color='green', label='noisy data')
if showGuess:
    ax.plot(f, guess_zPhi, 'k--', label='initial guess')
ax.plot(f[0:1],zPhi[0:1],'*',mfc='magenta', ms=15.)
ax.set(ylabel='Phase (rad)',xlabel='frequency (MHz)', title='simulated data')
ax.grid()
ax.legend(loc='best')
#plt.show() # just input data

################################################################
# And now fit the data using the guess as a starting point:

result = resonator.fit(measured_y, params=guess, x=f, verbose=True, weights=weights)

print('\n fit result:')
print(result.fit_report() + '\n')
result.params.pretty_print()
redchi=result.redchi
ch2Txt='chi/DOF=%.2f'%redchi
print(ch2Txt)


######################################################################
# Now we'll make some plots of the data and fit. Define a convenience function
# for plotting complex quantities:

def plot_ri(ax,data, *args, **kwargs):
    ax.plot(data.real, data.imag, *args, **kwargs)
    ax.plot(data.real[0:1],data.imag[0:1],'*',mfc='magenta', ms=15.)


fit_y = resonator.eval(params=result.params, x=f)

ax = plt.subplot(nrow, ncol, 3)
plot_ri(ax,measured_y, '.')
if showFit:
    plot_ri(ax,fit_y, 'r.-', label='best fit',linewidth=0.8)
if showGuess:
    plot_ri(ax,guess_y, 'k--', label='inital guess')
ax.legend(loc='best')
ax.set(xlabel='Re(Z)', ylabel='Im(Z)',title=ch2Txt)
ax.set_aspect(1.0)

ax = plt.subplot(nrow, ncol, 4)
ax.plot(f, np.abs(measured_y), '.')
if showFit:
    ax.plot(f, np.abs(fit_y), 'r.-', label='best fit',linewidth=0.8)
if showGuess:
    ax.plot(f, np.abs(guess_y), 'k--', label='initial guess')
ax.legend(loc='best')
ax.set( ylabel='abs(Z)', xlabel='MHz')

plt.show()
