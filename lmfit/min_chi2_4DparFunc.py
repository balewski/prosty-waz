#!/usr/bin/env python3

"""
Based on: https://lmfit.github.io/lmfit-py/examples/example_brute.html

Use simplex minimizer to find 2D minimum of elliptic well 
*) code is fully vectorized for arbitrary num of dims for x

"""
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import matplotlib
matplotlib.use('TkAgg')

import numpy as np
import copy
from lmfit import Minimizer, Parameters, fit_report

#=================================================================
#=================================================================
#=================================================================



# ------------------  cool graphics ------------------
def plot_results_brute(result, best_vals=True, varlabels=None,
                       output=None):
    """Visualize the result of the brute force grid search.

    The output file will display the chi-square value per parameter and contour
    plots for all combination of two parameters.

    Inspired by the `corner` package (https://github.com/dfm/corner.py).

    Parameters
    ----------
    result : :class:`~lmfit.minimizer.MinimizerResult`
        Contains the results from the :meth:`brute` method.

    best_vals : bool, optional
        Whether to show the best values from the grid search (default is True).

    varlabels : list, optional
        If None (default), use `result.var_names` as axis labels, otherwise
        use the names specified in `varlabels`.

    output : str, optional
        Name of the output PDF file (default is 'None')
    """
    npars = len(result.var_names)
    _fig, axes = plt.subplots(npars, npars)

    if not varlabels:
        varlabels = result.var_names
    if best_vals and isinstance(best_vals, bool):
        best_vals = result.params

    for i, par1 in enumerate(result.var_names):
        for j, par2 in enumerate(result.var_names):

            # parameter vs chi2 in case of only one parameter
            if npars == 1:
                axes.plot(result.brute_grid, result.brute_Jout, 'o', ms=3)
                axes.set_ylabel(r'$\chi^{2}$')
                axes.set_xlabel(varlabels[i])
                if best_vals:
                    axes.axvline(best_vals[par1].value, ls='dashed', color='r')

            # parameter vs chi2 profile on top
            elif i == j and j < npars-1:
                if i == 0:
                    axes[0, 0].axis('off')
                ax = axes[i, j+1]
                red_axis = tuple([a for a in range(npars) if a != i])
                ax.plot(np.unique(result.brute_grid[i]),
                        np.minimum.reduce(result.brute_Jout, axis=red_axis),
                        'o', ms=3)
                ax.set_ylabel(r'$\chi^{2}$')
                ax.yaxis.set_label_position("right")
                ax.yaxis.set_ticks_position('right')
                ax.set_xticks([])
                if best_vals:
                    ax.axvline(best_vals[par1].value, ls='dashed', color='r')

            # parameter vs chi2 profile on the left
            elif j == 0 and i > 0:
                ax = axes[i, j]
                red_axis = tuple([a for a in range(npars) if a != i])
                ax.plot(np.minimum.reduce(result.brute_Jout, axis=red_axis),
                        np.unique(result.brute_grid[i]), 'o', ms=3)
                ax.invert_xaxis()
                ax.set_ylabel(varlabels[i])
                if i != npars-1:
                    ax.set_xticks([])
                elif i == npars-1:
                    ax.set_xlabel(r'$\chi^{2}$')
                if best_vals:
                    ax.axhline(best_vals[par1].value, ls='dashed', color='r')

            # contour plots for all combinations of two parameters
            elif j > i:
                ax = axes[j, i+1]
                red_axis = tuple([a for a in range(npars) if a not in (i, j)])
                X, Y = np.meshgrid(np.unique(result.brute_grid[i]),
                                   np.unique(result.brute_grid[j]))
                lvls1 = np.linspace(result.brute_Jout.min(),
                                    np.median(result.brute_Jout)/2.0, 7, dtype='int')
                lvls2 = np.linspace(np.median(result.brute_Jout)/2.0,
                                    np.median(result.brute_Jout), 3, dtype='int')
                lvls = np.unique(np.concatenate((lvls1, lvls2)))
                ax.contourf(X.T, Y.T, np.minimum.reduce(result.brute_Jout, axis=red_axis),
                            lvls, norm=LogNorm())
                ax.set_yticks([])
                if best_vals:
                    ax.axvline(best_vals[par1].value, ls='dashed', color='r')
                    ax.axhline(best_vals[par2].value, ls='dashed', color='r')
                    ax.plot(best_vals[par1].value, best_vals[par2].value, 'rs', ms=3)
                if j != npars-1:
                    ax.set_xticks([])
                elif j == npars-1:
                    ax.set_xlabel(varlabels[i])
                if j - i >= 2:
                    axes[i, j].axis('off')

    if output is not None:
        plt.savefig(output)

#=================================================================
#=================================================================
#=================================================================


#  M A I N    C O D E 

####################################################################
# We start off by generating some synthetic data with noise for a decaying sine wave, define an objective function and create a Parameter set.
x = np.linspace(0, 15, 301)
np.random.seed(7)
noise_sig=0.3
noise = np.random.normal(size=x.size, scale=noise_sig)
data = (5. * np.sin(2*x - 0.1) * np.exp(-x*0.08) + noise)
#plt.plot(x, data, 'b')


def fcn2min(params, x, data):
    """Model decaying sine wave, subtract data. residua normalized: chi2/dof-->1"""
    amp = params['amp']
    shift = params['shift']
    omega = params['omega']
    decay = params['decay']
    model = amp * np.sin(x*omega + shift) * np.exp(-x*decay)
    return (model - data)/noise_sig  # no chi2/dof should be 1


# create a set of Parameters
params = Parameters()
params.add('amp', value=7, min=2.5)
params.add('decay', value=0.05, min=0.01)
params.add('shift', value=0.0, min=-np.pi/2., max=np.pi/2)
params.add('omega', value=3, min=1)

####################################################################
# initialize minimizer
# varying parameters need the brute_step attribute specified,
params['amp'].set(brute_step=0.4)
params['decay'].set(brute_step=0.01)
params['omega'].set(brute_step=0.2)
print('M:initilized params')
params.pretty_print()

print('M:initialize a Minimizer and perform the grid search:')
fitter = Minimizer(fcn2min, params, fcn_args=(x, data))
result_brute = fitter.minimize(method='brute', Ns=20, keep=5)

''' In a more realistic, complicated example the brute method will be used to get reasonable values for the parameters and perform another minimization (e.g., using leastsq) using those as starting values. That is where the keep` parameter comes into play: it determines the “number of best candidates from the brute force method that are stored in the candidates attribute”. 
'''

# for 'shift' parameter we have NOT sepciffied 'brute_step' so it somehow computes the grid using 'Ns'
par_name = 'shift'
indx_shift = result_brute.var_names.index(par_name)
grid_shift = np.unique(result_brute.brute_grid[indx_shift].ravel())
print("M:parameter = {}\nnumber of steps = {}\ngrid = {}".format(par_name,len(grid_shift), grid_shift))

print('M:Optimal solution')
print(fit_report(result_brute))


print('\nM:==========  show grid search best candidates #1 and #3 ========')
result_brute.show_candidates(1)
result_brute.show_candidates(3)

plt.plot(x, data, 'b',label='input data')
plt.plot(x, data + fcn2min(result_brute.params, x, data), 'r--',label='brute')


####################################################################
# 2nd minimization pass using 'lastsq' method
best_result = copy.deepcopy(result_brute)
#print('M:num brute results:',best_result.size())
k=0
for candidate in result_brute.candidates:
    trial = fitter.minimize(method='leastsq', params=candidate.params)
    print('trial',k,trial.chisqr , best_result.chisqr)
    k+=1
    if trial.chisqr < best_result.chisqr:
        best_result = trial

print('M: done  leastsq minimization,   the most optimal result:')
print(fit_report(best_result))

plt.plot(x, data + fcn2min(best_result.params, x, data), 'g--',label='brute followed by leastsq')
plt.legend()

plot_results_brute(result_brute, best_vals=True, varlabels=None)
plt.show()

