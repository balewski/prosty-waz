#!/usr/bin/env python3
''' computes two-point correlation for 3D mass distribution
Example data & code from Venkites
This computes spectrum for the first image in the batch of 32 images
f_compute_spectrum_3d(a1[0])
Output is : 
array([7.08387989e+06, 3.28201950e+06, 1.96989945e+06, 1.29067951e+06,
       8.88855280e+05, 5.79303274e+05, 4.77015606e+05, 3.58533467e+05,
       2.55271201e+05, 1.87804421e+05, 1.52941311e+05, 1.20373592e+05,
       8.99020955e+04, 6.93547466e+04, 5.53105024e+04, 4.32803411e+04,
       3.21364362e+04, 2.45587023e+04, 1.87231209e+04, 1.53876053e+04,
       1.13001430e+04, 9.40523400e+03, 7.63251142e+03, 6.31246125e+03,
       5.21533361e+03, 4.41272406e+03, 3.82497734e+03, 3.62602297e+03,
       3.14059634e+03, 2.73483562e+03, 3.66875483e+03, 2.10761531e+03,
       1.63054553e+03, 1.35017626e+03, 1.47162297e+03, 1.06022791e+03,
       1.01064532e+03, 9.81427116e+02, 9.53277740e+02, 8.38876481e+02,
       8.19499169e+02, 8.21931962e+02, 9.07073995e+02, 2.15897225e+03,
       6.72364738e+02, 6.55671060e+02, 1.36135356e+03, 5.34185661e+02,
       5.79213578e+02, 8.48842685e+02, 5.68254283e+02, 7.06984838e+02,
       6.71464160e+02])

'''

import numpy as np

#...!...!..................
def f_radial_profile_3d(data): # data are in momentum space
    z, y, x = np.indices((data.shape)) # Get a grid of x,y,z values
    print('RP:xyz',x.shape,x[0,0,0],x[-1,-1,-1])
    center = np.array([(x.max()-x.min())/2.0, (y.max()-y.min())/2.0, (z.max()-z.min())/2.0]) # compute centers

    print('RP:center',center)
    center=(np.array(data.shape)-1)/2.
    print('RP:center',center)
    # get radial values of every pair of points
    r = np.sqrt((x - center[0])**2 + (y - center[1])**2+ + (z - center[2])**2)
    r = r.astype(np.int) # It is the binned value of radial distance ‘r' (in the momentum space).
    print('RP:r',r.shape,r[0,0,0],r[-1,-1,-1])

    # np.ravel: A 1-D array, containing the elements of the input, is returned. A copy is made only if needed.
    
    # Compute histogram of r values
    tbin = np.bincount(r.ravel(), data.ravel())
    nr = np.bincount(r.ravel()) 
    radialprofile = tbin / nr

    print('RP: tbin',tbin.shape)
    return radialprofile[1:-1]

#...!...!..................
def f_compute_spectrum_3d(arr):
    y1=np.fft.fftn(arr)
    y1=np.fft.fftshift(y1)
    y2=abs(y1)**2
    z1=f_radial_profile_3d(y2)
    return(z1)

#=================================
#=================================
#  M A I N 
#=================================
#=================================

fname='/global/cfs/cdirs/m3363/vayyar/cosmogan_data/results_from_other_code/pytorch/results/3d/20210210_060657_3d_l0.5_80k/images/gen_img_epoch-15_step-37730.npy'
X=np.load(fname)[:,0,:,:,:]

# This is a file with 32, 3D images, with the second index being a channel
print('input X',X.shape)

# This computes spectrum for the first image in the batch of 32 images
Pk=f_compute_spectrum_3d(X[0])
print('M:',Pk.shape,Pk)

xBins=np.arange(len(Pk))
print('M:xBins',xBins)
