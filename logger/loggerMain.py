#!/usr/bin/python
# example taken from: 
# http://www.blog.pythonlibrary.org/2012/08/02/python-101-an-intro-to-logging/


import logging
import otherMod2
 
#----------------------------------------------------------------------
def main():
    """
    The main entry point of the application
    """
    logger = logging.getLogger("main") # base string for all messages
    logger.setLevel(logging.INFO)  # set lowest printable level
 
    # create the logging file handler
    logFile="Log.report"
    fh = logging.FileHandler(logFile) # physical log file
    print "Logger output:  cat ",logFile
 

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
 
    # add handler to logger object
    logger.addHandler(fh)
 
    logger.info("Program started")
    result = otherMod2.add(7, 8)
    logger.debug(" this will not print due to set level")
    logger.info("Done!") 
if __name__ == "__main__":
    main()


""" 
OUTPUT:
2015-07-14 23:16:25,232 - main - INFO - Program started
2015-07-14 23:16:25,235 - main.mod3.add4 - INFO - jan added 7 and 8 to get 15
2015-07-14 23:16:25,255 - main.mod3.add4 - DEBUG -  I was here, after 0.02 sec
2015-07-14 23:16:25,256 - main - INFO - Done!
"""
