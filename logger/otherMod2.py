# otherMod2.py
import logging
import time 

 
#----------------------------------------------------------------------
def add(x, y):
    """"""
    logger = logging.getLogger("main.mod3.add4")
    logger.setLevel(logging.DEBUG) # set lowest printable level
    logger.info("jan added %s and %s to get %s" % (x, y, x+y))
    time.sleep(0.02)
    logger.debug(" I was here, after 0.02 sec")
    return x+y
