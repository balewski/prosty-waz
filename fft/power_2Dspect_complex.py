#!/usr/bin/env python3
import numpy as np
import argparse,os
# based on https://dsp.stackexchange.com/questions/36902/calculate-1d-power-spectrum-from-2d-images

#...!...!..................
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2, 3], help="increase output verbosity", default=1, dest='verb')
   
    parser.add_argument( "-X","--noXterm", action='store_true', default=False, help="disable X-term for batch mode")
    parser.add_argument("--outPath", default='out/',help="output path for plots and tables")

    args = parser.parse_args()
    
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    if not os.path.exists(args.outPath):
        os.makedirs(args.outPath);   print('M: created',args.outPath)
    return args

#...!...!..................
def mini_plotter(args):
    import matplotlib as mpl
    if args.noXterm:
        mpl.use('Agg')  # to plot w/o X-server
        print('Graphics disabled')
    else:
        mpl.use('TkAgg') 
        print('Graphics started, canvas will pop-out')
    import matplotlib.pyplot as plt
    return plt

#...!...!..................
def save_fig( fid, ext='my', png=1):
     plt.figure(fid)
     plt.tight_layout()
     
     figName=args.outPath+'%s_f%d'%(ext,fid)
     if png: figName+='.png'
     else: figName+='.pdf'
     print('Graphics saving to ',figName)
     plt.savefig(figName)
     
#=================================
#=================================
#  M A I N 
#=================================
#=================================

if __name__ == "__main__":
    args=get_parser()
    plt=mini_plotter(args)

    # - - - - - - define data - - - - - - 
    shift = np.fft.fftshift
    from scipy import interpolate
    '''
    Create fractal noise
    '''
    nN, nE = 1024, 512
    dE, dN = 101., 134.  # Arbitrary values for sampling in dx and dy
    amplitude = 50.
    
    rfield = np.random.rand(nN, nE)
    spec = np.fft.fft2(rfield)

    regime = np.array([.15, .60, 1.])
    beta = np.array([5./3, 8./3, 2./3])
    beta += 1.  # Betas are defined for 1D PowerSpec, increasing dimension

    kE = np.fft.fftfreq(nE, dE)
    kN = np.fft.fftfreq(nN, dN)

    k = kN if kN.size < kE.size else kE
    k = k[k > 0]
    k_rad = np.sqrt(kN[:, np.newaxis]**2 + kE[np.newaxis, :]**2)

    k0 = 0
    k1 = regime[0] * k.max()
    k2 = regime[1] * k.max()

    r0 = np.logical_and(k_rad > k0, k_rad < k1)
    r1 = np.logical_and(k_rad >= k1, k_rad < k2)
    r2 = k_rad >= k2

    print('M: k_rad',k_rad.shape, 'r0,1,2=',r0.shape,r1.shape,r2.shape)
    amp = np.empty_like(k_rad)
    amp[r0] = k_rad[r0] ** -beta[0]
    amp[r0] /= amp[r0].max()

    amp[r1] = k_rad[r1] ** -beta[1]
    amp[r1] /= amp[r1].max()/amp[r0].min()

    amp[r2] = k_rad[r2] ** -beta[2]
    amp[r2] /= amp[r2].max()/amp[r1].min()

    amp[k_rad == 0.] = amp.max()

    amp *= amplitude**2
    spec *= np.sqrt(amp)  # We come from powerspec!
    noise = np.abs(np.fft.ifft2(spec))  # new 2D spectrum
    print('N:',noise.shape)


    ampE_slice = shift(amp)[amp.shape[0]//2, :amp.shape[1]//2]
    kE_slice = shift(k_rad)[amp.shape[0]//2, :amp.shape[1]//2]


    '''
    Noise analysis from random 2D spectrum
    '''
    spec = shift(np.fft.fft2(noise))
    pspec = np.abs(spec)**2
    pspec[k_rad == 0.] = 0.

    kE = shift(np.fft.fftfreq(spec.shape[1], dE))
    kN = shift(np.fft.fftfreq(spec.shape[0], dN))
    k_rad = np.sqrt(kN[:, np.newaxis]**2 + kE[np.newaxis, :]**2)

    kE = shift(np.fft.fftfreq(spec.shape[1], dE))
    kN = shift(np.fft.fftfreq(spec.shape[0], dN))

    power_interp = interpolate.RectBivariateSpline(kN, kE, pspec)

    def power2DMean(k, N=256):
        """ Mean 2D Power works! """
        theta = np.linspace(-np.pi, np.pi, N, False)
        power = np.empty_like(k)
        for i in range(k.size):
            kE = np.sin(theta) * k[i]
            kN = np.cos(theta) * k[i]
            power[i] = np.median(power_interp.ev(kN, kE) * 4 * np.pi)
            # Median is more stable than the mean here
        return power / pspec.size

    
    # - - - - just plotting - - - - 
    ncol,nrow=2,1; figId=4
    plt.figure(figId,facecolor='white', figsize=(8,4))
    ax=plt.subplot(nrow,ncol,1)

    ax.imshow(noise)
    ax.set_title('Fractal Noise')


    ax=plt.subplot(nrow,ncol,2)
    ax.loglog(kE_slice, ampE_slice, ls='--', label='Model Spec')
    ax.loglog(k, power2DMean(k), alpha=.6, label='Noise Spec')
    ax.legend()
    ax.set_title('2D Mean Spectrum')
     
    #ax.plot(X,Y,'o-')
    #ax.set(xlabel='time',ylabel='speed',title='parabola')
    
    save_fig(figId)
    plt.show()
