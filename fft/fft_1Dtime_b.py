#!/usr/bin/env python
# based on
# https://www.delftstack.com/howto/matplotlib/matplotlib.pyplot.specgram-in-python/

import math
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
import matplotlib.colors as colors

dt=0.0002
w=2
x=np.linspace(0,5,math.ceil(5/dt))
A=20*(np.sin(3 * np.pi * x))
samplingFrequency   = 400

print('A:',A.shape)

# Plot the signal
plt.subplot(211)
plt.plot(x,A)

plt.xlabel('Sample')
plt.ylabel('Amplitude')

# Plot the spectrogram
plt.subplot(212)


#The method returns three values f, t, and Sxx. f represents the array of sample frequencies, t represents the array of sample times and Sxx represents the spectrogram of A

if 1:
    f, t, Sxx = signal.spectrogram(A, fs=samplingFrequency, nfft=256)
    plt.pcolormesh(t, f, Sxx,shading='auto', norm=colors.LogNorm(vmin=Sxx.min(), vmax=Sxx.max()), cmap='PuBu_r' )

    print('fft:',f.shape,t.shape,Sxx.shape)
    #print('f:',f)
    #print('t:',t)

if 0:  # alternative:
    powerSpectrum, freqenciesFound, time, imageAxis = plt.specgram(A, Fs=samplingFrequency)
    #The darker the color of the spectrogram at a point, the stronger is the signal at that point.
    print('alt:', powerSpectrum.shape, freqenciesFound.shape, time.shape)

plt.xlabel('Time of audio')
plt.ylabel('Frequency')
plt.title('Z=LOG,  Darker=tronger')

plt.tight_layout()
plt.show()   
