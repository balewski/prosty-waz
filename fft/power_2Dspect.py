#!/usr/bin/env python3
import numpy as np
import argparse,os
# based on https://bertvandenbroucke.netlify.app/2019/05/24/computing-a-power-spectrum-in-python/

#...!...!..................
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2, 3], help="increase output verbosity", default=1, dest='verb')
   
    parser.add_argument( "-X","--noXterm", action='store_true', default=False, help="disable X-term for batch mode")
    parser.add_argument("--outPath", default='out/',help="output path for plots and tables")

    args = parser.parse_args()
    
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    if not os.path.exists(args.outPath):
        os.makedirs(args.outPath);   print('M: created',args.outPath)
    return args

#...!...!..................
def mini_plotter(args):
    import matplotlib as mpl
    if args.noXterm:
        mpl.use('Agg')  # to plot w/o X-server
        print('Graphics disabled')
    else:
        mpl.use('TkAgg') 
        print('Graphics started, canvas will pop-out')
    import matplotlib.pyplot as plt
    return plt

#...!...!..................
def save_fig( fid, ext='my', png=1):
     plt.figure(fid)
     plt.tight_layout()
     
     figName=args.outPath+'%s_f%d'%(ext,fid)
     if png: figName+='.png'
     else: figName+='.pdf'
     print('Graphics saving to ',figName)
     plt.savefig(figName)


#...!...!..................    
def power_2Dcloud(image,d=1):  # d: Sample spacing (inverse of the sampling rate)
    print('Pow2D: img',image.shape)
    npix = image.shape[0]
    assert npix == image.shape[1]
    assert npix%2==0  # for computation of kvals
    
    fourier_image = np.fft.fftn(image)
    fourier_amplitudes2= np.abs(fourier_image)**2  

    kfreq = np.fft.fftfreq(npix) * npix  
    kfreq2D = np.meshgrid(kfreq, kfreq)
    knrm = np.sqrt(kfreq2D[0]**2 + kfreq2D[1]**2)

    knrm = knrm.flatten()
    fourier_amplitudes2 = fourier_amplitudes2.flatten()

    kbins = np.arange(0.5, npix//2+1, 1.)
    #kvals = 0.5 * (kbins[1:] + kbins[:-1])
    
    Abins, _, _ = stats.binned_statistic(knrm, fourier_amplitudes2,  statistic = "mean", bins = kbins)
    Abins *= np.pi * (kbins[1:]**2 - kbins[:-1]**2)

    kphys=np.fft.fftfreq(npix, d=d)
    kvals=kphys[:npix//2]
    return kvals[1:],Abins[1:]  # k,P, skip 0-freq, k  units: 1/d 

    ''' Notes
    fft.fftfreq(n, d=1.0) - Discrete Fourier Transform sample frequencies.
       E.g: if the sample spacing is in seconds, then the frequency unit is Hz.
       d: Sample spacing (inverse of the sampling rate).

    f = [0, 1, ...,   n/2-1,     -n/2, ..., -1] / (d*n)   if n is even
    f = [0, 1, ..., (n-1)/2, -(n-1)/2, ..., -1] / (d*n)   if n is odd
    
    fft.fftshift(x, axes=None) - Shift the zero-frequency component to the center of the spectrum.

    freqs = np.fft.fftfreq(10, 0.1)
    >>>array([ 0.,  1.,  2., ..., -3., -2., -1.])
    np.fft.fftshift(freqs)
    >>>array([-5., -4., -3., -2., -1.,  0.,  1.,  2.,  3.,  4.])

    '''

#=================================
#=================================
#  M A I N 
#=================================
#=================================

if __name__ == "__main__":
    args=get_parser()
    plt=mini_plotter(args)

    # - - - - - - define data - - - - - - 
    import matplotlib.image as mpimg
    import scipy.stats as stats
    inpF="clouds.png"
    image = mpimg.imread(inpF)
    #nPix=500 ;  image=image[:nPix,:nPix]  # crop image here
    k,P=power_2Dcloud(image,d=1)
    print('M:k',k.shape,P.shape)
    if k.shape[0]<30: print(k)
    
    
    # - - - - just plotting - - - - 
    ncol,nrow=2,1; figId=4
    plt.figure(figId,facecolor='white', figsize=(8,4))
    ax=plt.subplot(nrow,ncol,1)

    ax.imshow(image)
    ax.set(title=inpF, xlabel='dist (m)', ylabel='dist (m)')


    ax=plt.subplot(nrow,ncol,2)
    ax.loglog(k, P)
    ax.set(xlabel="k (1/m)",ylabel='P(k)',title='power spectrum')
    plt.tight_layout()
    save_fig(figId)
    plt.show()
