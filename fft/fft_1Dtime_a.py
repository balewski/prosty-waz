#!/usr/bin/env python
# plbased on
# https://pythontic.com/visualization/signals/spectrogram
# The specgram() method uses Fast Fourier Transform(FFT) to get the frequencies present in the signal

import matplotlib.pyplot as plt
import numpy as np

# Define the list of frequencies
frequencies         = np.arange(5,105,5)

# Sampling Frequency
samplingFrequency   = 400

 
# Create two ndarrays
s1 = np.empty([0]) # For samples
s2 = np.empty([0]) # For signal

# Start Value of the sample
start   = 1
# Stop Value of the sample
stop    = samplingFrequency+1
 

for frequency in frequencies:
    sub1 = np.arange(start, stop, 1)
    
    # Signal - Sine wave with varying frequency + Noise
    sub2 = np.sin(2*np.pi*sub1*frequency*1/samplingFrequency)+np.random.randn(len(sub1))

    s1      = np.append(s1, sub1)
    s2      = np.append(s2, sub2)  

    start   = stop+1
    stop    = start+samplingFrequency
 
# Plot the signal
plt.subplot(211)
plt.plot(s1,s2)

plt.xlabel('Sample')
plt.ylabel('Amplitude')

# Plot the spectrogram
plt.subplot(212)
powerSpectrum, freqenciesFound, time, imageAxis = plt.specgram(s2, Fs=samplingFrequency)

plt.xlabel('Time')
plt.ylabel('Frequency')

plt.tight_layout()
plt.show()   
