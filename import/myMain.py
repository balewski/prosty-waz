#!/usr/bin/python
# good examples: http://stackoverflow.com/questions/2349991/python-how-to-import-other-python-files
print "start1 - here you need firts source the source-me file"
# import from this direcotry
from Toms import TomsF
TomsF()
print "import from 'code1' will fail if it not included in PYTHONPATH"
#import from other directry , will fail if it is not included in syst.var PYTHONPATH
from Pots import PotsF
PotsF()

exit(0)
# this variable prints content of all searched directories, including PYTHONPATH
import  sys
print sys.path


