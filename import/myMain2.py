#!/usr/bin/python
# good examples: http://stackoverflow.com/questions/2349991/python-how-to-import-other-python-files
print "start2 - here the path is set inside python code"
# import from this direcotry
from Toms import TomsF
TomsF()
print "import from 'code1' will fail if it not included in PYTHONPATH"
#import from other directry , will fail if it is not included in syst.var PYTHONPATH

import sys 
import os
sys.path.append(os.path.abspath("/home/pi/prosty-waz/import/code1"))
from Pots import *

PotsF()

print sys.path




