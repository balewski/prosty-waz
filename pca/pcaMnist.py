#!/usr/bin/env python3

# Code based on
# https://towardsdatascience.com/pca-using-python-scikit-learn-e653f8989e60
# modiffied by Jan Balewski, uses data standarization

import numpy as np
import matplotlib.pyplot as plt
from numpy import linalg as LA

from sklearn import decomposition
from sklearn.preprocessing import StandardScaler

np.random.seed(5)

from tensorflow.keras.datasets import mnist
(X_train, y_train), (X_test, y_test) = mnist.load_data()
print('X_train',X_train.shape)
X=X_train.reshape( (X_train.shape[0],-1))
print('X',X.shape,X.dtype,X[::40,::90])

print("Standardize the Data: z = (x - u) / s")
scaler = StandardScaler()
# Fit on training set only.
scaler.fit(X)
sc_s=scaler.scale_
sc_m=scaler.mean_
sc_v=scaler.var_
print('scaler scale:',sc_s.shape,sc_s[::40])
print('scaler mean:',sc_m.shape,sc_m[::40])
print('scaler variance:',sc_v.shape,sc_v[::40])
print('scaler n_samples_seen_:',scaler.n_samples_seen_)

# Apply transform to both the training set and the test set.
Xnor = scaler.transform(X)

print('\n Fit PCA')
pca = decomposition.PCA(.85)
#pca = decomposition.PCA(n_components=784)
pca.fit(Xnor)

b=pca.explained_variance_
print('explained_variance:',b.shape,b[:5],sum(b))

c=pca.explained_variance_ratio_
expl_var=sum(c)
print('explained_variance_ratio',c.shape,c[:5],expl_var)

d=pca.singular_values_
print('?singular_values',d.shape,d[:5])

trans_mean=pca.mean_
print('mean',trans_mean.shape, trans_mean[::50])
print('n_components:',pca.n_components_)
print('noise_variance:',pca.noise_variance_)


print('\n transform data manually to PCs')
Xpc = np.dot(Xnor-pca.mean_, pca.components_.T) # transform
print('Xpc',Xpc.shape,Xpc[::40,::20])

print('\n inverse transform to Xnor, and back to oryginal')
Xnor2=np.dot(Xpc, pca.components_) + pca.mean_
print('Xpc',Xnor2.shape,Xnor2[::40,::20])

X2=scaler.inverse_transform(Xnor2)
print('X2',X2.shape,X2.dtype,X2[::40,::30])


# just plotting
plt.figure(figsize=(8,8));

imid=34

for i in range(2):
    imid+=1
    # Original Image
    plt.subplot(2, 2, 1+2*i);
    plt.imshow(X[imid].reshape(28,28),
               cmap = plt.cm.gray, interpolation='nearest',
               clim=(0, 255));
    plt.xlabel('784 components')
    plt.title('Original y=%d'%y_train[imid])

    # Original Image
    plt.subplot(2, 2, 2+2*i);
    plt.imshow(X2[imid].reshape(28,28),
               cmap = plt.cm.gray, interpolation='nearest',
               clim=(0, 255));
    plt.xlabel('%d components'%pca.n_components_)
    plt.title('Pca--> Pca^-1, expl_var=%.2f'%expl_var);

plt.show()
