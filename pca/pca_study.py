import numpy as np
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

def perform_pca(x, y):
    """
    Perform PCA on two 1D numpy arrays and return the principal components and variance.

    :param x: 1D numpy array
    :param y: 1D numpy array
    :return: 2D numpy array containing principal components and explained variance
    """
    # Combine x and y into a single 2D array
    data = np.column_stack((x, y))

    # Standardize the data
    scaler = StandardScaler()
    data_std = scaler.fit_transform(data)

    # Create a PCA instance and fit the standardized data
    pca = PCA(n_components=2)
    pca.fit(data_std)

    # Extract the principal components and explained variance
    components = pca.components_
    explained_variance = pca.explained_variance_ratio_

     # Compute the magnitude of each principal component
    magnitudes = np.linalg.norm(components, axis=1)

    # Combine components with their respective explained variance and magnitude
    result = np.column_stack((components, explained_variance, magnitudes))

    return result

# Example data
x = np.random.rand(100)
y = np.random.rand(100)

# Perform PCA
pca_result = perform_pca(x, y)

# Print the result
print("PCA Result: [PCx,PCy,var,magn]")
print(pca_result)
