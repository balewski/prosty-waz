#!/usr/bin/env python3

# Code source: Gaoel Varoquaux
# License: BSD 3 clause
# modiffied by Jan Balewski, no data standarization

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from numpy import linalg as LA

from sklearn import decomposition
from sklearn import datasets

# https://scikit-learn.org/stable/modules/generated/sklearn.decomposition.PCA.html

np.random.seed(5)

iris = datasets.load_iris()
X = iris.data
y = iris.target

print('Xraw',X.shape)
# Make an instance of the Model
#pca = decomposition.PCA(.95)

pca = decomposition.PCA(n_components=3)
pca.fit(X)
Xpc = pca.transform(X)
print('Xpc',Xpc.shape)

trans_comp=pca.components_
print('components',trans_comp.shape,trans_comp)
DN1=LA.norm(trans_comp, axis=0)
print('DN1 per inp:',DN1,DN1.shape)
DN1=LA.norm(trans_comp, axis=1)
print('DN1 per out:',DN1,DN1.shape)

b=pca.explained_variance_
print('explained_variance:',b.shape,b)

c=pca.explained_variance_ratio_
print('explained_variance_ratio',c.shape,c,sum(c))

d=pca.singular_values_
print('?singular_values',d.shape,d)

trans_mean=pca.mean_
print('mean',trans_mean.shape,trans_mean)
print('n_components:',pca.n_components_)
print('noise_variance:',pca.noise_variance_)

# try manually forward & backward PCA transformation
# https://stackoverflow.com/questions/32750915/pca-inverse-transform-manually

#try forward  PCA transform manually
X2 = np.dot(X-pca.mean_, pca.components_.T) # transform
print('X2',X2.shape)
print('Xpc:',Xpc[:2])
print('X2:',X2[:2])
D2=Xpc-X2
DN2=LA.norm(D2, axis=1)
print('DN2:',DN2[:2],DN2.shape)

#try to invert PCA
X3=np.dot(Xpc, pca.components_) + pca.mean_
print('X3',X3.shape)
print('X:',X[:2])
print('X3:',X3[:2])
D3=X-X3
DN3=LA.norm(D3, axis=1)
print('DN3:',DN3[:5],DN3.shape)


# plotting
fig = plt.figure(1, figsize=(4, 3))
plt.clf()
ax = Axes3D(fig, rect=[0, 0, .95, 1], elev=48, azim=134)

plt.cla()

for name, label in [('Setosa', 0), ('Versicolour', 1), ('Virginica', 2)]:
    ax.text3D(X[y == label, 0].mean(),
              X[y == label, 1].mean() + 1.5,
              X[y == label, 2].mean(), name,
              horizontalalignment='center',
              bbox=dict(alpha=.5, edgecolor='w', facecolor='w'))
# Reorder the labels to have colors matching the cluster results
y = np.choose(y, [1, 2, 0]).astype(np.float)
ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=y, cmap=plt.cm.nipy_spectral,
           edgecolor='k')

ax.w_xaxis.set_ticklabels([])
ax.w_yaxis.set_ticklabels([])
ax.w_zaxis.set_ticklabels([])

plt.show()
