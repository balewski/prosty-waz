#!/usr/bin/env python3
'''
Find correlation angle between 2 sets of XY-data
'''
import os,sys
sys.path.append(os.path.abspath("../"))
from H5_py.Util_H5io3 import  write3_data_hdf5, read3_data_hdf5

from scipy import stats
from sklearn.decomposition import PCA
import numpy as np
class DataEmpty:    pass  # empty class

import argparse,os

#...!...!..................
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument( "-X","--noXterm", action='store_true', default=False, help="disable X-term for batch mode")
    parser.add_argument("--outPath", default='out/',help="output path for plots and tables")

    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#............................
#............................
#............................
class MiniPlotter():
#...!...!..................
    def __init__(self,args):
        import matplotlib as mpl
        if args.noXterm:
            mpl.use('Agg')  # to plot w/o X-server
            print('Graphics disabled')
        else:
            mpl.use('TkAgg')
            print('Graphics started, canvas will pop-out')
        import matplotlib.pyplot as plt
        self.plt=plt
        self.args=args
#...!...!..................
    def show(self):
        #self.plt.figure(fid)
        #self.plt.tight_layout()
        #print('Graphics saving to ',figName)
        #self.plt.savefig(figName)
        self.plt.show()

        

#...!...!..................
def angle_between(vector1, vector2):
    """ Returns the angle in radians between given vectors"""

    def unit_vector(vector):
        """ Returns the unit vector of the vector"""
        return vector / np.linalg.norm(vector)

    v1_u = unit_vector(vector1)
    v2_u = unit_vector(vector2)
    minor = np.linalg.det(
        np.stack((v1_u[-2:], v2_u[-2:]))
    )
    if minor == 0:
        raise NotImplementedError('Too odd vectors =(')
    return np.sign(minor) * np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

#...!...!..................
def std_phi_pca1(pca):
    assert pca.n_components_==2
    n=pca.n_samples_
    assert n>2
    [s1,s2]=np.sqrt(pca.explained_variance_)
    phi=np.arctan(s2/np.sqrt(n)/s1)
    print('OV:phi=%.3f'%phi)
    return phi
        
#............................
#............................
#............................
class Plotter(MiniPlotter):
    def __init__(self, args):
        MiniPlotter.__init__(self,args)

#...!...!..................
    def overlay_pca(self,ax,pca):
        n=pca.n_components_
        M=pca.mean_
        ax.plot([M[0]],[M[1]],"*",markersize=30)
        PC=pca.components_
        angErr=std_phi_pca1(pca)
        PV=pca.explained_variance_
        for j in range(n):
            P=PC[j]
            exv=PV[j]
            print('OV:P',j,P,'exvar=',exv)
            dCol='C%d'%j
            ax.arrow(M[0],M[1], P[0],P[1],color=dCol,head_length=0.06,head_width=0.08,width=0.025,alpha=0.6)
            angVal=np.arctan2(P[1],P[0])
        
            txt1='P%d ang=%.2f +/-%.2f (rad) = %.1f +/-%.1f(deg)'%(j,angVal,angErr,angVal*57.296,angErr*57.296)
            print(txt1)
            ax.text(0.01,0.1-j*0.04,txt1,transform=ax.transAxes,color=dCol)#,fontsize=12)
            if j==1: continue
            # line along P
            fac=1.5
            a=M+P*fac
            b=M-P*fac
            d=np.stack((a,b))
            print('d:',d)
            ax.plot(d[:,0],d[:,1],color=dCol,linewidth=0.7)

            # rotate P
            for i in [1,-1]:
                theta=i*angErr  #np.deg2rad(-30)            
                rot = np.array([[np.cos(theta), -np.sin(theta)], [np.sin(theta), np.cos(theta)]])
                Pr=np.dot(rot, P)
                d=np.stack((M,Pr*fac+M))
                print('d:',i,d)
                ax.plot(d[:,0],d[:,1],color=dCol,linewidth=0.7)


    
#...!...!..................
    def expectation_xy(self,xy,uv,plDD,figId=6):

        fig=self.plt.figure(figId,facecolor='white', figsize=(6,6))
        nrow,ncol=1,1
        ax = self.plt.subplot(nrow,ncol,1)
        ax.set_aspect(1.0)
        dd=1.5        
        ax.set_ylim(-dd,+dd)
        ax.set_xlim(-dd,+dd)
        ax.grid()
        ax.legend(loc='best')

        ax.scatter(xy[0],xy[1],alpha=0.7,color='r',label='xy')
        ax.scatter(uv[0],uv[1],alpha=0.7,color='g',label='uv')
        ax.legend(loc='best')
        return ax
        
#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__=="__main__":
    args=get_parser()

    plDD={}
    xyA=None
    
    blob,_=read3_data_hdf5('xyQ1.h5')
    xy=blob['xy']#[:,::4]
    #xy=np.hstack((xy,xy))
    print('M:xy:',xy.shape)
    
    # create the PCA instance
    pca = PCA(2)
    # fit PCA on data
    
    xyT=xy.T
    pca.fit(xyT)
    # access values and vectors
    print('M:pca:comp',pca.components_)
    print('M:pca.expl var:',pca.explained_variance_)

    P=pca.components_[0]

    # get the direction
    D=np.mean(xy[:,3:10],axis=1)
    print('D:',D)
    ang=angle_between(P,D)
    print('M: P1 ang=',ang)
    if np.abs(ang)>np.pi/2.: P=-P

    print('PCA mean',pca.mean_)


    #plDD['PM']=[P,M]
    # transform data
    uvT = pca.transform(xyT)
    uv=uvT.T
    uv=D  # direction of PCA1
    #uv=M # center of gravity
    #xy=xy[:,:10]
    
    # ....  plotting ........
    
    plot=Plotter(args)
    ax=plot.expectation_xy(xy,uv,plDD)
    plot.overlay_pca(ax,pca)

    plot.show()
    
    print('M:done')


