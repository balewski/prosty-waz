# use: 'Doctest'
# unittest embedded  test module inside function being tested

def square(x):
    """Return the square of x.

    >>> square(2)
    4
    >>> square(-2)
    4
    """

    return x * x +1

if __name__ == '__main__':
    import doctest
    doctest.testmod()
