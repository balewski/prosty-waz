#!/usr/bin/env python
# based on
#  https://jakevdp.github.io/PythonDataScienceHandbook/05.12-gaussian-mixtures.html

import sys

import numpy as np
import random
from matplotlib import pyplot as plt


from sklearn.datasets.samples_generator import make_blobs
X, y_true = make_blobs(n_samples=400, centers=4,
                       cluster_std=0.60, random_state=0)
X = X[:, ::-1] # flip axes for better plotting

from sklearn.cluster import KMeans
kmeans = KMeans(4, random_state=0)
labels = kmeans.fit(X).predict(X)
plt.scatter(X[:, 0], X[:, 1], c=labels, s=40, cmap='viridis');

plt.show()
plt.figure(13,facecolor='white', figsize=(6,4))

