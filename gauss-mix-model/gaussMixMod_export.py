#!/usr/bin/env python3
''' based on
Let's create a function that will help us visualize the locations and shapes of the GMM clusters by drawing ellipses based on the GMM output

Export/import GMM as YAML

https://stackoverflow.com/questions/25914614/how-to-save-write-opencv-em-gmm-model-using-python

'''

import matplotlib as mpl
mpl.use('TkAgg')

import numpy as np
from pprint import pprint

from matplotlib import pyplot as plt
from matplotlib.patches import Ellipse

# Generate some data
from sklearn.datasets.samples_generator import make_blobs
from sklearn import mixture

import sklearn
print('sklearn ver=',sklearn.__version__)

import ruamel.yaml  as yaml
import time,os
import warnings
warnings.simplefilter('ignore', yaml.error.MantissaNoDotYAML1_1Warning)

#...!...!..................
def read_yaml(ymlFn, verb=1):
        print('  read  yaml:',ymlFn,end='')
        ymlFd = open(ymlFn, 'r')
        bulk=yaml.load( ymlFd, Loader=yaml.CLoader)
        ymlFd.close()
        print(' done, size=%d'%len(bulk))
        if verb>1: print('see keys:',list(bulk.keys()))
        elif verb>2: pprint(bulk)
        return bulk

#...!...!..................
def write_yaml(rec,ymlFn,verb=1):
        start = time.time()
        ymlFd = open(ymlFn, 'w')
        yaml.dump(rec, ymlFd, Dumper=yaml.CDumper)
        ymlFd.close()
        xx=os.path.getsize(ymlFn)/1024
        if verb:
                print('  closed  yaml:',ymlFn,' size=%.1f kB'%xx,'  elaT=%.1f sec'%(time.time() - start))


#...!...!..................
def draw_ellipse(position, covariance, lab,ax=None, **kwargs):
    """Draw an ellipse with a given position and covariance"""
    ax = ax or plt.gca()
    
    # Convert covariance to principal axes
    if covariance.shape == (2, 2):
        U, s, Vt = np.linalg.svd(covariance)
        angle = np.degrees(np.arctan2(U[1, 0], U[0, 0]))
        width, height = 2 * np.sqrt(s)
    else:
        angle = 0
        width, height = 2 * np.sqrt(covariance)

    ax.text(position[0],position[1],'L=%d'%lab,c='m' )   
    # Draw the Ellipse
    for nsig in range(1, 4):
        ax.add_patch(Ellipse(position, nsig * width, nsig * height,
                             angle, **kwargs))
#...!...!..................        
def plot_gmm(gmm, X, ax, label=True):
    labels = gmm.predict(X)
    if label:
        ax.scatter(X[:, 0], X[:, 1], c=labels, s=40, cmap='viridis', zorder=2)
    else:
        ax.scatter(X[:, 0], X[:, 1], s=40, zorder=2)
    ax.axis('equal')
    
    w_factor = 0.2 / gmm.weights_.max()

    n_comp=gmm.get_params()['n_components']
        
    for pos, covar, w , lab in zip(gmm.means_, gmm.covariances_, gmm.weights_,range(n_comp)):
        draw_ellipse(pos, covar, lab, alpha=w * w_factor)


#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__=="__main__":
    X, y_true = make_blobs(n_samples=400, centers=4,
                           cluster_std=0.60, random_state=0)
    X = X[:, ::-1] # flip axes for better plotting
    print('X shape',X.shape)


    covTypeL=['full','diag','spherical']
    gmm = mixture.GaussianMixture(n_components=4, covariance_type=covTypeL[0])
    gmm.fit(X)

    print('extract GMM config')
    gmConf=gmm.get_params()
    print('gmConf:',type(gmConf))
    pprint(gmConf)
          

    print('Predict the labels for the data samples in X using trained model.')
    nt=6;     Xt=X[:nt]  # random
    nt=3;     Xt=X[[1,2,4]]  # selected from cluster#3
    
    label=gmm.predict(Xt)
    probs = gmm.predict_proba(Xt)    
    score=gmm.score_samples(Xt)  # weighted log probability for each sample
    avr_score=gmm.score(Xt)
    print('Xt:',Xt.shape,'probs:',probs.shape,'avr_score=',avr_score)
    print('Probabilistic cluster assignment')
    for i in range(nt):
        print('sample i=',i,' data:',Xt[i].round(2),'label:',label[i],' probs:',probs[i].round(2),'score:%.1f'%score[i])

   

    # export/import GMM as Yaml .....
    gmmF='myGmm1.yaml'
    write_yaml({'gmm1':gmm},gmmF)
    blob=read_yaml(gmmF)
    gmm1=blob['gmm1']

    
    print('just plotting of orig and restored GMMs...')
    plt.figure(10,facecolor='white', figsize=(6,8))
    nrow,ncol=2,1
    ax=plt.subplot(nrow, ncol, 1)
    ax.set_aspect(1.0)
    plot_gmm(gmm, X,ax)
    ax.plot(Xt[:,0],Xt[:,1],'*r')    
    plt.grid()

    ax=plt.subplot(nrow, ncol, 2)
    ax.set_aspect(1.0)
    plot_gmm(gmm1, X,ax)
    
    plt.show()




