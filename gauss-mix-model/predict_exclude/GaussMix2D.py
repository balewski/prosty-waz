from sklearn import mixture
import numpy as np

#...!...!..................
def covar_to_ellipse(center,cov):
    # Convert covariance to principal axes
    #print('cc',cov.shape,cov)
    if cov.shape == (2, 2):
        U, s, Vt = np.linalg.svd(cov)
        angle = np.degrees(np.arctan2(U[1, 0], U[0, 0]))
        width, height = 2 * np.sqrt(s)
    else:
        angle = np.float(0)
        width=height = 2 * np.sqrt(cov)
    #print('cc1',type(center),type(angle), type(width),type(height))
    #print('cc2',center.shape,angle.shape,width.shape,height.shape)
    return (center,angle,width,height)

#............................
#............................
#............................
class GaussMix2D(object):
    def __init__(self,num_clust=2,covType='full'):
        #'covariance_type' should be in ['spherical', 'tied', 'diag', 'full']

        self.gmm = mixture.GaussianMixture(n_components=num_clust, covariance_type=covType,verbose=1)
        self.covElli={}

#...!...!..................
    def fit(self,X):
        self.gmm.fit(X)
        print('GMM fit done')
        self.find_log_prob_bias()

#...!...!..................
    def find_log_prob_bias(self):
        pos2D=self.gmm.means_
        log_probs_raw = self.gmm._estimate_log_prob(pos2D)
        print('LP bias all:',log_probs_raw)
        self.log_prob_bias_raw=[ log_probs_raw[i,i] for i in range(pos2D.shape[0]) ]
        print('LP bias:',self.log_prob_bias_raw)
        
#...!...!..................
    def lock_clusters_ids(self):
        pos2D=self.gmm.means_
        nCl=pos2D.shape[0]
        print('sort %d clusters by y-position'%nCl,pos2D.shape,pos2D)
        xV=pos2D[:,1]
        clIdx=np.argsort(xV)
        print('xV:',xV,' idx:',clIdx)

        for k,i in enumerate(clIdx):
            a=self.gmm.means_[i]
            b=self.gmm.covariances_[i]
            self.covElli[k]=covar_to_ellipse(a,b)
        print('Gmix clust=%d, clIdx :'%nCl,clIdx)
        id2=['*']*nCl
        for i in range(nCl):
            id2[clIdx[i]]=i
        self.labelID=np.array(id2)
        print('Gmix clust=%d is mapped to labelID:'%nCl,self.labelID)

        
#...!...!..................
    def predict_exclude(self,X,kSig=None):
        nSamp=X.shape[0]
        print('PreEx: nSamp=%d, kSig='%nSamp,kSig)
        labels_raw = self.gmm.predict(X)
        
        log_probs_raw = self.gmm._estimate_log_prob(X)
        print('elp X:',X.shape,"LPR:",log_probs_raw.shape,'LR:',labels_raw.shape)
        if kSig!=None: log_prob_thr=-kSig*kSig/2
        
        log_probs=[]
        labels=[]
        for i in range(nSamp):
            labRaw=labels_raw[i]
            log_prob_i=log_probs_raw[i,labRaw] - self.log_prob_bias_raw[labRaw]
            labelId=self.labelID[labRaw] # user ordered label but w/o exclusion
            if kSig!=None and lob_prob_i < log_prob_thr: labelId=-1 # added exclusion
            log_probs.append(log_prob_i)
            labels.append(labelId)
        log_probs=np.array(log_probs)
        labels=np.array(labels)
        print('LP:', log_probs.shape)
        for i in range(nSamp):
            #if ytrue[i]!=1: continue
            #if labels[i]!=0: continue
            #if log_probs[i]>-2: continue
            print(i,'clust label raw=%d pred=%d log_prob=%.2f'%(labels_raw[i],labels[i],log_probs[i]),X[i])
            if i>20: break
        
        return labels,log_probs


#...!...!..................
    def predict_eval(self,X,ytrue):
        labels_raw = self.gmm.predict(X)
        labels=self.labelID[labels_raw] # now labels are user controlled but w/o exclusion
        log_probs_raw = self.gmm._estimate_log_prob(X)
        print('elp X:',X.shape,"LPR:",log_probs_raw.shape,'LR:',labels_raw.shape)
        log_probs=[]
        for i in range(labels_raw.shape[0]):
            xSig=log_probs_raw[i,labels_raw[i]] - self.log_prob_bias_raw[labels_raw[i]]
            log_probs.append(xSig)
        log_probs=np.array(log_probs)
        print('LP:', log_probs.shape)
        for i in range(labels.shape[0]):
            #if ytrue[i]!=1: continue
            if labels[i]!=0: continue
            if log_probs[i]>-2: continue
            print(i,'clust true=%d raw=%d pred=%d log_prob=%.2f'%(ytrue[i],labels_raw[i],labels[i],log_probs[i]),X[i])
        return labels


