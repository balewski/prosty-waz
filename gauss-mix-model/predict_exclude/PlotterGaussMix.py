
import numpy as np
from matplotlib import cm as cmap

import sys,os
sys.path.append(os.path.abspath("../../matplotlib/"))

from PlotterBackbone import PlotterBackbone
import matplotlib.ticker as ticker
from matplotlib.patches import Ellipse
from matplotlib.colors import LogNorm


#............................
#............................
#............................
class PlotterGaussMix(PlotterBackbone):
    def __init__(self,  args):
        PlotterBackbone.__init__(self,args)


#...!...!..................
    def rawIQ(self,dataIQ,tit0='',xyRange=None,figId=5):
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white',figsize=(6,4.5))

        assert dataIQ.ndim==2
        assert dataIQ.shape[1]==2

        nrow,ncol=1,1
        ax=self.plt.subplot(nrow, ncol, 1)
        ax.set_aspect(1.0)

        binsL=(30,30)
        #normOpt=None ; ,cmin=1
        normOpt=LogNorm()
        #binsL=( np.linspace(-180,220,30), np.linspace(-200,120,30)) #tmp fixed range
        zsum,xbins,ybins,img = ax.hist2d(dataIQ[:,0],dataIQ[:,1],bins=binsL,norm=normOpt,cmap = cmap.rainbow)
        fig.colorbar(img, ax=ax,label='shots, sum=%.0f'%dataIQ.shape[0])
        
        zMx=np.max(zsum)
        ax.text(0.05,0.01,'zMax=%d shots'%zMx,transform=ax.transAxes,color='red')
        #ax.set(xlabel='I quadrature/%0.0e'%ampl_fact,ylabel='Q quadrature/%0.0e'%ampl_fact)
        ax.set_title(tit0)
        [ label.set_rotation(45) for label in ax.get_xticklabels()]
        ax.grid()
        if xyRange!=None:
            ax.set_xlim(xyRange[0])
            ax.set_ylim(xyRange[1])
        return ax,xbins,ybins


#...!...!..................
    def addGmixEllipses(self,ax,gmix,lab=None,ecol='black',nSig=3):
        assert ax!=None
        for k in gmix.covElli:
            center,angle,width,height = gmix.covElli[k]
            # Draw the Ellipse
            #print('cent',center,type(center))
            symb='X'
            if k==0: symb='o'
            if k==1: symb='*'
            print('PlEli:',k,[center[0]],[center[1]],symb)
            ax.plot([center[0]],[center[1]],symb,mec='black',markersize=15.)
            for nsig in range(1, nSig+1):
                lnst='-'
                if nsig==3: lnst='--'
                ax.add_patch(Ellipse(center, nsig * width, nsig * height, angle, edgecolor=ecol, facecolor='none',linestyle=lnst))
        if lab:
            #print('ddd',ecol,lab, lab=='measB')
            if ecol=='black':
                ax.text(0.5,0.98,lab,transform=ax.transAxes)
            elif lab=='fit=measB':
                ax.text(0.5,0.94,lab,transform=ax.transAxes,color=ecol)
            else:
                ax.text(0.5,0.90,lab,transform=ax.transAxes,color=ecol)
