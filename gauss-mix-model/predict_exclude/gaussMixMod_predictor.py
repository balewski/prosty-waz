#!/usr/bin/env python3
''' 
1) Training
-) generate training 2D data for nClust=3
-) fit GMM to the data
-) draw train data & fit ellipses

2) Predictions
- draw nSig exclusion regions 
- predict cluster with nSig proximity 

Utility classes:
PlotterGaussMix  - any graphics
GaussMix2D - manages GMM

'''

import numpy as np
from pprint import pprint

# Generate some data
from sklearn.datasets.samples_generator import make_blobs
from sklearn import mixture

import sklearn
print('sklearn ver=',sklearn.__version__)

import time,os
#import warnings
#warnings.simplefilter('ignore', yaml.error.MantissaNoDotYAML1_1Warning)

from GaussMix2D import GaussMix2D
from PlotterGaussMix import PlotterGaussMix

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-n","--numSamples",default=5000,type=int,help="number training samples")
    parser.add_argument( "--numClust",default=3,type=int,help="number of generated 2D clusters")
    parser.add_argument( "-o","--outPath",default='out/',help="output path for plots and tables")
    parser.add_argument( "-X","--noXterm", dest='noXterm', action='store_true', default=False,help="disable X-term for batch mode")

    args = parser.parse_args()
    args.outPath+='/'
    args.prjName='gmmPred'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    assert os.path.exists(args.outPath)
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
#...!...!..................
if __name__=="__main__":

    args=get_parser()
    gmix=GaussMix2D(num_clust=args.numClust)
    plot=PlotterGaussMix(args)
    
      
    Xall, yall = make_blobs(n_samples=args.numSamples, centers=args.numClust, cluster_std=0.60, random_state=0)
    numTest=args.numSamples//10
    X=Xall[numTest:]; 
    Xtest=Xall[:numTest];ytest = yall[:numTest]
    print('X shape',X.shape)

    gmix.fit(X)
    gmix.lock_clusters_ids()
    ax,xbins,ybins=plot.rawIQ(X,tit0='train data')
    plot.addGmixEllipses(ax,gmix,lab='fit1-2-3')
    xyRange=[(xbins[0],xbins[-1]),(ybins[0],ybins[-1])]

    # plotting   predictions with exclusion
    kSig=2.
    ypred,log_probs=gmix.predict_exclude(X,kSig=None)
    jCl=1
    Xb=X[ypred==jCl]
    #Xb=Xtest[ypred>=0]
    
    ax,xbins,ybins=plot.rawIQ(Xb,tit0='pred=%d  kSig=%s'%(jCl,str(kSig)),xyRange=xyRange)
    plot.addGmixEllipses(ax,gmix,lab='')
    

    if 0:
        # plotting   predictions  with evalution
        ypred=gmix.predict_eval(Xtest,ytest)
        
        jCl=1
        Xb=Xtest[ypred==jCl]
    

        ax,xbins,ybins=plot.rawIQ(Xb,tit0='pred=%d all'%jCl)
        plot.addGmixEllipses(ax,gmix,lab='fit1-2-3')


    
    #plot.addPartitionCurve(ax,gmix,lcol='g')

    #ok66
    
    plot.display_all()



