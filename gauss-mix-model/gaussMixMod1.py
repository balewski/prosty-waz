#!/usr/bin/env python
# based on
#  https://jakevdp.github.io/PythonDataScienceHandbook/05.12-gaussian-mixtures.html
'''
A Gaussian mixture model (GMM) attempts to find a mixture of multi-dimensional Gaussian probability distributions that best model any input dataset.
'''

import numpy as np
from matplotlib import pyplot as plt


# Generate some data
from sklearn.datasets.samples_generator import make_blobs
X, y_true = make_blobs(n_samples=400, centers=4,
                       cluster_std=0.60, random_state=0)
X = X[:, ::-1] # flip axes for better plotting

from sklearn import mixture

gmm = mixture.GaussianMixture(n_components=4).fit(X)
labels = gmm.predict(X)
probs = gmm.predict_proba(X)

# visualize this uncertainty by, for example, making the size of each point proportional to the certainty of its prediction
ptsize = 50 * probs.max(1) ** 2  # square emphasizes differences

plt.scatter(X[:, 0], X[:, 1], c=labels, s=ptsize, cmap='viridis');
plt.grid()


print('Probabilistic cluster assignment')

for i in range(5):
    print('sample i=',i,' data:',X[i].round(3),' probs:',probs[i].round(3))


plt.show()

plt.figure(10,facecolor='white', figsize=(6,4))


