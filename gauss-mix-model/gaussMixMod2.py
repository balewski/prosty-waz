#!/usr/bin/env python
# based on
#  https://jakevdp.github.io/PythonDataScienceHandbook/05.12-gaussian-mixtures.html

'''
Let's create a function that will help us visualize the locations and shapes of the GMM clusters by drawing ellipses based on the GMM output
'''

import numpy as np
from matplotlib import pyplot as plt
from matplotlib.patches import Ellipse

# Generate some data
from sklearn.datasets.samples_generator import make_blobs
X, y_true = make_blobs(n_samples=400, centers=4,
                       cluster_std=0.60, random_state=0)
X = X[:, ::-1] # flip axes for better plotting
print('X shape',X.shape)

from sklearn import mixture


def draw_ellipse(position, covariance, ax=None, **kwargs):
    """Draw an ellipse with a given position and covariance"""
    ax = ax or plt.gca()
    
    # Convert covariance to principal axes
    if covariance.shape == (2, 2):
        U, s, Vt = np.linalg.svd(covariance)
        angle = np.degrees(np.arctan2(U[1, 0], U[0, 0]))
        width, height = 2 * np.sqrt(s)
    else:
        angle = 0
        width, height = 2 * np.sqrt(covariance)
    
    # Draw the Ellipse
    for nsig in range(1, 4):
        ax.add_patch(Ellipse(position, nsig * width, nsig * height,
                             angle, **kwargs))
        
def plot_gmm(gmm, X, label=True, ax=None):
    ax = ax or plt.gca()
    labels = gmm.fit(X).predict(X)
    if label:
        ax.scatter(X[:, 0], X[:, 1], c=labels, s=40, cmap='viridis', zorder=2)
    else:
        ax.scatter(X[:, 0], X[:, 1], s=40, zorder=2)
    ax.axis('equal')
    
    w_factor = 0.2 / gmm.weights_.max()
    for pos, covar, w in zip(gmm.means_, gmm.covariances_, gmm.weights_):
        draw_ellipse(pos, covar, alpha=w * w_factor)




'''  How many DOF are allowed
* diag: the size of the cluster along each dimension can be set independently, with the resulting ellipse constrained to align with the axes

* spherical: constrains the shape of the cluster such that all dimensions are equal. Result is similar characteristics to that of k-means

* full: allows each cluster to be modeled as an ellipse with arbitrary orientation

'''
covTypeL=['full','diag','spherical']
gmm = mixture.GaussianMixture(n_components=4, covariance_type=covTypeL[0])
plt.figure(10,facecolor='white', figsize=(6,4))
plot_gmm(gmm, X)
plt.grid()


rng = np.random.RandomState(13)
X_stretched = np.dot(X, rng.randn(2, 2))
plt.figure(11,facecolor='white', figsize=(6,4))
plot_gmm(gmm, X_stretched)

plt.show()




