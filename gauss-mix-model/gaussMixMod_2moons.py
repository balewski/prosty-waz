#!/usr/bin/env python
# based on
#  https://jakevdp.github.io/PythonDataScienceHandbook/05.12-gaussian-mixtures.html

'''
GMM as Density Estimation
'''

import numpy as np
from matplotlib import pyplot as plt
from matplotlib.patches import Ellipse

# Generate some data
from sklearn.datasets import make_moons
Xmoon, ymoon = make_moons(200, noise=.05, random_state=0)
from sklearn import mixture


def draw_ellipse(position, covariance, ax=None, **kwargs):
    """Draw an ellipse with a given position and covariance"""
    ax = ax or plt.gca()
    
    # Convert covariance to principal axes
    if covariance.shape == (2, 2):
        U, s, Vt = np.linalg.svd(covariance)
        angle = np.degrees(np.arctan2(U[1, 0], U[0, 0]))
        width, height = 2 * np.sqrt(s)
    else:
        angle = 0
        width, height = 2 * np.sqrt(covariance)
    
    # Draw the Ellipse
    for nsig in range(1, 4):
        ax.add_patch(Ellipse(position, nsig * width, nsig * height,
                             angle, **kwargs))
        
def plot_gmm(gmm, X, label=True, ax=None):
    ax = ax or plt.gca()
    labels = gmm.fit(X).predict(X)
    if label:
        ax.scatter(X[:, 0], X[:, 1], c=labels, s=40, cmap='viridis', zorder=2)
    else:
        ax.scatter(X[:, 0], X[:, 1], s=40, zorder=2)
    ax.axis('equal')
    
    w_factor = 0.2 / gmm.weights_.max()
    for pos, covar, w in zip(gmm.means_, gmm.covariances_, gmm.weights_):
        draw_ellipse(pos, covar, alpha=w * w_factor)




'''  How many DOF are allowed
* diag: the size of the cluster along each dimension can be set independently, with the resulting ellipse constrained to align with the axes

* spherical: constrains the shape of the cluster such that all dimensions are equal. Result is similar characteristics to that of k-means

* full: allows each cluster to be modeled as an ellipse with arbitrary orientation

'''

covTypeL=['full','diag','spherical']
gmm = mixture.GaussianMixture(n_components=4, covariance_type=covTypeL[0])
plt.figure(10,facecolor='white', figsize=(6,4))
plot_gmm(gmm, Xmoon)
plt.grid()


'''
 to adjust the model likelihoods using some analytic criterion such as

* the Akaike information criterion (AIC)
https://en.wikipedia.org/wiki/Akaike_information_criterion

* the Bayesian information criterion (BIC).
https://en.wikipedia.org/wiki/Bayesian_information_criterion
'''

n_components = np.arange(1, 21)
models = [mixture.GaussianMixture(n, covariance_type='full', random_state=0).fit(Xmoon)
          for n in n_components]

plt.figure(11,facecolor='white', figsize=(6,4))
plt.plot(n_components, [m.bic(Xmoon) for m in models], label='BIC')
plt.plot(n_components, [m.aic(Xmoon) for m in models], label='AIC')
plt.legend(loc='best')
plt.xlabel('n_components');
plt.grid()
plt.show()




