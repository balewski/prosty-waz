#!/usr/bin/env python3
'''
based on 
https://scikit-image.org/docs/dev/auto_examples/filters/plot_entropy.html

For an image, local entropy is related to the complexity contained in a given neighborhood, typically defined by a structuring element. The entropy filter can detect subtle variations in the local gray level distribution.

 the image is composed of two surfaces with two slightly different distributions. The image has a uniform random distribution in the range [-15, +15] in the middle of the image and a uniform random distribution in the range [-14, 14] at the image borders, both centered at a gray value of 128. To detect the central square, we compute the local entropy measure using a circular structuring element of a radius big enough to capture the local gray level distribution. 

'''

import matplotlib.pyplot as plt
import numpy as np

from skimage import data
from skimage.util import img_as_ubyte
from skimage.filters.rank import entropy
from skimage.morphology import disk

rng = np.random.default_rng()

noise_mask = np.full((128, 128), 28, dtype=np.uint8)
noise_mask[32:-32, 32:-32] = 30

noise = (noise_mask * rng.random(noise_mask.shape) - 0.5
         * noise_mask).astype(np.uint8)
img = noise + 128

# ... characterize image
zmin,zmax=np.min(img)*1.,np.max(img)*1.
print('Image dynamic range: [%.1f, %.1f]  rel=%.2f'%(zmin,zmax, (zmax-zmin)/(zmax+zmin)))

disk_sz=0.3  # try 3
entr_img = entropy(img, disk(10))  # size of local area computing entropy
# ... characterize  entropy
zmin,zmax=np.min(entr_img),np.max(entr_img)
print('entropy_mage dynamic range: [%.1f, %.1f]  rel=%.2f'%(zmin,zmax, (zmax-zmin)/(0.+zmax+zmin)))

fig, (ax0, ax1, ax2) = plt.subplots(nrows=1, ncols=3, figsize=(10, 4))

img0 = ax0.imshow(noise_mask, cmap='gray')
ax0.set_title("Object")
ax1.imshow(img, cmap='gray')
ax1.set_title("Noisy image")

cf=ax2.imshow(entr_img, cmap='viridis')
ax2.set_title("Local entropy, disk=%.1f"%disk_sz)
fig.colorbar(cf, ax=ax2)
 
fig.tight_layout()
plt.show()
