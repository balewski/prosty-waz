#!/usr/bin/env python3
import sys,os
'''
See details 
https://github.com/CSchoel/nolds
'''

import numpy as np
import nolds


rwalk = np.cumsum(np.random.random(1000))  # highly predictable series
#rwalk = np.random.random(1000)  # pure random series
tolerance = 0.2 * np.std(rwalk)
print('rwalk:',rwalk.shape,', tol=%.3e'%(tolerance),rwalk[:20])

#sample entropy (sampen)
#Measures the complexity of a time-series, based on approximate entropy



for emb_dim in range(2,500,50):
    apEn=nolds.sampen(rwalk,tolerance =tolerance, emb_dim=emb_dim)
    print('emb_dim=%d  apEn=%.3e'%(emb_dim,apEn))

